WEB_BUILD_DIR=frontend/dist
ISORT_OPTS=--profile black --combine-star --use-parentheses -m VERTICAL_HANGING_INDENT -w 79
FLAKE_OPTS=--max-line-length=79 --ignore=E203,W503
SRCS:=backend/api backend/app.py backend/sandbox backend/migrations

.PHONY: all debug test coverage lint doc openapi clean backend-lint backend-lint-apply flake8 isort-check isort-apply black-check black-apply

all:
	cd frontend
	npm run build

debug:
	cd frontend
	npm run debug

test:
	cd backend
	pytest tests_api/

coverage:
	cd backend
	py.test tests_api/ --cov=api/ --cov-report term --cov-report html

isort-check:
	isort ${ISORT_OPTS} --check --diff ${SRCS}

isort-apply:
	isort --atomic ${ISORT_OPTS} ${SRCS}

flake8:
	flake8 ${FLAKE_OPTS} ${SRCS}

black-check:
	black --line-length 79 --check ${SRCS}

black-apply:
	black --line-length 79 ${SRCS}

backend-lint: flake8 isort-check black-check

backend-lint-apply: isort-apply black-apply

lint:
	cd frontend
	npm run -s lint

openapi:
	$(MAKE) -C doc/ openapi

doc:
	$(MAKE) -C doc/ images
	$(MAKE) -C doc/ openapi
	$(MAKE) -C doc/ docker-compose
	$(MAKE) -C doc/ api
	$(MAKE) -C doc/ html

clean:
	rm -rf $(WEB_BUILD_DIR)/*
	find . -name __pycache__ -exec rm -r {} +
	find . -name *.pyc -delete
	$(MAKE) -C doc/ clean

# --

.PHONY: docker docker-sandbox docker-front docker-docker-python docker-base docker-backend docker-worker docker-database

docker-sandbox:
	docker build -f backend/docker/sandbox/Dockerfile -t registry.gitlab.com/decide.imt-atlantique/deseasion/sandbox:latest backend

docker-front:
	docker build -f frontend/Dockerfile -t registry.gitlab.com/decide.imt-atlantique/deseasion/frontend:latest frontend

docker-base:
	docker build -f backend/Dockerfile -t registry.gitlab.com/decide.imt-atlantique/deseasion/base:latest --target base backend

docker-database:
	docker build -f backend/docker/database/Dockerfile -t registry.gitlab.com/decide.imt-atlantique/deseasion/database:latest backend

docker-backend: docker-base
	docker build -f backend/Dockerfile -t registry.gitlab.com/decide.imt-atlantique/deseasion/backend:latest --cache-from registry.gitlab.com/decide.imt-atlantique/deseasion/base:latest backend

docker-worker: docker-base
	docker build -f backend/Dockerfile -t registry.gitlab.com/decide.imt-atlantique/deseasion/worker:latest --cache-from registry.gitlab.com/decide.imt-atlantique/deseasion/base:latest --target worker backend

docker: docker-sandbox docker-front docker-backend docker-worker docker-database