=========
deSEAsion
=========

**The deSEAsion repository has moved to https://gitlab.com/decide.imt-atlantique/deseasion
This current repository is frozen and unmaintained!**

deSEAsion is a collaborative support tool for maritime decision making.

Copyright and licence
=====================

deSEAsion is copyrighted (C) 2016-2020 by IMT Atlantique Bretagne Pays de la Loire
and Service hydrographique et océanographique de la marine (Shom).
Licensed under the European Union Public Licence (EUPL) v1.2.

Please refer to the file LICENCE containing the text of the EUPL v1.2.

You may also obtain a copy of the license at:
https://joinup.ec.europa.eu/software/page/eupl

For more information on this license, please refer to:

  - European Union Public Licence:
    https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics



Documentation
=============

See the `documentation <https://deseasion-2227c9.gitlab.io>`_ if you need any information
about the project, how to install or contribute to it.
