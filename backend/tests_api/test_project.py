from flask import json

import api.models as models

from .utils import FlaskTestCase


class ProjectTestCase(FlaskTestCase):
    def test_create_project(self):
        """Create a new project"""
        json_data = {
                'name': 'Test project',
                'description': 'Test description',
                'extent': [-180, -90, 180, 90]
                }
        resp = self.post('/projects', json=json_data, authenticate=True)
        data = json.loads(resp.data)
        assert data['project']['name'] == json_data['name']
        assert data['project']['description'] == json_data['description']
        assert data['project']['extent'] == json_data['extent']
        assert data['project']['manager']['id'] == self.user.id

    def test_create_incorrect_extent_project(self):
        """Create a project with an invalid extent"""
        json_data = {
                'name': 'Test project 2',
                'extent': [-182, -92, 80, 90]
                }
        resp = self.post('/projects', json=json_data, authenticate=True)
        assert resp.status_code == 400

    def test_get_managed_projects(self):
        """Get the listo f projects managed by the user"""
        proj = models.Project(name='New managed', manager=self.user)
        proj.create()
        resp = self.get('/projects', authenticate=True)
        data = json.loads(resp.data)
        assert 'New managed' in (p['name'] for p in data['projects'])

    def test_get_project_details(self):
        """Get the details of a project"""
        proj = models.Project(name='New project', description='Description details', manager=self.user)
        proj.create()
        resp = self.get('/project/{}'.format(proj.id), authenticate=True)
        data = json.loads(resp.data)
        assert data['project']['name'] == 'New project'
        assert data['project']['description'] == 'Description details'
        assert data['project']['extent'] == [-180, -90, 180, 90]
        assert data['project']['manager']['id'] == self.user.id

    def test_get_project_hierarchy(self):
        proj = models.Project(name='Hierarchy', manager=self.user)
        proj.decision_map.name = 'hierarchy_root'
        proj.create()
        resp = self.get('/project/{}/hierarchy'.format(proj.id), authenticate=True)
        data = json.loads(resp.data)
        assert data['hierarchy']['name'] == 'hierarchy_root'
