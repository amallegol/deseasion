from flask import json
import unittest
import base64

from api import create_app
from api.models import db

from .factories import UserFactory


def get_auth_headers(username=None, password=None):
    """Returns a Basic authentication header with the given username and password"""
    encoded = base64.b64encode(bytes('{}:{}'.format(username, password), 'utf-8'))
    return {'Authorization': 'Basic ' + encoded.decode()}


class TestClientMixin:
    def _request(self, method, *args, **kwargs):
        if kwargs.pop('authenticate', False):
            if 'headers' in kwargs:
                kwargs['headers']['Authorization'] = self.jwt_token
            else:
                kwargs['headers'] = {'Authorization': self.jwt_token}
        if 'json' in kwargs:
            kwargs['data'] = json.dumps(kwargs.pop('json'))
            kwargs['content_type'] = 'application/json'
        return method(*args, **kwargs)

    def get(self, *args, **kwargs):
        return self._request(self.client.get, *args, **kwargs)

    def put(self, *args, **kwargs):
        return self._request(self.client.put, *args, **kwargs)

    def post(self, *args, **kwargs):
        return self._request(self.client.post, *args, **kwargs)

    def delete(self, *args, **kwargs):
        return self._request(self.client.delete, *args, **kwargs)


class FlaskTestCase(TestClientMixin, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(FlaskTestCase, cls).setUpClass()
        cls.app = create_app('test.py')
        cls.client = cls.app.test_client()
        cls.app_context = cls.app.app_context()
        cls.app_context.push()
        cls.db = db
        cls.db.drop_all()
        cls.db.create_all()
        cls.user = UserFactory.new()
        cls._login()

    @classmethod
    def tearDownClass(cls):
        cls.app_context.pop()

    @classmethod
    def _login(cls):
        username = cls.user.username
        auth_header = get_auth_headers(username, 'password')
        resp = cls.client.get(
                '/login',
                headers=auth_header
                )
        data = json.loads(resp.data)
        assert 'access_token' in data
        assert 'token_type' in data
        cls.jwt_token = '{} {}'.format(data['token_type'], data['access_token'])
