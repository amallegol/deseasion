from flask import json

from .utils import FlaskTestCase


class UserTestCase(FlaskTestCase):
    # def test_login_correct(self):
        # username = self.user.username
        # auth_header = get_auth_headers(username, 'password')
        # resp = self.client.get('/login', headers=auth_header)
        # data = json.loads(resp.data)
        # assert 'access_token' in data
        # assert 'token_type' in data

    # def test_login_incorrect(self):
        # username = self.user.username
        # auth_header = get_auth_headers(username, 'wrong_password')
        # resp = self.client.get('/login', headers=auth_header)
        # data = json.loads(resp.data)
        # assert resp.status_code == 401
        # assert 'message' in data

    def test_user_connected(self):
        """Get the authenticated user's information"""
        resp = self.get('/user', authenticate=True)
        data = json.loads(resp.data)
        assert data['user']['username'] == self.user.username
        assert data['user']['email'] == self.user.email

    def test_user_not_authenticated(self):
        """Try to create a new project without being authenticated"""
        project_data = {'name': 'Forbidden'}
        resp = self.post('/projects', json=project_data)
        assert resp.status_code == 401
