from flask import json

from .factories import load_shp
from .utils import FlaskTestCase


class ProjectDataTestCase(FlaskTestCase):
    def test_get_geo_data_list(self):
        """Get the full list of geo data"""
        geo_data = load_shp('Zones.shp', user=self.user)
        resp = self.get('/geo-data', authenticate=True)
        assert resp.status_code == 200
        data = json.loads(resp.data)
        assert geo_data.name in (g['name'] for g in data['geodata'])

    def test_get_geo_data(self):
        """Get a specific geo data using its id"""
        geo_data = load_shp('Zones.shp', user=self.user)
        resp = self.get('/geo-data/{}'.format(geo_data.id), authenticate=True)
        assert resp.status_code == 200
        data = json.loads(resp.data)
        assert data['geodata']['name'] == 'Zones'

    def test_update_geo_data(self):
        """Update a geo data attribute"""
        geo_data = load_shp('Zones.shp', user=self.user)
        uri = 'geo-data/{}'.format(geo_data.id)
        new_data = {'name': 'New_name_zones'}
        resp = self.put(uri, json=new_data, authenticate=True)
        data = json.loads(resp.data)
        assert resp.status_code == 200
        assert data['geodata']['name'] == 'New_name_zones'
        assert geo_data.name == 'New_name_zones'

    def test_incorrect_geo_data_name(self):
        """Try to update a data with an incorrect name"""
        geo_data = load_shp('Zones.shp', user=self.user)
        uri = 'geo-data/{}'.format(geo_data.id)
        new_data = {'name': 'New incorrect name'}
        resp = self.put(uri, json=new_data, authenticate=True)
        data = json.loads(resp.data)
        assert resp.status_code == 400
        assert geo_data.name != 'New incorrect name'

    def test_delete_geo_data(self):
        """Deletes a geo data"""
        from sqlalchemy import inspect
        geo_data = load_shp('Zones.shp', user=self.user)
        uri = 'geo-data/{}'.format(geo_data.id)
        resp = self.delete(uri, authenticate=True)
        assert resp.status_code == 200
        assert not inspect(geo_data).persistent
