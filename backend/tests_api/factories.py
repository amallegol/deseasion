import os.path

from api.models import User, UserPermission, PermissionAbility
from api.services.geo_data_loading_service import load_vector_file


class UserFactory:
    @classmethod
    def new(cls):
        username = 'username'
        password = 'password'
        email = 'username@email'
        user = User(username=username, email=email, password=password)
        user.permissions.append(UserPermission(ability=PermissionAbility.create_project))
        user.permissions.append(UserPermission(ability=PermissionAbility.create_geo_data))
        user.create()
        return user


def load_shp(path, user=None):
    f_path = os.path.join(os.path.dirname(__file__), 'test_data', path)
    geo_data = load_vector_file(f_path)
    if user is not None:
        geo_data.upload_user = user
        geo_data.update()
    return geo_data
