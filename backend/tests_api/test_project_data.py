from flask import json

import api.models as models

from .utils import FlaskTestCase


class ProjectDataTestCase(FlaskTestCase):
    def setUp(self):
        self.project = models.Project(name='Project', manager=self.user)
        self.project.create()

    def test_get_data_details(self):
        data_id = self.project.decision_map.id
        resp = self.get('/project-data/{}'.format(data_id), authenticate=True)
        data = json.loads(resp.data)
        assert data['project_data']['name'] == self.project.decision_map.name
        assert data['project_data']['id'] == self.project.decision_map.id

    def test_create_hierarchy_data_input(self):
        """Create a new data in the hierarchy"""
        data_id = self.project.decision_map.id
        data_types = ('criterion', 'decision_map')
        for d_type in data_types:
            name = 'new_' + d_type
            json_data = {
                    'data_type': d_type,
                    'name': name
                    }
            resp = self.post('/project-data/{}/input-data'.format(data_id),
                             json=json_data, authenticate=True)
            data = json.loads(resp.data)
            assert name in (d['name'] for d in data['input_data'])

    def test_create_hierarchy_data_input_incorrect(self):
        """Try to create a new data in the hierarchy with an incorrect datatype"""
        data_id = self.project.decision_map.id
        json_data = {
                'name': 'new_incorrect_data',
                'data_type': 'incorrect_data_type'
                }
        resp = self.post('/project-data/{}/input-data'.format(data_id),
                         json=json_data, authenticate=True)
        assert resp.status_code == 400

    def test_create_hierarchy_data_input_forbidden(self):
        """Try to create a new data in the hierarchy with a forbidden data_type"""
        data_id = self.project.decision_map.id
        data_types = ('intermediate_data', 'geo_data')
        for d_type in data_types:
            json_data = {
                    'name': 'forbidden_intermediate_data',
                    'data_type': d_type
                    }
            resp = self.post('/project-data/{}/input-data'.format(data_id),
                             json=json_data, authenticate=True)
            assert resp.status_code == 400

    def test_create_hierarchy_data_incorrect_name(self):
        """Try to create a new data in the hierarchy with a name containing forbidden chars"""
        data_id = self.project.decision_map.id
        json_data = {
                'name': 'Forbidden name -+é',
                'data_type': 'criterion'
                }
        resp = self.post('/project-data/{}/input-data'.format(data_id),
                         json=json_data, authenticate=True)
        assert resp.status_code == 400

    def test_create_incorrect_datatype(self):
        def assert_incorrect(json_data, msg):
            data_id = self.project.decision_map.id
            resp = self.post('/project-data/{}/input-data'.format(data_id),
                             json=json_data, authenticate=True)
            assert resp.status_code == 400, '{} should not be correct: {}'.format(json_data, msg)

        assert_incorrect({'name': 'incorrect_data'}, 'No data_type attribute')
        assert_incorrect({'data_type': 'incorrect_data_type', 'name': 'incorrect_data'},
                         'Incorrect data_type attribute')
