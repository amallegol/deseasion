import unittest
import jwt

from api.security import JWTHandler


class JWTHandlerTest(unittest.TestCase):
    def setUp(self):
        self.jwt_handler = JWTHandler()
        self.algorithm = 'HS256'
        self.secret_key = 'secretkey'
        self.jwt_handler.secret_key = self.secret_key
        self.jwt_handler.signing_algorithm = self.algorithm

    def test_create_token(self):
        data = {'data': 'test'}
        token = self.jwt_handler.create_token(data)
        decoded = jwt.decode(token, self.secret_key)
        self.assertEqual(data, decoded)

    def test_create_user_token(self):
        user_id = 12
        token = self.jwt_handler.create_user_token(user_id)
        decoded = jwt.decode(token, self.secret_key)
        self.assertEqual(decoded.get('sub', None), user_id)

    def test_get_user_id(self):
        user_id = 12
        token = self.jwt_handler.create_user_token(user_id)
        self.assertEqual(user_id, self.jwt_handler.get_user_id(token))


if __name__ == '__main__':
    unittest.main()
