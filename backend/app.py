from getpass import getpass

import click
from api import create_app, create_celery, create_migrate, create_swagger
from api.models import PermissionAbility, User, UserPermission, db
from flask.cli import AppGroup

app = create_app("config.py")
create_swagger(app)
celery = create_celery(app)
create_migrate(app)


user_cli = AppGroup("user", short_help="Enable manipulations of users")
db_cli_group = app.cli.commands["db"]


@user_cli.command("create")
def create_user_command():
    """Create a user."""
    email = input("Email: ")
    if "@" not in email:
        raise ValueError("Invalid email, should contain the '@' character")
    _username = input("Username: ")
    username = email.split("@")[0] if len(_username) == 0 else _username
    password = getpass("Password: ")
    password_confirm = getpass("Confirm password: ")
    if password != password_confirm:
        raise ValueError("The password doesn't match")
    user = User(username=username, email=email, password=password)
    create_project = input("Allow to create projects? (y/n) [y]: ").lower()
    create_data = input("Allow to upload geo-data? (y/n) [y]: ").lower()
    if len(create_project) == 0 or create_data.lower()[0] == "y":
        user.permissions.append(
            UserPermission(ability=PermissionAbility.create_project)
        )
    if len(create_data) == 0 or create_data.lower()[0] == "y":
        user.permissions.append(
            UserPermission(ability=PermissionAbility.create_geo_data)
        )
    user.create()


@db_cli_group.command()
@click.option(
    "-d",
    "--directory",
    default=None,
    help=('Migration script directory (default is "migrations")'),
)
@click.option(
    "--tag",
    default=None,
    help=('Arbitrary "tag" name - can be used by custom env.py ' "scripts"),
)
@click.option(
    "-f",
    "--force",
    default=False,
    help="Force database initialization (even if already initialized)",
)
@click.option(
    "-x",
    "--x-arg",
    multiple=True,
    help="Additional arguments consumed by custom env.py scripts",
)
def create(directory, tag, force, x_arg):
    """Creates all the SQLAlchemy tables and add SQL functions."""
    from alembic import command
    from alembic.migration import MigrationContext
    from sqlalchemy.sql import text

    config = app.extensions["migrate"].migrate.get_config(
        directory, x_arg=x_arg
    )

    engine = db.get_engine()
    with engine.connect() as conn:
        migration_ctx = MigrationContext.configure(conn)
        current = migration_ctx.get_current_revision()

    if current is None or force:
        db.create_all()
        with open("sql/dissolve_adjacent.sql") as f:
            db.session.execute(text(f.read()))
        with open("sql/tile_bbox.sql") as f:
            db.session.execute(text(f.read()))
        with open("sql/trigger_feature_make_valid.sql") as f:
            db.session.execute(text(f.read()))
        db.session.commit()
        command.stamp(config, revision="head", tag=tag)
    else:
        print(
            "Database already exists. You may force its creation "
            "with '--force' option (may break database).\n"
            "If wanting to upgrade it, use 'flask db upgrade' command instead."
        )


@db_cli_group.command()
@click.option(
    "-d",
    "--directory",
    default=None,
    help=('Migration script directory (default is "migrations")'),
)
@click.option(
    "-x",
    "--x-arg",
    multiple=True,
    help="Additional arguments consumed by custom env.py scripts",
)
def drop(directory, x_arg):
    """Drops all the SQLAlchemy tables."""
    from alembic import command

    config = app.extensions["migrate"].migrate.get_config(
        directory, x_arg=x_arg
    )

    if input("Your data will be lost. Continue? (Y/y): ")[0] in ["Y", "y"]:
        db.drop_all()
        command.stamp(config, revision=None)


app.cli.add_command(user_cli)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
