from api.models import PermissionAbility, User, UserPermission
from flask_script import Command, prompt, prompt_bool, prompt_pass
from flask_script.commands import InvalidCommand


class CreateUser(Command):
    """Create a new user."""

    def run(self):
        email = prompt("Email")
        if "@" not in email:
            raise InvalidCommand(
                "Invalid email, should contain the '@' character"
            )
        username = prompt("Username", default=email.split("@")[0])
        password = prompt_pass("Password")
        password_confirm = prompt_pass("Confirm password")
        if password != password_confirm:
            raise InvalidCommand("The password doesn't match")
        user = User(username=username, email=email, password=password)
        if prompt_bool("Allow to create projects?", default=True):
            user.permissions.append(
                UserPermission(ability=PermissionAbility.create_project)
            )
        if prompt_bool("Allow to upload geo-data?", default=True):
            user.permissions.append(
                UserPermission(ability=PermissionAbility.create_geo_data)
            )
        user.create()
