import time
from datetime import datetime

from celery import Task, shared_task, states
from celery.result import AsyncResult
from celery.signals import (
    task_failure,
    task_postrun,
    task_prerun,
    task_revoked,
)

from ..exceptions import ProcessingError
from ..models import (
    PreferenceModel,
    Project,
    ProjectTaskModel,
    ProjectTaskType,
    db,
)
from ..models.geo_data_utils import ModelProcessingService
from ..services import genetic_algorithm_service as ga_service


class ContextTask(Task):
    app_context = None

    def __call__(self, *args, **kwargs):
        with self.app_context:
            return self.run(*args, **kwargs)


class ProgressFeedback:
    def __init__(self, task):
        self._task = task
        self.message = None

    def set_progress(self, count, total):
        progress = int(100 * count / total)
        self._task.update_state(
            state=states.STARTED,
            meta={"progress": progress, "message": self.message},
        )

    def set_message(self, message):
        self.message = message
        self._task.update_state(
            state=states.STARTED, meta={"message": message}
        )

    def forget_message(self):
        self.message = None
        self._task.update_state(state=states.STARTED, meta={"message": None})


@shared_task(name=ProjectTaskType.process_project_data.name, bind=True)
def process_and_set_model(self, *, model_id):
    """Process the model and save the new data."""
    model = PreferenceModel.get_by_id(model_id)
    processing_service = ModelProcessingService()
    feedback = ProgressFeedback(self)
    feedback.set_message("Processing model geometries")
    geometries = processing_service.process_model_geometries(
        model, feedback=feedback
    )
    with db.session.no_autoflush:
        features = processing_service.process_model(
            model, geometries, feedback=feedback
        )
        processing_service.set_model_features(model, features)


@shared_task(name=ProjectTaskType.process_zone_proposition.name, bind=True)
def process_zone_proposition(
    self, *, project_id, iterations, geo_size, duration, ga_params
):
    """Create zone propositions for the project decision map"""
    assert iterations is not None or duration is not None
    # TODO: Check permissions of the project
    project = Project.get_by_id(project_id)
    # TODO: Handle the case where the root_data is not a decision map
    # Use Gall-Peters equal-area projection
    ga_ctx = ga_service.ga_context_from_decision_map(
        project.root_data,
        geo_size,
        proj=(
            "+proj=cea +lon_0=0 +lat_ts=45 +x_0=0 +y_0=0 +ellps=WGS84 "
            "+datum=WGS84 +units=m +no_defs"
        ),
    )
    ga = ga_service.ga_parametrized(ga_ctx, ga_params)
    time_start = time.time()
    iteration_continue, duration_continue = True, True
    while iteration_continue and duration_continue:
        ga.iterate()
        current_duration = time.time() - time_start
        iteration_continue = (
            ga.iteration < iterations if iterations is not None else True
        )
        duration_continue = (
            current_duration < duration if duration is not None else True
        )
        if iterations is not None and duration is None:
            progress = ga.iteration / iterations
        elif iterations is None and duration is not None:
            progress = current_duration / duration
        else:
            progress = max(
                [ga.iteration / iterations, current_duration / duration]
            )
        self.update_state(
            state=states.STARTED,
            meta={"progress": min([int(100 * progress), 100])},
        )
    proposition = ga_service.zone_proposition_from_genetic_algorithm(ga)
    proposition.project = project
    proposition.create()


@task_prerun.connect
def project_task_prerun(task_id, task, args, kwargs, **kw):
    """Prerun signal for the tasks."""
    try:
        task_type = ProjectTaskType[task.name]
    except KeyError:
        return
    time = datetime.utcnow()
    with task.app_context:
        project = None
        if task_type == ProjectTaskType.process_project_data:
            model_id = kwargs["model_id"]
            model = PreferenceModel.get_by_id(model_id)
            project = model.data_generator.project
        elif task_type == ProjectTaskType.process_zone_proposition:
            project_id = kwargs["project_id"]
            project = Project.get_by_id(project_id)
        if project is not None:
            task_model = ProjectTaskModel(
                task_id=task_id, project=project, started_at=time
            )
            task_model.type = task_type
            task_model.params = kwargs
            task_model.state = states.STARTED
            task_model.create()
    print("task {} ({}) started at {}".format(task.name, task_id, time))


@task_postrun.connect
def project_task_postrun(task_id, task, args, kwargs, retval, state, **kw):
    """Postrun signal for the tasks

    If the task name is 'process_project_data', the final status and finish
    time is saved to the database.
    """
    if state is None:
        result = AsyncResult(task_id)
        state = result.state
    time = datetime.utcnow()
    with task.app_context:  # use the flask application context
        task_model = ProjectTaskModel.get_by_task_id(task_id)
        if task_model is not None:
            task_model.finished_at = time
            task_model.state = state
            task_model.update()
    print(
        "task {} ({}) finishing with state {} at {}".format(
            task.name, task_id, state, time
        )
    )


@task_failure.connect
def project_task_failure(
    task_id, exception, args, kwargs, traceback, einfo, **kw
):
    """Save the 'FAILED' state and the error message when a task fails"""
    error = einfo.exception
    if isinstance(error, ProcessingError):
        with process_and_set_model.app_context:
            task_model = ProjectTaskModel.get_by_task_id(task_id)
            if task_model is not None:
                task_model.error_message = str(error)
                task_model.finished_at = datetime.utcnow()
                task_model.update()


@task_revoked.connect
def project_task_revoked(request, terminated, signum, expired, **wk):
    """Save the 'REVOKED' state and the time when a task is revoked"""
    with request.task.app_context:
        task_model = ProjectTaskModel.get_by_task_id(request.task_id)
        if task_model is not None:
            task_model.finished_at = datetime.utcnow()
            task_model.state = states.REVOKED
            task_model.update()
