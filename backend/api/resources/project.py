from flask import jsonify, request
from flask_restful import Resource
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError

from ..exceptions import InvalidValue, RequestError
from ..models import (
    DataType,
    PermissionAbility,
    Project,
    ProjectData,
    ProjectTaskModel,
    db,
)
from ..models.project_data_utils import ProjectDataProcessingService
from ..schemas import (
    DataGeneratorSchema,
    DataGeoSchema,
    GeneticAlgorithmSchema,
    MessageSchema,
    ProjectAccessSchema,
    ProjectCreationSchema,
    ProjectDataBaseSchema,
    ProjectDataCreationSchema,
    ProjectFromTemplateRequestBody,
    ProjectGetResponseSchema,
    ProjectGlobalDataSchema,
    ProjectHierarchyGetResponse,
    ProjectHierarchySchema,
    ProjectListGetResponseSchema,
    ProjectObjectivePostResponseSchema,
    ProjectObjectiveRequestBody,
    ProjectPermissionGetResponseSchema,
    ProjectSchema,
    ProjectSharedDataListGetResponseSchema,
    ProjectSharedDataListPostResponseSchema,
    ProjectTaskSchema,
    ProjectTemplateListGetResponseSchema,
    ProjectTemplatePostResponseSchema,
    TemplateSchema,
)
from ..services import project_data_service, project_service
from ..services.auth_service import check_jwt_authentication, token_required
from ..services.permission_service import has_ability
from ..tasks import process_zone_proposition
from .utils import (
    dump_data,
    get_json_content,
    with_request_body,
    with_response,
)


class ProjectListAPI(Resource):
    @token_required
    @with_response(
        200,
        schema=ProjectListGetResponseSchema,
        description="Full list of accessible projects",
    )
    def get(self):
        """
        Return the full list of projects accessible by the user.

        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).

        .. :quickref: Project; Get the list of accessible projects
        """
        projects = project_service.get_all_authorized_projects()
        return dump_data(ProjectSchema(many=True), projects=projects)

    @token_required
    @has_ability(PermissionAbility.create_project)
    @with_response(
        200,
        schema=ProjectGetResponseSchema,
        description="Created project details",
    )
    @with_request_body(
        schema=[ProjectCreationSchema, ProjectFromTemplateRequestBody],
        description=(
            "Either the project details or the template from which to create "
            "it"
        ),
    )
    def post(self):
        """
        Create a new project.

        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not allowed to create projects.

        .. :quickref: Project; Create a new project
        """
        user = check_jwt_authentication(request)
        content = get_json_content()
        schema = ProjectCreationSchema()
        if "template" in content.keys():
            # load the project from the template using the template id
            template_id = content.pop("template")
            with db.session.no_autoflush:
                project = project_service.create_project_from_template(
                    template_id, user
                )
            print(project, project.is_template)
            # update the project with the content
            project = schema.load(content, instance=project)
        else:
            project = schema.load(content)
            project.manager = user
        project.create()
        return dump_data(schema, project=project)


class ProjectAPI(Resource):
    @token_required
    @with_response(
        200, schema=ProjectGetResponseSchema, description="Project details"
    )
    def get(self, project_id):
        """
        Return the project details for a given id.

        :param int project_id: The id of the project.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not allowed to access this project.
        :status 404: The project doesn't exist.

        .. :quickref: Project; Get the project details
        """
        project = project_service.get_if_authorized(project_id)
        return dump_data(ProjectSchema(), project=project)

    @token_required
    @with_response(
        200, schema=ProjectGetResponseSchema, description="New project details"
    )
    @with_request_body(schema=ProjectSchema, description="New project details")
    def put(self, project_id):
        """
        Update the project details.

        :param int project_id: The id of the project.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not allowed to modify this project.
        :status 404: The project doesn't exist.

        .. :quickref: Project; Update the project details
        """
        project = project_service.get_if_authorized(project_id)
        content = get_json_content()
        schema = ProjectSchema()
        project = schema.load(content, instance=project)
        project.update()
        return dump_data(schema, project=project)

    @token_required
    @with_response(
        200, schema=MessageSchema, description="Project successfully deleted"
    )
    def delete(self, project_id):
        """
        Delete the project.

        :param int project_id: The id of the project.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 400:
            The project is referenced by other objects and cannot be deleted.
        :status 403: The user is not allowed to delete this project.
        :status 404: The project doesn't exist.

        .. :quickref: Project; Delete the project
        """
        project = project_service.get_if_authorized(project_id)
        try:
            project.delete()
        except IntegrityError:
            raise RequestError(
                (
                    "The project is referenced by other objects and cannot be "
                    "deleted"
                ),
                400,
            )
        return jsonify(message="project {} deleted".format(project_id))


class ProjectPermissionsAPI(Resource):
    @token_required
    @with_response(
        200,
        schema=ProjectPermissionGetResponseSchema,
        description="Project permissions",
    )
    def get(self, project_id):
        """
        Allow the project manager to get the list of permissions.

        :param int project_id: The id of the project.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not the manager of this project.
        :status 404: The project doesn't exist.

        .. :quickref: Project; Get the project permissions
        """
        user = check_jwt_authentication(request)
        project = project_service.get_if_manager(project_id, user)
        schema = ProjectAccessSchema()
        return dump_data(schema, access=project)

    @token_required
    @with_response(
        200,
        schema=ProjectPermissionGetResponseSchema,
        description="New project permissions",
    )
    @with_request_body(
        schema=ProjectAccessSchema, description="New project permissions"
    )
    def put(self, project_id):
        """
        Allow to modify the list of permissions for the project.

        Only the project manager is allowed to modify the permissions.
        The project manager is always added to the list of authorized users.

        :param int project_id: The id of the project.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not the manager of this project.
        :status 404: The project doesn't exist.

        .. :quickref: Project; Modify the project permissions
        """
        user = check_jwt_authentication(request)
        project = project_service.get_if_manager(project_id, user)
        schema = ProjectAccessSchema()
        content = get_json_content()
        project = schema.load(content, instance=project)
        if not any([p.user is project.manager for p in project.permissions]):
            # always authorize the manager of the project
            project.permissions.append(
                Project.Permission(user=project.manager)
            )
        project.update()
        return dump_data(schema, access=project)


class ProjectSharedDataListAPI(Resource):
    def load_content(self):
        """Loads the content and the data_type from the request body"""
        content = get_json_content()
        try:
            data_type_str = content.pop("data_type")
            data_type = DataType[data_type_str]
        except KeyError:
            raise RequestError(
                'The json data should contain a "data_type" field with a one '
                "of the following values: "
                "{}".format(", ".join(d.name for d in DataType)),
                400,
            )
        return content, data_type

    @token_required
    @with_response(
        200,
        schema=ProjectSharedDataListPostResponseSchema,
        description="List of project shared data",
    )
    @with_request_body(
        schema=ProjectDataCreationSchema,
        description="New project data details",
    )
    def post(self, project_id):
        """
        Create a new project data shared in the project.

        :param int project_id: The id of the project.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 400: There is an error in the json schema for the data,
                     or the data already exists in the project.
        :status 403: The user is not allowed to access this project.
        :status 404: The project doesn't exist.

        .. :quickref: Project; Create a new shared data in the project
        """
        project = project_service.get_if_authorized(project_id)
        content, data_type = self.load_content()
        schema = ProjectDataCreationSchema.type_schemas[data_type.name]
        try:
            input_data = content.pop("input_data", [])
            project_data = schema().load(content)
        except NameError as error:
            raise RequestError(str(error), 400)
        if project_data.data_type in (DataType.geo_data, DataType.global_data):
            for d in project.shared_data:
                if d.data is not None and project_data.data == d.data:
                    raise RequestError(
                        "This data is already shared in the project", 400
                    )
        project_data.project = project

        try:
            for data_input_id in input_data:
                project_data.add_input(
                    project_data_service.get_if_authorized(data_input_id)
                )
        except (TypeError, ValueError) as exc:
            raise RequestError(str(exc), 400)

        ps = ProjectDataProcessingService()
        data_graph = ps.get_project_data_graph(project)
        # Need to make graph with ids otherwise topological sort will create
        # new ProjectData when cloning the dictionary
        graph = {
            p.id: [idata.id for idata in inputs]
            for p, inputs in data_graph.items()
        }
        try:
            ps._topological_sort(graph)
        except TypeError:
            raise RequestError(
                "new data input creates cycles in project data graph", 400
            )

        project.shared_data.append(project_data)
        project_data.create()
        dump_sch = ProjectDataBaseSchema(
            only=("name", "id", "data", "data_type"), many=True
        )
        return dump_data(dump_sch, shared_data=project.shared_data)

    @token_required
    @with_response(
        200,
        schema=ProjectSharedDataListGetResponseSchema,
        description="List of project shared data",
    )
    def get(self, project_id):
        """
        Return the list of project data shared in the project.

        :param int project_id: The id of the project.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not allowed to access this project.
        :status 404: The project doesn't exist.

        .. :quickref: Project; Get the list of shared data in the project
        """
        project = project_service.get_if_authorized(project_id)
        dump_schemas = {
            DataType.geo_data: DataGeoSchema(
                only=("name", "id", "data", "data_type")
            ),
            DataType.generator: DataGeneratorSchema(
                only=("name", "id", "data", "data_type", "preference_model")
            ),
            DataType.global_data: ProjectGlobalDataSchema(
                only=("name", "id", "data", "data_type")
            ),
        }
        res = []
        for projdata in project.shared_data:
            res.append(dump_schemas[projdata.data_type].dump(projdata))
        return {"shared_data": res}


class ProjectSharedDataAPI(Resource):
    @token_required
    @with_response(
        200,
        schema=ProjectSharedDataListPostResponseSchema,
        description="List of project shared data without deleted data",
    )
    def delete(self, project_id, data_id):
        """
        Delete a shared data from the project.

        :param int project_id: The id of the project
        :param int data_id: The id of the data to delete from the project
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not allowed to access the project.
        :status 404: The project or the data do not exist.

        .. :quickref: Project; Delete a share project data
        """
        project = project_service.get_if_authorized(project_id)
        data = ProjectData.get_by_id(data_id)
        if data is None or data not in project.shared_data:
            raise RequestError(
                "The project does not contain a data {}".format(data_id), 404
            )
        project.shared_data.remove(data)
        data.delete()
        dump_sch = ProjectDataBaseSchema(
            only=("name", "id", "data", "data_type"), many=True
        )
        return dump_data(dump_sch, shared_data=project.shared_data)


class ProjectHierarchyAPI(Resource):
    @token_required
    @with_response(
        200,
        schema=ProjectHierarchyGetResponse,
        description="Project hierarchy tree",
    )
    def get(self, project_id):
        """
        Return the hierarchy of the data in the project.

        :param int project_id: The id of the project.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not allowed to access the project.
        :status 404: The project doesn't exist.

        .. :quickref: Project; Get the project data hierarchy
        """
        project = project_service.get_if_authorized(project_id)
        return dump_data(ProjectHierarchySchema(), hierarchy=project.root_data)


class ProjectObjectiveAPI(Resource):
    @token_required
    @with_response(
        200,
        schema=ProjectObjectivePostResponseSchema,
        description="Details of the newly created asynchronously running task",
    )
    @with_request_body(
        schema=ProjectObjectiveRequestBody,
        description=(
            "Genetic algorithm parameters (at least one of ``iterations`` or "
            "``duration`` must be set)"
        ),
    )
    def post(self, project_id):
        """
        Generate recommended zones using a genetic algorithm.

        Execute the genetic algorithm on the project's decision map
        to recommend the best geometries fitting on the decision map.

        :param int project_id: The id of the project.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 400: There is an error in the parameters of the algorithm.
        :status 403: The user is not allowed to access the project.
        :status 404: The project doesn't exist.

        .. :quickref: Project; Generate zone recommendations
        """
        project = project_service.get_if_authorized(project_id)
        content = get_json_content()
        geo_size = content.get("geo_size")
        iterations = content.get("iterations")
        duration = content.get("duration")
        try:
            params = content.get("genetic_algorithm_params", {})
            ga_params = GeneticAlgorithmSchema().load(params)
        except ValidationError as error:
            if len(error.field_names) == 1:
                raise InvalidValue(
                    "Error in the parameter {}".format(error.field_names[0])
                )
            else:
                raise InvalidValue(
                    "Error in the parameters: "
                    "{}".format(", ".join(error.field_names))
                )
        kwargs = {
            "project_id": project.id,
            "iterations": iterations,
            "geo_size": geo_size,
            "duration": duration,
            "ga_params": ga_params,
        }
        task = process_zone_proposition.apply_async(countdown=1, kwargs=kwargs)
        temporary_task = ProjectTaskModel(task_id=task.id, project=project)
        return dump_data(ProjectTaskSchema(), task=temporary_task)


class ProjectTemplateAPI(Resource):
    @token_required
    @with_response(
        200,
        schema=ProjectTemplatePostResponseSchema,
        description="Created template details",
    )
    def post(self, project_id):
        """Create a template from a project.

        :param int project_id: The id of the project.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not allowed to access the project.
        :status 404: The project doesn't exist.

        .. :quickref: Project; Create a template from project
        """
        user = check_jwt_authentication(request)
        with db.session.no_autoflush:
            template = project_service.create_template_from_project(
                project_id, user
            )
        schema = TemplateSchema()
        content = get_json_content()
        template = schema.load(content, instance=template)
        template.create()
        return dump_data(schema, template=template)


class TemplateListAPI(Resource):
    @token_required
    @with_response(
        200,
        schema=ProjectTemplateListGetResponseSchema,
        description="List of accessible project templates",
    )
    def get(self):
        """
        Return the list of templates of the user.

        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).

        .. :quickref: Template; Get the list of accessible templates
        """
        user = check_jwt_authentication(request)
        templates = project_service.get_user_templates(user)
        return dump_data(TemplateSchema(many=True), templates=templates)


class TemplateAPI(Resource):
    @token_required
    @with_response(
        200,
        schema=ProjectTemplatePostResponseSchema,
        description="New project template details",
    )
    @with_request_body(
        schema=TemplateSchema, description="New project template details"
    )
    def put(self, template_id):
        """Modify a project template.

        :param int template_id: The id of the project template.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not allowed to access the template.
        :status 404: The template doesn't exist.

        .. :quickref: Template; Modify a project template
        """
        user = check_jwt_authentication(request)
        template = project_service.get_template_if_authorized(
            template_id, user
        )
        content = get_json_content()
        schema = TemplateSchema()
        template = schema.load(content, instance=template)
        template.update()
        return dump_data(schema, template=template)

    @token_required
    @with_response(
        200, schema=MessageSchema, description="Template deleted successfully"
    )
    def delete(self, template_id):
        """Delete a project template.

        :param int template_id: The id of the project template.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not allowed to delete the template.
        :status 404: The template doesn't exist.

        .. :quickref: Template; Delete a project template
        """
        user = check_jwt_authentication(request)
        template = project_service.get_template_if_authorized(
            template_id, user
        )
        template.delete()
        return jsonify(message="template {} deleted".format(template_id))
