from flask_restful import Api

from .auth import LoginAPI, RefreshAPI, UserAPI
from .genetic_algorithm import ZonePropositionAPI, ZonePropositionDownloadAPI
from .geo_data import (
    DataAPI,
    DataAttributeAPI,
    FeatureAPI,
    GeoDataAccessAPI,
    GeoDataAPI,
    GeoDataDownloadAPI,
    GeoDataListAPI,
    GeoDataUploadAPI,
    GeoDataUploadRasterAPI,
    GeoDataVectorTileAPI,
    GlobalDataAccessAPI,
    GlobalDataAPI,
    GlobalDataListAPI,
    StreamDataListAPI,
    StreamGeoDataAccessAPI,
    StreamGeoDataAPI,
    StreamGeoDataVersionAPI,
    StreamGeoDataVersionsAPI,
    WFSGeoDataUploadAPI,
    WMSGeoDataUploadAPI,
)
from .permission import UserListAPI
from .project import (
    ProjectAPI,
    ProjectHierarchyAPI,
    ProjectListAPI,
    ProjectObjectiveAPI,
    ProjectPermissionsAPI,
    ProjectSharedDataAPI,
    ProjectSharedDataListAPI,
    ProjectTemplateAPI,
    TemplateAPI,
    TemplateListAPI,
)
from .project_data import (
    MRSortInferenceAPI,
    ProjectDataActiveModelAPI,
    ProjectDataAPI,
    ProjectDataHierarchyAPI,
    ProjectDataInputListAPI,
    ProjectDataListAPI,
    ProjectDataModelAPI,
    ProjectDataModelChangeAPI,
)
from .share import DataShareAPI, ProjectSharesAPI, ShareProjectDataAPI
from .tasks import ProjectTaskListAPI, TaskAPI

api = Api()

api.add_resource(DataAPI, "/data/<int:data_id>")

api.add_resource(GlobalDataListAPI, "/global-data")
api.add_resource(GlobalDataAPI, "/global-data/<int:data_id>")
api.add_resource(GlobalDataAccessAPI, "/global-data/<int:data_id>/permissions")

api.add_resource(GeoDataListAPI, "/geo-data")
api.add_resource(
    GeoDataVectorTileAPI,
    "/geo-data/<int:geo_data_id>/tiles/<int:z>/<int:x>/<int:y>",
)
api.add_resource(GeoDataAPI, "/geo-data/<int:geo_data_id>")
api.add_resource(GeoDataAccessAPI, "/geo-data/<int:geo_data_id>/permissions")
api.add_resource(GeoDataDownloadAPI, "/geo-data/<int:geo_data_id>/export")
api.add_resource(GeoDataUploadAPI, "/geo-data/upload")
api.add_resource(GeoDataUploadRasterAPI, "/geo-data/upload-raster")
api.add_resource(StreamDataListAPI, "/stream-geo-data")
api.add_resource(StreamGeoDataAPI, "/stream-geo-data/<int:stream_geo_data_id>")
api.add_resource(
    StreamGeoDataAccessAPI,
    "/stream-geo-data/<int:stream_geo_data_id>/permissions",
)
api.add_resource(
    StreamGeoDataVersionsAPI,
    "/stream-geo-data/<int:stream_geo_data_id>/versions",
)
api.add_resource(
    StreamGeoDataVersionAPI,
    "/stream-geo-data/<int:stream_geo_data_id>/version/<int:geo_data_id>",
)
api.add_resource(WFSGeoDataUploadAPI, "/stream-geo-data/upload-wfs")
api.add_resource(WMSGeoDataUploadAPI, "/stream-geo-data/upload-wms")
api.add_resource(DataAttributeAPI, "/attribute/<int:attribute_id>")
api.add_resource(FeatureAPI, "/feature/<int:feature_id>")

api.add_resource(ProjectListAPI, "/projects")
api.add_resource(ProjectAPI, "/project/<int:project_id>")
api.add_resource(
    ProjectPermissionsAPI, "/project/<int:project_id>/permissions"
)
api.add_resource(
    ProjectSharedDataListAPI, "/project/<int:project_id>/shared-data"
)
api.add_resource(
    ProjectSharedDataAPI, "/project/<int:project_id>/shared-data/<int:data_id>"
)
api.add_resource(ProjectHierarchyAPI, "/project/<int:project_id>/hierarchy")
api.add_resource(ProjectTaskListAPI, "/project/<int:project_id>/tasks")
api.add_resource(ProjectObjectiveAPI, "/project/<int:project_id>/objective")
api.add_resource(ProjectSharesAPI, "/project/<int:project_id>/shared")
api.add_resource(ProjectTemplateAPI, "/project/<int:project_id>/template")
api.add_resource(TemplateListAPI, "/templates")
api.add_resource(TemplateAPI, "/template/<int:template_id>")

api.add_resource(TaskAPI, "/task/<string:task_id>")

api.add_resource(ProjectDataListAPI, "/project-data")
api.add_resource(ProjectDataAPI, "/project-data/<int:data_id>")
api.add_resource(
    ProjectDataInputListAPI, "/project-data/<int:data_id>/input-data"
)
api.add_resource(
    ProjectDataActiveModelAPI, "/project-data/<int:data_id>/model"
)
api.add_resource(
    ProjectDataModelAPI, "/project-data/<int:data_id>/model/<int:model_id>"
)
api.add_resource(
    ProjectDataModelChangeAPI, "/project-data/<int:data_id>/model/change"
)
api.add_resource(
    MRSortInferenceAPI, "/project-data/<int:data_id>/mr-sort-inference"
)
api.add_resource(
    ProjectDataHierarchyAPI, "/project-data/<int:data_id>/hierarchy"
)
api.add_resource(ShareProjectDataAPI, "/project-data/<int:data_id>/share")
api.add_resource(DataShareAPI, "/shares/<string:uid>")

api.add_resource(ZonePropositionAPI, "/zone-proposition/<int:proposition_id>")
api.add_resource(
    ZonePropositionDownloadAPI, "/zone-proposition/<int:proposition_id>/export"
)

api.add_resource(UserListAPI, "/users")
api.add_resource(LoginAPI, "/login")
api.add_resource(RefreshAPI, "/refresh")
api.add_resource(UserAPI, "/user")
