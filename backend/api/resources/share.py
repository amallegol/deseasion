from flask import jsonify, send_file
from flask_restful import Resource, reqparse

from ..exceptions import RequestError
from ..models import DataShare
from ..schemas import (
    DataShareSchema,
    MessageSchema,
    ProjectSharePostResponseSchema,
    ProjectSharesGetResponseSchema,
)
from ..services import project_data_service, project_service
from ..services.geo_data_loading_service import (
    get_geo_data_download,
    get_temporary_directory,
)
from .permission import token_required
from .utils import (
    dump_data,
    get_json_content,
    with_query_arg,
    with_request_body,
    with_response,
)


class ProjectSharesAPI(Resource):
    @token_required
    @with_response(
        200,
        schema=ProjectSharesGetResponseSchema,
        description="List of data shares of the project",
    )
    def get(self, project_id):
        """
        Get the list of data shares of a project.

        :param int project_id: The id of the project.
        :status 403: The user is not allowed to access the project.
        :status 404: The project does not exist.

        .. :quickref: Project; Get list of data shares
        """
        project = project_service.get_if_authorized(project_id)
        shared = []
        for data in project.data_list:
            for share in data.shares:
                if not share.is_expired():
                    shared.append(share)
        schema = DataShareSchema(many=True)
        return dump_data(schema, shared=shared)


class ShareProjectDataAPI(Resource):
    @token_required
    @with_response(
        200,
        schema=ProjectSharePostResponseSchema,
        description="New data share details",
    )
    @with_request_body(
        schema=DataShareSchema, description="New data share details to create"
    )
    def post(self, data_id):
        """
        Create a new data share.

        :param int data_id: The id of the project data.
        :reqheader Authorization:
            JSON Web Token with Bearer scheme
            (``Authorization: Bearer <token>``).
        :status 403: The user is not allowed to access the project data.
        :status 404: The project data does not exist.

        .. :quickref: Project data; Share data
        """
        data = project_data_service.get_if_authorized(data_id)
        content = get_json_content()
        schema = DataShareSchema()
        share = schema.load(content)
        share.data = data
        share.create()
        return dump_data(schema, share=share)


class DataShareAPI(Resource):
    @with_query_arg(
        "outputFormat",
        str,
        required=False,
        description="Desired output format (leave blank for zip shapefile)",
    )
    @with_response(
        status=200,
        description="Geodata in specified file format",
        content={
            "application/octet-stream": {"type": "string", "format": "binary"}
        },
    )
    def get(self, uid):
        """Return the geometries of a shared data.

        :param str uid: The uid of a data share.
        :status 404: The share does not exist at this time.
        :status 415: Output format is not supported.
        :status 500:
            The geo-data shared don't exist yet or there was an error with the
            geometry while creating the file.

        .. :quickref: Shares; Download a shared geodata
        """
        parser = reqparse.RequestParser()
        parser.add_argument("outputFormat", type=str, location="args")
        args = parser.parse_args()

        share = DataShare.get_by_uid(uid)
        if share is None or share.is_expired():
            raise RequestError("Share not existing", 404)
        geo_data = share.data.data
        with get_temporary_directory() as temp_dir:
            filename, file_object = get_geo_data_download(
                geo_data, temp_dir, args.get("outputFormat")
            )
            return send_file(
                file_object, as_attachment=True, download_name=filename
            )

    @token_required
    @with_response(
        200,
        schema=MessageSchema,
        description="Data share deleted successfully",
    )
    def delete(self, uid):
        """Delete a data share.

        :param str uid: The uid of a data share.
        :status 403: The user cannot delete share on this project.
        :status 404: The share or its project data does not exist.

        .. :quickref: Shares; Delete a data share
        """
        share = DataShare.get_by_uid(uid)
        if share is None or share.is_expired():
            raise RequestError("Share not existing", 404)
        project_data_service.get_if_authorized(share.data_id)
        share.expired = True
        share.update()
        return jsonify(message="share {} set to expired".format(uid))
