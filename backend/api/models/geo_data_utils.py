from collections import defaultdict

import numpy as np
from flask import current_app as app
from geoalchemy2 import Geography, Geometry
from geoalchemy2.shape import from_shape, to_shape
from pandas import DataFrame
from shapely.errors import TopologicalError
from shapely.geometry import MultiPolygon, Polygon
from shapely.ops import unary_union
from shapely.prepared import prep

from ..exceptions import ProcessingError
from . import db
from .geo_data import (
    DataAttributeNominal,
    DataAttributeOrdinal,
    DataAttributeQuantitative,
    ExplainabilityType,
    GeneratedGeoData,
    GeoFeature,
    GlobalData,
)
from .preference import (
    ContinuousRule,
    DiscreteRules,
    GeoBuffer,
    KeepOverlap,
    MRSort,
    PrefDefaultValues,
    WeightedSum,
)
from .preference_model_utils import evaluate_continuous_rule


def check_geom_type(geom):
    result = None
    if geom.type == "Polygon":
        result = geom
    elif (
        geom.type in ("MultiPolygon", "GeometryCollection")
        and not geom.is_empty
    ):
        geoms = []
        for g in geom.geoms:
            new_geom = check_geom_type(g)
            if new_geom is not None:
                geoms.append(new_geom)
        if len(geoms) == 1:
            result = geoms[0]
        elif len(geoms) > 1:
            result = MultiPolygon(geoms)
    if result is not None and result.area <= 1e-08:
        result = None
    if result is not None:
        return result.buffer(0)
    else:
        return Polygon()


class GeometryFeature:
    def __init__(self, ids, geometry):
        self.ids = frozenset(ids)
        self.geometry = geometry

    @classmethod
    def from_dict(cls, value):
        from shapely.wkt import loads

        return cls(ids=value["ids"], geometry=loads(value["geometry"]))

    def to_dict(self):
        from shapely.wkt import dumps

        return {"ids": list(self.ids), "geometry": dumps(self.geometry)}

    def intersection(self, other):
        ids = self.ids | other.ids
        try:
            geometry = self.geometry.intersection(other.geometry)
            return GeometryFeature(ids=ids, geometry=geometry)
        except TopologicalError:
            app.logger.warning(
                "Topological error in the intersection of {} and {}".format(
                    self.ids, other.ids
                )
            )
            return GeometryFeature(ids=ids, geometry=Polygon())

    def difference_union(self, geoms):
        ids = self.ids
        # FIXME: Fix the errors instead of simply ignoring them
        try:
            geom_union = unary_union([g.geometry for g in geoms])
        except ValueError:
            app.logger.warning(
                "Error in the union of geometries of {} and {}".format(
                    ids, [g.ids for g in geoms]
                )
            )
            return GeometryFeature(ids=ids, geometry=Polygon())
        if geom_union.is_empty:
            return GeometryFeature(ids=ids, geometry=self.geometry)
        try:
            geometry = self.geometry.difference(geom_union)
        except TopologicalError:
            app.logger.warning(
                "Topological error in the difference of {} and {}".format(
                    ids, [g.ids for g in geoms]
                )
            )
            return GeometryFeature(ids=ids, geometry=Polygon())
        return GeometryFeature(ids=ids, geometry=geometry)

    def is_valid(self):
        return self.geometry.is_valid

    def is_empty(self):
        return self.geometry.is_empty

    def intersects(self, other):
        return self.geometry.intersects(other.geometry)

    def touches(self, other):
        return self.geometry.touches(other.geometry)


class ModelProcessingService:
    def decompose(self, geometries_1, geometries_2, feedback):
        geoms = []

        count = 0
        total = len(geometries_1) * 2 + len(geometries_2)
        for geom1 in geometries_1:
            prepared_geom1 = prep(geom1.geometry)
            geoms2 = filter(
                lambda g: prepared_geom1.intersects(g.geometry), geometries_2
            )
            for geom2 in geoms2:
                g = geom1.intersection(geom2)
                g.geometry = check_geom_type(g.geometry)
                geoms.append(g)
            count += 1
            feedback.set_progress(count, total)

        for geom1 in geometries_1:
            prepared_geom1 = prep(geom1.geometry)
            geoms2 = filter(
                lambda g: prepared_geom1.intersects(g.geometry), geometries_2
            )
            g = geom1.difference_union(list(geoms2))
            g.geometry = check_geom_type(g.geometry)
            geoms.append(g)
            count += 1
            feedback.set_progress(count, total)

        for geom2 in geometries_2:
            prepared_geom2 = prep(geom2.geometry)
            geoms1 = filter(
                lambda g: prepared_geom2.intersects(g.geometry), geometries_1
            )
            g = geom2.difference_union(list(geoms1))
            g.geometry = check_geom_type(g.geometry)
            geoms.append(g)
            count += 1
            feedback.set_progress(count, total)

        new_geoms = []
        for geom in geoms:
            g = check_geom_type(geom.geometry)
            if g is not None and not g.is_empty:
                geom.geometry = g
                new_geoms.append(geom)
        geoms = new_geoms
        return geoms

    def _decompose_geometries(self, *geometries):
        """Decompose the geometries.

        Args:
            geometries (list): List of GeometryFeature objects.
        """
        result = geometries[0]
        for geoms in geometries[1:]:
            result = self.decompose(result, geoms)
        return result

    def buffer_geometries(self, distance, geometries):
        """Create a buffer around the geometries.

        The geometries are cast to a geography type in PostGIS, allowing to use
        a distance in metres for the buffer.

        Args:
            distance: The distance of the buffer.
            geometries (iterable): Iterable of GeometryFeature objects

        Returns:
            A new list of GeometryFeature objects with a buffer around the
            geometries.
        """
        q = db.select(
            db.cast(
                db.func.st_buffer(
                    db.cast(db.column("geom"), Geography), distance
                ),
                Geometry,
            )
        )
        q = q.select_from(
            db.func.unnest([g.geometry.wkt for g in geometries]).alias("geom")
        )
        result = db.session.execute(q).fetchall()
        buffered = [
            GeometryFeature(g.ids, to_shape(res[0]))
            for g, res in zip(geometries, result)
        ]
        return buffered

    def _get_default_values(self, model):
        values = {}
        for d in model.input_data:
            if d.data is not None:
                attrs = d.data.attributes
                attributes = {}
                for attr in attrs:
                    value = None
                    if isinstance(model, PrefDefaultValues):
                        # replace by default values
                        value = model.get_default_value(attr)
                    attributes[attr.name] = value
                values[d.name] = {
                    "properties": {"area": None},
                    "attributes": attributes,
                }
        return values

    def _get_global_properties(self, model):
        global_data = {}
        for d in model.input_data:
            if isinstance(d.data, GlobalData):
                props = d.data.properties
                global_data[d.name] = {
                    "properties": {},
                    "attributes": {
                        prop.attribute.name: prop.value for prop in props
                    },
                }
        return global_data

    def _get_sandbox_data(self, model, geometries):
        features_ids = frozenset.union(*(g.ids for g in geometries))
        q = db.session.query(
            GeoFeature, db.func.st_area(db.cast(GeoFeature.geom, Geography))
        )
        q = q.filter(GeoFeature.id.in_(list(features_ids)))
        q_result = q.all()

        features_data = {}
        project_data_names = {
            d.data.id: d.name for d in model.input_data if d.data is not None
        }
        default_values = self._get_default_values(model)

        for f, area in q_result:
            properties = {"area": area}
            attributes = {p.attribute.name: p.value for p in f.properties}
            if isinstance(model, ContinuousRule):
                # replace None by default values
                for p in f.properties:
                    if p.value is None:
                        attr = p.attribute
                        attributes[attr.name] = model.get_default_value(attr)
            features_data[f.id] = {
                "name": project_data_names[f.data.id],
                "data": {"properties": properties, "attributes": attributes},
            }

        global_data = self._get_global_properties(model)

        process_list = []
        areas = self.get_geometries_area(geometries)
        for g, area in zip(geometries, areas):
            data = {}
            data[model.data_generator.name] = {
                "properties": {"area": area},
                "attributes": {},
            }
            for f_id in g.ids:
                feature = features_data[f_id]
                data[feature["name"]] = feature["data"]
            data = {**data, **global_data}
            for k, v in default_values.items():
                if k not in data:
                    data[k] = v
            process_list.append(data)

        return process_list

    def _get_attributes_stats(self, model):
        """
        Return the statistics of the input data attributes.

        The statistics are returned as a dictionary containing the stats in the
        form {'data name': {'attribute name': {'stat1': value, 'stat2': value,
        ...}}}

        The returned statistics are 'min', 'max', 'mean', 'std' and 'count' if
        they already exist in the attribute.
        Percentiles are also returned from 'percentile_0' to 'percentile_100'.
        """
        input_attributes = []
        for data in model.input_data:
            if data.data is not None:
                input_attributes += data.data.attributes
        stats = defaultdict(dict)
        for attr in input_attributes:
            keys = ["min", "max", "mean", "std", "count"]
            if attr.statistics is not None:
                attr_stat = {}
                for stat_key, stat_val in attr.statistics.items():
                    if stat_key in keys:
                        keys.remove(stat_key)
                        attr_stat[stat_key] = stat_val
                try:
                    df = DataFrame([v.value for v in attr.values])
                    series = df.quantile(np.arange(0, 1.01, 0.01))[0]
                    for k, v in series.items():
                        attr_stat["percentile_{}".format(int(k * 100))] = v
                except (TypeError, ValueError):
                    app.logger.info(
                        "The attribute is not a number, percentiles ignored"
                    )
                stats[attr.data.name][attr.name] = attr_stat
        return dict(stats) if len(stats) > 0 else None

    def _get_inference_data(self, model, geometries):
        features_ids = frozenset.union(*(g.ids for g in geometries))
        q_result = GeoFeature.query.filter(
            GeoFeature.id.in_(list(features_ids))
        ).all()
        features = {f.id: f for f in q_result}
        attributes = []
        for d in model.input_data:
            if d.data is not None:
                attributes += d.data.attributes
        data_list = []
        for geometry in geometries:
            data = {a: None for a in attributes}
            for f_id in geometry.ids:
                feature = features.get(f_id)
                for value in feature.properties:
                    data[value.attribute] = value.value
            data_list.append(data)
        return data_list

    def _get_result_attributes(self, result_list, model):
        old_attrs = {}
        if model.data_generator.data is not None:
            old_attrs = {
                a.name: a for a in model.data_generator.data.attributes
            }
        is_quantitative = (
            lambda v: isinstance(v, int) or isinstance(v, float) or v is None
        )
        attrs = {}
        for properties in result_list:
            for name, value in properties.items():
                if name not in attrs:
                    if is_quantitative(value):
                        new = DataAttributeQuantitative(name=name)
                    else:
                        new = DataAttributeNominal(name=name)
                    if name in old_attrs:
                        old = old_attrs[name]
                        if new.type == old.type:
                            new = old
                    attrs[name] = new
                if isinstance(
                    attrs[name], DataAttributeQuantitative
                ) and not is_quantitative(value):
                    attrs[name] = DataAttributeNominal(name=name)
        return attrs

    def get_decomposed_data(self, model):
        # TODO: Find a cleaner way to get the decomposed data values
        geometries = self.process_model_geometries(model)
        return self._get_inference_data(model, geometries)

    def _process_rule(self, model, geometries):
        """Process a rule model."""
        data_json = self._get_sandbox_data(model, geometries)
        data_stats = self._get_attributes_stats(model)
        result_list = evaluate_continuous_rule(
            data=data_json, rule=model.rule, stats=data_stats
        )
        attrs = self._get_result_attributes(result_list, model)
        new_features = []
        for result, g, data in zip(result_list, geometries, data_json):
            values = []
            explainability = {
                "pref_type": model.pref_type.name,
                "disaggregation": [],
            }
            for d_name, props in data.items():
                if d_name == model.data_generator.name:
                    continue
                for prop, val in props["attributes"].items():
                    # if isinstance(val, numbers.Number):
                    explainability["disaggregation"].append(
                        {"data": d_name, "attribute": prop, "value": val}
                    )
            new_feature = GeoFeature(
                geom=from_shape(g.geometry, srid=4326),
                explainability=explainability,
            )
            for name, value in result.items():
                # Ignore null values
                if value is None:
                    continue
                attr = attrs[name]
                cls = attr.get_value_class()
                values.append(
                    cls(value=value, attribute=attr, feature=new_feature)
                )
            # If the feature has values
            if len(values) > 0:
                new_features.append(new_feature)
        return new_features

    def _process_model_buffer(self, model, geometries):
        """Copy the attributes from the geometries."""
        features_ids = frozenset.union(*(g.ids for g in geometries))
        features = GeoFeature.query.filter(
            GeoFeature.id.in_(list(features_ids))
        ).all()
        geo_data = None
        for f in features:
            if geo_data is None:
                geo_data = f.data
            else:
                assert geo_data is f.data
        old_attrs = {}
        if model.data_generator.data is not None:
            old_attrs = {
                a.name: a for a in model.data_generator.data.attributes
            }
        attrs = {}
        for a in geo_data.attributes:
            old_attr = old_attrs.get(a.name)
            attrs[a.id] = old_attr if a.same_as(old_attr) else a.new_copy()

        features_dict = {f.id: f for f in features}
        new_features = []
        for g in geometries:
            f_id = list(g.ids)[0]  # Copy attributes from the first geometry
            old_feature = features_dict[f_id]
            explainability = {
                "pref_type": model.pref_type.name,
                "disaggregation": [],
            }
            for prop in old_feature.properties:
                # if isinstance(prop.attribute, DataAttributeQuantitative):
                explainability["disaggregation"].append(
                    {
                        "data": model.data_generator.name,
                        "attribute": prop.attribute.name,
                        "value": prop.value,
                    }
                )
            new_feature = GeoFeature(
                geom=from_shape(g.geometry, srid=4326),
                explainability=explainability,
            )
            new_props = []
            for v in old_feature.properties:
                cls = attrs[v.attribute_id].get_value_class()
                val = cls(
                    value=v.value,
                    attribute=attrs[v.attribute_id],
                    feature=new_feature,
                )
                new_props.append(val)
            new_features.append(new_feature)
        return new_features

    def _process_criteria_rules(self, model, geometries):
        """Process criteria rules.

        A category will be assigned for each of the resulting features, in a
        'category' attribute.

        Returns:
            A list of Feature objects.
        """
        data_json = self._get_sandbox_data(model, geometries)
        data_stats = self._get_attributes_stats(model)
        results = []
        for category in model.categories:
            evaluation = category.evaluate_data(data_json, data_stats)
            results.append((category.name, category.rules, evaluation))
        final = np.ma.masked_all_like(data_json)
        final_rule = np.ma.masked_all_like(data_json)
        for categ, rules, evaluation in results:
            for rule, row in zip(rules, evaluation):
                mask = np.where(row & final.mask)
                final[mask] = categ
                final_rule[mask] = rule
        if np.ma.is_masked(final):
            raise ProcessingError("No rule validated")
        # TODO: Send a warning if several categories are valid
        # (r.count(True) > 1)
        order = [c.name for c in model.categories]
        attribute = DataAttributeOrdinal(name="category", order=order)
        if model.data_generator.data is not None:
            for attr in model.data_generator.data.attributes:
                if attribute.same_as(attr):
                    attribute = attr
                    break
        new_features = []
        for categ, rule, g, data in zip(
            final, final_rule, geometries, data_json
        ):
            explainability = {
                "pref_type": model.pref_type.name,
                "rule": rule,
                "disaggregation": [],
            }
            for d_name, props in data.items():
                if d_name == model.data_generator.name:
                    continue
                for prop, val in props["attributes"].items():
                    # if isinstance(val, numbers.Number):
                    explainability["disaggregation"].append(
                        {"data": d_name, "attribute": prop, "value": val}
                    )
            new_feature = GeoFeature(
                geom=from_shape(g.geometry, srid=4326),
                explainability=explainability,
            )
            cls = attribute.get_value_class()
            props = [
                cls(value=categ, attribute=attribute, feature=new_feature)
            ]
            new_features.append(new_feature)
        return new_features

    def _process_mr_sort(self, model, geometries):
        """Process using the MR-Sort algorithm.

        Each resulting feature will be assigned a category by the MR-Sort
        algorithm, in a 'category' attribute.

        Returns:
            A list of Feature objects.
        """
        data_json = self._get_sandbox_data(model, geometries)
        new_features = []
        order = [c.name for c in model.categories]
        attribute = DataAttributeOrdinal(name="category", order=order)
        if model.data_generator.data is not None:
            for attr in model.data_generator.data.attributes:
                if attribute.same_as(attr):
                    attribute = attr
                    break
        profiles = {}
        for criterion in model.criteria:
            for index, value in enumerate(criterion.profiles):
                if index not in profiles:
                    profiles[index] = []
                profiles[index].append((criterion.attribute.name, value))
        for data, g in zip(data_json, geometries):
            attributes = []
            explainability = {
                "pref_type": model.pref_type.name,
                "disaggregation": [],
                "lower": None,
                "upper": None,
            }
            for d_name, props in data.items():
                if d_name == model.data_generator.name:
                    continue
                for prop, val in props["attributes"].items():
                    attributes.append((d_name, prop, val))
                    # if isinstance(val, numbers.Number):
                    explainability["disaggregation"].append(
                        {"data": d_name, "attribute": prop, "value": val}
                    )
            category, exp = model.check_category(attributes)
            for k, profile_exp in exp.items():
                explainability[k] = (
                    None
                    if profile_exp is None
                    else [
                        {
                            "attribute": attr,
                            "value": val,
                            "weight": profile_exp["weights"].get(attr, 0),
                        }
                        for attr, val in profiles[profile_exp["profile"]]
                    ]
                )
            new_feature = GeoFeature(
                geom=from_shape(g.geometry, srid=4326),
                explainability=explainability,
            )
            cls = attribute.get_value_class()
            props = [
                cls(
                    value=category.name,
                    attribute=attribute,
                    feature=new_feature,
                )
            ]
            new_features.append(new_feature)
        return new_features

    def _process_weighted_sum(self, model, geometries):
        features_ids = frozenset.union(*(g.ids for g in geometries))
        old_features = GeoFeature.query.filter(
            GeoFeature.id.in_(list(features_ids))
        ).all()
        old_features_dict = {f.id: f for f in old_features}
        new_features = []
        attribute = DataAttributeQuantitative(name="result")
        if model.data_generator.data is not None:
            for attr in model.data_generator.data.attributes:
                if attribute.same_as(attr):
                    attribute = attr
                    break
        default_values = {
            d.attribute_id: d.value for d in model.default_values
        }
        for geom in geometries:
            value = None
            explainability = {
                "pref_type": model.pref_type.name,
                "disaggregation": [],
            }
            features = (old_features_dict[_id] for _id in geom.ids)
            vals = {
                p.attribute_id: p.value for f in features for p in f.properties
            }
            for operand in model.operands:
                new_val = vals.get(operand.attribute_id)
                if new_val is None:
                    new_val = default_values.get(operand.attribute_id)
                if new_val is not None:
                    explainability["disaggregation"].append(
                        {
                            "data": operand.attribute.data.name,
                            "attribute": operand.attribute.name,
                            "value": new_val,
                            "weight": operand.weight,
                        }
                    )
                    new_val = operand.weight * new_val
                    value = new_val if value is None else value + new_val
                else:
                    # Ignore the feature if there is no default value
                    value = None
                    break
            if value is not None:
                new_feature = GeoFeature(
                    geom=from_shape(geom.geometry, srid=4326),
                    explainability=explainability,
                )
                cls = attribute.get_value_class()
                props = [  # noqa: F841 (those props are persisted by ORM)
                    cls(value=value, attribute=attribute, feature=new_feature)
                ]
                new_features.append(new_feature)
        return new_features

    def _decompose_features(self, features, feedback):
        def id_generator():
            num = 0
            while True:
                yield num
                num += 1

        feedback.set_message("Decomposing overlaps")
        gen_id = id_generator()
        geoms = {next(gen_id): to_shape(f.geom) for f in features}
        new_geoms = {}
        for i, f1id in enumerate(geoms):
            f1geom = geoms[f1id]
            new_geoms[f1id] = f1geom
            new_geoms_tmp = new_geoms.copy()
            for f2id, f2geom in new_geoms.items():
                if f1id == f2id:
                    continue
                if f1geom.intersects(f2geom):
                    try:
                        intersection = check_geom_type(
                            f1geom.intersection(f2geom)
                        )
                    except TopologicalError:
                        intersection = None
                        app.logger.warning("Intersection error in the overlap")
                    if intersection is not None and not intersection.is_empty:
                        new_geoms_tmp[next(gen_id)] = intersection

                    try:
                        g2diff = check_geom_type(f2geom.difference(f1geom))
                    except TopologicalError:
                        g2diff = None
                        app.logger.warning("Difference error in the overlap")
                    if g2diff is not None and not g2diff.is_empty:
                        new_geoms_tmp[f2id] = g2diff
                    else:
                        del new_geoms_tmp[f2id]

                    try:
                        g1diff = check_geom_type(f1geom.difference(f2geom))
                    except TopologicalError:
                        g1diff = None
                        app.logger.warning("Difference error in the overlap")
                    if g1diff is not None and not g1diff.is_empty:
                        new_geoms_tmp[f1id] = g1diff
                        f1geom = g1diff
                    else:
                        del new_geoms_tmp[f1id]
                        f1geom = None
                        break
            new_geoms = new_geoms_tmp
            feedback.set_progress(i, len(geoms))
            if f1geom is None:
                continue
        feedback.forget_message()
        polys = list(new_geoms.values())
        return polys

    def process_model_geometries(self, model, feedback=False):
        """Return the processed geometries.

        Decompose the geometries or create a buffer around the geometries,
        depending on the preference model.

        Returns:
            A list of GeometryFeature.
        """
        if isinstance(model, GeoBuffer):
            return self._process_buffer_geometry(model)
        else:
            return self._process_regular_geometries(model, feedback=feedback)

    def _process_buffer_geometry(self, model):
        """Create a buffer around the geometries.

        Returns:
            A list of GeometryFeature.
        """
        # Prepare the extent
        extent = to_shape(model.data_generator.project.extent)
        extent_prep = prep(extent)
        # Check the input data is correct
        if len(model.input_data) != 1:
            raise ProcessingError("Incorrect number of input data")
        input_data = model.input_data[0]
        if input_data.data is None:
            raise ProcessingError("No geometries for the input data")
        # Convert the features to Shapely geometries
        geoms = []
        for feature in input_data.data.features:
            geom = to_shape(feature.geom)
            geoms.append(GeometryFeature(ids=[feature.id], geometry=geom))
        # Buffer
        geometries = self.buffer_geometries(model.radius, geoms)
        # Filter the geometries that intersect with the extent
        filtered = []
        for geom in geometries:
            if extent_prep.intersects(geom.geometry):
                if model.cut_to_extent:
                    geom.geometry = geom.geometry.intersection(extent)
                filtered.append(geom)
        return filtered

    def _process_regular_geometries(self, model, feedback):
        """Decompose the geometries.

        Returns:
            A list of GeometryFeature.
        """
        # Prepare the extent
        extent = to_shape(model.data_generator.project.extent)
        extent_prep = prep(extent)
        # Convert the features to Shapely geometries and cut them to the extent
        # if needed
        input_geometries = []
        geo_data_list = (
            i.data for i in model.input_data if i.data is not None
        )
        for data in geo_data_list:
            if isinstance(data, GlobalData):
                continue
            geoms = []
            for feature in data.features:
                geom = to_shape(feature.geom)
                if extent_prep.intersects(geom) and not geom.is_empty:
                    if model.cut_to_extent:
                        geom = geom.intersection(extent)
                    geoms.append(
                        GeometryFeature(ids=[feature.id], geometry=geom)
                    )
            input_geometries.append(geoms)
        # Check the input data exists
        if len(input_geometries) == 0:
            raise ProcessingError("No input data exist")
        # Decompose the geometries
        geometries = input_geometries[0]
        for i, geoms in enumerate(input_geometries[1:]):
            feedback.set_message(
                "Decomposing geometries ({}/{})".format(
                    i + 1, len(input_geometries) - 1
                )
            )
            geometries = self.decompose(geometries, geoms, feedback=feedback)
        return geometries

    def _process_overlap(self, model, features, feedback):
        if model.keep_overlap == KeepOverlap.all:
            return features
        new_features = []
        polys = self._decompose_features(features, feedback=feedback)
        input_features = []
        for feature in features:
            input_features.append((to_shape(feature.geom), feature.properties))
        attrs = defaultdict(list)
        for poly in polys:
            explainability = {
                "pref_type": model.keep_overlap.name,
                "aggregated": [],
            }
            new_feature = GeoFeature(geom=from_shape(poly, srid=4326))
            cent = prep(poly.representative_point())
            intersecting = filter(
                lambda f: cent.intersects(f[0]), input_features
            )
            tmp_values = defaultdict(list)
            for geom, properties in intersecting:
                for val in properties:
                    attr = val.attribute
                    cls = val.__class__
                    tmp_values[attr].append(cls(value=val.value))
            new_feature.explainability = explainability
            values = []
            for attr, vals in tmp_values.items():
                if len(vals) >= 1:
                    try:
                        val = model.get_overlap_value(vals)
                        val.feature = new_feature
                        attrs[attr].append(val)
                        explainability["aggregated"].append(
                            {
                                "attribute": attr.name,
                                "values": [v.value for v in vals],
                            }
                        )
                    except TypeError:
                        raise ProcessingError(
                            "Error overlapping the values types."
                        )
            new_features.append(new_feature)
        for attr, values in attrs.items():
            attr.values = values
        return new_features

    def process_model(self, model, geometries, feedback):
        """Process the model on the geometries.

        Args:
            model: A preference model.
            geometries (list): A list of GeometryFeature objects.

        Returns:
            A list of Feature objects.
        """
        features = []
        feedback.set_message("Processing model")
        if isinstance(model, GeoBuffer):
            features = self._process_model_buffer(model, geometries)
        elif isinstance(model, ContinuousRule):
            features = self._process_rule(model, geometries)
        elif isinstance(model, DiscreteRules):
            features = self._process_criteria_rules(model, geometries)
        elif isinstance(model, MRSort):
            features = self._process_mr_sort(model, geometries)
        elif isinstance(model, WeightedSum):
            features = self._process_weighted_sum(model, geometries)
        else:
            raise ValueError
        feedback.set_message("Processing overlaps")
        features = self._process_overlap(model, features, feedback=feedback)
        feedback.set_message("Dissolving geometries")
        features = self._process_dissolve(model, features)
        return features

    def set_model_features(self, model, features, filter_none=True):
        """Save the features.

        If a geo data already exists, the old features are replaced by the
        features given in arguements.
        If no geo data exists, a new one is created using the features.

        Args:
            model (PreferenceModel)
            features (list of GeoFeature)
            filter_none (bool): Remove the features with only None values
                (default: True)
        """
        if filter_none:
            # Remove features without values
            features_original = list(features)
            features = []
            for feat in features_original:
                if any((v.value is not None for v in feat.properties)):
                    features.append(feat)

        # Remove features with empty geometries
        features = list(
            filter(lambda f: to_shape(f.geom).is_empty is False, features)
        )

        # List the data attributes
        attrs = []
        for f in features:
            for p in f.properties:
                if p.attribute not in attrs:
                    attrs.append(p.attribute)

        if model.data_generator.data is None:
            # Create a new data...
            model.data_generator.data = GeneratedGeoData(
                name=model.data_generator.name,
                features=features,
                attributes=attrs,
            )
        else:
            # ...or modify the old one
            data = model.data_generator.data
            data.name = model.data_generator.name
            data.features = features
            data.extent = None
            data.attributes = attrs
        model.data_generator.data.update()
        model.data_generator.update_modification()
        model.data_generator.update()

    def get_geometries_area(self, geometries):
        """Return the areas of the geometries.

        Calculates the area in PostGIS by casting the geometries to Geography
        data, allowing to have the area in square metres.

        Args:
            geometries (list): List of GeometryFeature objects.
        """
        q = db.select(db.func.st_area(db.cast(db.column("geom"), Geography)))
        q = q.select_from(
            db.func.unnest([g.geometry.wkt for g in geometries]).alias("geom")
        )
        q_result = db.session.execute(q).fetchall()
        areas = [r[0] for r in q_result]
        return areas

    def _process_dissolve(self, model, features):
        """Dissolve the adjacent geometries with the same attributes.

        Does not do anything if the dissolve_adjacent attribute of the model is
        not set. Otherwise, if two geometries touch each other and have the
        same values for their attributes, they will be merged in a single
        geometry.
        """
        if not model.dissolve_adjacent:
            return features

        # Group the geometries when the features have identical attributes
        # values
        # keys are a tuple of (attribute, value) and values are lists of
        # shapely geometries
        grouped = defaultdict(list)
        for feature in features:
            f_key = tuple(
                (prop.attribute, prop.value) for prop in feature.properties
            )
            shape = to_shape(feature.geom)
            grouped[f_key].append(shape)
        # Create the new features
        new_features = []
        attrs = defaultdict(list)
        for key, geoms in grouped.items():
            # Union of each group of geometries
            union = unary_union(geoms)
            # Create explainability field for merged feature
            explainability = {
                "pref_type": ExplainabilityType.merged_features.name,
            }
            # If the union creates a multi-polygon, separate it in a list of
            # polygons
            assert union.geom_type in ("MultiPolygon", "Polygon")
            new_geoms = (
                list(union.geoms)
                if union.geom_type == "MultiPolygon"
                else [union]
            )
            for new_geom in new_geoms:
                new_feature = GeoFeature(
                    geom=from_shape(new_geom, srid=4326),
                    explainability=explainability,
                )
                for attr, value in key:
                    cls = attr.get_value_class()
                    val = cls(value=value, attribute=attr, feature=new_feature)
                    attrs[attr].append(val)
                new_features.append(new_feature)
        for attr, values in attrs.items():
            attr.values = values
        return new_features

    def dissolve_adjacent_features(self, data_id):
        """Dissolve the adjacent features with the same attributes values."""
        q = "SELECT dissolve_adjacent(:data_id)"
        keys = {"data_id": data_id}
        db.session.execute(q, keys)
