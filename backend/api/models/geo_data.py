import enum
import numbers
from copy import copy, deepcopy
from datetime import datetime
from functools import total_ordering
from typing import Type

import simplejson as json
from geoalchemy2.shape import to_shape
from geoalchemy2.types import Geometry
from pandas import DataFrame
from shapely.geometry import mapping
from sqlalchemy.dialects.postgresql import ARRAY, JSONB
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property

from . import db
from .mixins import ModelMixin, TimestampMixin
from .permission import HasPermissions


class StreamDataType(enum.Enum):
    wfs = 1
    wms = 2


# todo: Make a Mixin with common fields between StreamGeoData/GeoData?
class StreamGeoData(HasPermissions, TimestampMixin, db.Model):
    type = db.Column(db.Enum(StreamDataType))
    name = db.Column(db.String, nullable=False)
    title = db.Column(db.String)
    original_name = db.Column(db.String)
    url = db.Column(db.String, nullable=False)
    upload_user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    description = db.Column(db.String)
    keywords = db.Column(ARRAY(db.String, dimensions=1))

    versions = db.relationship(
        "GeoData",
        back_populates="stream",
        cascade="all",
        order_by="GeoData.created_at",
    )

    _upload_user = db.relationship("User")

    __mapper_args__ = {"polymorphic_on": type}

    def __init__(
        self,
        name: str,
        url: str,
        original_name: str = None,
        is_public: bool = False,
        title: str = None,
        description: str = None,
        keywords: list = None,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.name = name
        self.url = url
        self.original_name = original_name
        self.title = title
        self.description = description
        self.keywords = keywords if keywords else []
        self.is_public = is_public

    @hybrid_property
    def upload_user(self):
        return self._upload_user

    @upload_user.setter
    def upload_user(self, user):
        """Make sure that the user has permissions on the data."""
        if user is not None:
            self.permissions.append(StreamGeoData.Permission(user=user))
        self._upload_user = user
        for geodata in self.versions:
            geodata.upload_user = user


class WFSGeoData(StreamGeoData):
    __tablename__ = "wfs_geo_data"
    id = db.Column(
        db.Integer, db.ForeignKey("stream_geo_data.id"), primary_key=True
    )
    feature_type = db.Column(db.String, nullable=False)
    version = db.Column(db.String)

    __mapper_args__ = {"polymorphic_identity": StreamDataType.wfs}

    def __init__(
        self,
        url: str,
        feature_type: str,
        name: str = None,
        original_name: str = None,
        is_public: bool = False,
        title: str = None,
        description: str = None,
        keywords: list = None,
        **kwargs,
    ):
        super().__init__(
            url=url,
            name=(name or feature_type),
            original_name=original_name,
            is_public=is_public,
            title=title,
            description=description,
            keywords=keywords,
            **kwargs,
        )
        self.feature_type = feature_type


class WMSGeoData(StreamGeoData):
    __tablename__ = "wms_geo_data"
    id = db.Column(
        db.Integer, db.ForeignKey("stream_geo_data.id"), primary_key=True
    )
    classes = db.Column(ARRAY(db.Float, dimensions=2))
    layer = db.Column(db.String, nullable=False)
    resolution = db.Column(db.Float)
    version = db.Column(db.String)

    __mapper_args__ = {"polymorphic_identity": StreamDataType.wms}

    def __init__(
        self,
        url: str,
        layer: str,
        name: str = None,
        original_name: str = None,
        is_public: bool = False,
        title: str = None,
        description: str = None,
        keywords: list = None,
        classes: list[list[float]] = None,
        **kwargs,
    ):
        super().__init__(
            url=url,
            name=(name or layer),
            original_name=original_name,
            is_public=is_public,
            title=title,
            description=description,
            keywords=keywords,
            **kwargs,
        )
        self.layer = layer
        self.classes = classes


class GeoDataType(enum.Enum):
    geo_data = 1
    generated_geo_data = 2
    proposition_geo_data = 3
    global_data = 4


class BaseData(TimestampMixin, db.Model):
    type = db.Column(db.Enum(GeoDataType))  # inheritance discriminator
    name = db.Column(db.String, nullable=False)
    _properties_modified_at = db.Column("properties_modified_at", db.DateTime)

    attributes = db.relationship(
        "DataAttribute", back_populates="data", cascade="all,delete-orphan"
    )

    features = db.relationship(
        "Feature", back_populates="data", cascade="all,delete-orphan"
    )

    __mapper_args__ = {"polymorphic_on": type}

    def __init__(self, name, features=[], **kwargs):
        super().__init__(**kwargs)
        self.name = name
        self.features = features

    def __repr__(self):
        return "<BaseData(name={})>".format(self.name)

    def __deepcopy__(self, memo):
        cls = type(self)
        other = cls(
            name=self.name,
        )
        memo[id(self)] = other
        return other

    def load_properties(self, force=False):
        """Load the model properties and save them to the database

        Args:
            force (bool): to force the loading of the properties
        """
        if (
            force
            or self.modified_at is None
            or self._properties_modified_at is None
            or self._properties_modified_at < self.modified_at
        ):
            for attribute in self.attributes:
                attribute.load_statistics()
            self._properties_modified_at = datetime.utcnow()
            self.save()


class BaseGeoData(BaseData):
    extent = db.Column(Geometry)

    @declared_attr
    def features(cls):
        return db.relationship(
            "GeoFeature", back_populates="data", cascade="all,delete-orphan"
        )

    __mapper_args__ = {"polymorphic_abstract": True}

    def __repr__(self):
        return "<BaseGeoData(name={})>".format(self.name)

    def __deepcopy__(self, memo):
        cls = type(self)
        other = cls(
            name=self.name,
        )
        memo[id(self)] = other
        other.features = [deepcopy(f, memo) for f in self.features]
        return other

    def as_geojson(self):
        features_geojson = [f.as_geojson() for f in self.features]
        collection = {
            "type": "FeatureCollection",
            "features": features_geojson,
        }
        return collection

    def load_extent(self):
        self.extent = (
            db.session.query(
                db.func.st_envelope(db.func.st_collect(GeoFeature.geom))
            )
            .filter_by(data_id=self.id)
            .one()[0]
        )

    def load_properties(self, force=False):
        """Load the model properties and save them to the database

        Args:
            force (bool): to force the loading of the properties
        """
        if (
            force
            or self.modified_at is None
            or self._properties_modified_at is None
            or self._properties_modified_at < self.modified_at
        ):
            self.load_extent()
            for attribute in self.attributes:
                attribute.load_statistics()
            self._properties_modified_at = datetime.utcnow()
            self.save()


class UploadableData:
    upload_user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    @declared_attr
    def _upload_user(cls):
        return db.relationship("User")

    @hybrid_property
    def upload_user(self):
        return self._upload_user

    @upload_user.setter
    def upload_user(self, user):
        """Make sure that the user has permissions on the data."""
        self._upload_user = user
        if user is not None:
            self.permissions.append(type(self).Permission(user=user))


class GlobalData(HasPermissions, UploadableData, BaseData):
    __tablename__ = "global_data"

    id = db.Column(db.Integer, db.ForeignKey("base_data.id"), primary_key=True)
    feature_id = db.Column(db.Integer, db.ForeignKey("feature.id"))
    feature = db.relationship(
        "Feature",
        cascade="all",
        primaryjoin="global_data.c.feature_id == feature.c.id",
        post_update=True,
    )
    description = db.Column(db.String)
    properties = db.relationship(
        "DataValue",
        cascade="all",
        primaryjoin=(
            "global_data.c.feature_id == foreign(data_value.c.feature_id)"
        ),
        post_update=True,
    )

    __mapper_args__ = {"polymorphic_identity": GeoDataType.global_data}

    def __init__(self, feature, **kwargs):
        super().__init__(**kwargs)
        self.feature = feature

    def __repr__(self):
        properties = {val.attribute.name: val.value for val in self.properties}
        return f"<GlobalData(name={self.name}, properties={properties})>"

    def get_property(self, prop):
        """Returns the value of the property"""
        return self.feature.get_property(prop)

    def _choose_attribute_type(self, value) -> Type["DataAttribute"]:
        """Create new DataValue and DataAttribute based on value.

        :param prop: property name (attribute name)
        :param value:
        :return: created data value object (not persisted yet!)
        """
        match value:
            case int() | float():
                return DataAttributeQuantitative
            case _:
                return DataAttributeNominal

    def set_property(self, prop, value):
        """Set property value.

        Try to reuse existing attribute if type matches.

        :param prop: property name (attribute name)
        :param value: new property value
        """
        old_prop = None
        for p in self.properties:
            if p.attribute.name == prop:
                old_prop = p
                break
        attr_type = self._choose_attribute_type(value)
        if old_prop is None or not isinstance(old_prop.attribute, attr_type):
            attr = attr_type(name=prop, data=self)
            db.session.add(attr)
        else:
            attr = old_prop.attribute
            db.session.delete(old_prop)
        db.session.add(
            attr.get_value_class()(
                value=value, attribute=attr, feature=self.feature
            )
        )
        db.session.commit()


class GeoData(HasPermissions, UploadableData, BaseGeoData):
    """Table for the geographical data loaded from files.

    Attributes:
        original_name (str): The name of the file.
        source_driver (str): Which format the original data used.
        upload_user_id (int): Reference to the user who uploaded the data.
        description (str): A description text.
    """

    __tablename__ = "geo_data"

    id = db.Column(db.Integer, db.ForeignKey("base_data.id"), primary_key=True)
    original_name = db.Column(db.String)
    source_driver = db.Column(db.String)
    description = db.Column(db.String)

    stream_id = db.Column(db.Integer, db.ForeignKey("stream_geo_data.id"))
    stream = db.relationship("StreamGeoData", back_populates="versions")

    __mapper_args__ = {"polymorphic_identity": GeoDataType.geo_data}

    def __init__(
        self,
        *args,
        original_name=None,
        source_driver=None,
        is_public=False,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.original_name = original_name
        self.source_driver = source_driver
        self.is_public = is_public

    def __deepcopy__(self, memo):
        print("{}".format(self.name))
        memo[id(self)] = self
        return self

    def __repr__(self):
        return "<GeoData(name={}, source_driver={})>".format(
            self.name, self.source_driver
        )

    def pull_stream_metadata(self):
        """Replace metadata with those taken from stream.

        Fields pulled:
            * name
            * original_name
            * description
        """
        if self.stream is None:
            return
        self.original_name = self.stream.original_name
        self.name = self.stream.name
        self.description = self.stream.description


class GeneratedGeoData(BaseGeoData):
    """Table for the geographical data generated by the preference model in the
    projects."""

    __mapper_args__ = {"polymorphic_identity": GeoDataType.generated_geo_data}

    project_data = db.relationship("DataGenerator", uselist=False)

    def __repr__(self):
        return "<GeneratedGeoDate(name={})>".format(self.name)

    def __deepcopy__(self, memo):
        other = super().__deepcopy__(memo)
        other.features = [deepcopy(f, memo) for f in self.features]
        other.project_data = deepcopy(self.project_data, memo)
        return other


class PropositionGeoData(BaseGeoData):
    """Table for the geographical data proposed by the genetic algorithm."""

    __mapper_args__ = {
        "polymorphic_identity": GeoDataType.proposition_geo_data
    }

    def __repr__(self):
        return "<PropositionGeoData(name={s.name})>".format(s=self)


class AttributeType(enum.Enum):
    quantitative = 1
    nominal = 2
    ordinal = 3


class DataAttribute(ModelMixin, db.Model):
    __tablename__ = "data_attribute"

    type = db.Column(db.Enum(AttributeType))
    name = db.Column(db.String, nullable=False)
    statistics = db.Column(JSONB)
    data_id = db.Column(
        db.Integer, db.ForeignKey("base_data.id"), nullable=False, index=True
    )

    data = db.relationship("BaseData", back_populates="attributes")
    values = db.relationship(
        "DataValue", back_populates="attribute", cascade="all,delete-orphan"
    )

    __mapper_args__ = {"polymorphic_on": type}

    def __init__(self, **kwargs):
        print("Attribute {} created".format(kwargs.get("name")))
        super().__init__(**kwargs)

    def __deepcopy__(self, memo):
        if self.data.type is GeoDataType.geo_data:
            memo[id(self)] = self
            return self
        else:
            cls = type(self)
            other = cls(
                name=self.name,
            )
            memo[id(self)] = other
            other.name = self.name
            other.data = deepcopy(
                self.data, memo
            )  # TODO: do not duplicate data
            other.values = [deepcopy(v, memo) for v in self.values]
            return other

    def new_copy(self):
        cls = self.__class__
        return cls(type=self.type, name=self.name)

    def load_statistics(self):
        """Load statistics about the attribute values.

        The stats are calculated using pandas' describe function.
        """
        values = [v.value for v in self.values]
        stats = DataFrame(values).describe(include="all")
        stats_dict = json.loads(stats[0].to_json())
        self.statistics = {"total": len(values)}
        for key, value in stats_dict.items():
            if "%" in key:
                self.statistics.setdefault("percentiles", {})[key] = value
            else:
                self.statistics[key] = value

    def get_value_class(self):
        raise NotImplementedError

    def same_as(self, other) -> bool:
        """Check attribute is same as other.

        :param other: other data attribute or ``None``
        :return: ``True`` if they are the same, ``False`` otherwise

        .. warning:: This does not check values or statistics
        """
        return (
            isinstance(other, DataAttribute)
            and self.type == other.type
            and self.name == other.name
        )


class DataAttributeQuantitative(DataAttribute):
    __mapper_args__ = {"polymorphic_identity": AttributeType.quantitative}

    def get_value_class(self):
        return DataValueQuantitative


class DataAttributeNominal(DataAttribute):
    __mapper_args__ = {"polymorphic_identity": AttributeType.nominal}

    def get_value_class(self):
        return DataValueNominal


class DataAttributeOrdinal(DataAttribute):
    order = db.Column(JSONB)  # maybe not the best type?

    __mapper_args__ = {"polymorphic_identity": AttributeType.ordinal}

    def __deepcopy__(self, memo):
        other = super().__deepcopy__(memo)
        other.order = copy(self.order)
        return other

    def new_copy(self):
        obj = super().new_copy()
        obj.order = list(self.order)
        return obj

    def get_value_class(self):
        return DataValueOrdinal

    def same_as(self, other) -> bool:
        """Check attribute is same as other.

        :param other: other data attribute or ``None``
        :return: ``True`` if they are the same, ``False`` otherwise

        .. warning:: This does not check values or statistics
        """
        return super().same_as(other) and self.order == other.order


@total_ordering
class DataValue(ModelMixin, db.Model):
    __tablename__ = "data_value"

    type = db.Column(db.Enum(AttributeType))
    attribute_id = db.Column(
        db.Integer, db.ForeignKey("data_attribute.id"), index=True
    )
    feature_id = db.Column(
        db.Integer, db.ForeignKey("feature.id", ondelete="CASCADE"), index=True
    )
    value = db.Column(JSONB)

    __table_args__ = (db.UniqueConstraint("attribute_id", "feature_id"),)
    __mapper_args__ = {"polymorphic_on": type}

    attribute = db.relationship("DataAttribute", back_populates="values")
    feature = db.relationship("Feature", back_populates="properties")

    def __hash__(self):
        return hash((self.id, self.attribute_id, self.feature_id, self.value))

    def __lt__(self, other):
        raise NotImplementedError

    def __eq__(self, other):
        raise NotImplementedError

    def __deepcopy__(self, memo):
        other = type(self)()
        memo[id(self)] = other
        other.value = copy(self.value)
        other.attribute = deepcopy(self.attribute, memo)
        other.feature = deepcopy(self.feature, memo)
        return other


class DataValueQuantitative(DataValue):
    __mapper_args__ = {"polymorphic_identity": AttributeType.quantitative}

    def _check_type(self, other):
        if isinstance(other, DataValueQuantitative) is False:
            raise TypeError

    def __hash__(self):
        return super().__hash__()

    def __add__(self, other):
        if isinstance(other, numbers.Number):
            return self.__class__(value=self.value + other)
        elif isinstance(other, DataValueQuantitative):
            return self.__class__(value=self.value + other.value)
        else:
            return NotImplemented

    def __radd__(self, other):
        if isinstance(other, numbers.Number):
            return self.__class__(value=self.value + other)
        elif isinstance(other, DataValueQuantitative):
            return self.__class__(value=self.value + other.value)
        else:
            return NotImplemented

    def __truediv__(self, other):
        if isinstance(other, numbers.Number):
            return self.__class__(value=(self.value / other))
        elif isinstance(other, DataValueQuantitative):
            return self.__class__(value=(self.value / other.value))
        else:
            return NotImplemented

    def __lt__(self, other):
        """Check if self < other."""
        self._check_type(other)
        return self.value < other.value

    def __eq__(self, other):
        """Check if self == other."""
        self._check_type(other)
        return self.value == other.value


class DataValueNominal(DataValue):
    __mapper_args__ = {"polymorphic_identity": AttributeType.nominal}

    def _check_type(self, other):
        if isinstance(other, DataValueNominal) is False:
            raise TypeError

    def __hash__(self):
        return super().__hash__()

    def __lt__(self, other):
        """Check if self < other."""
        raise TypeError

    def __eq__(self, other):
        """Check if self == other."""
        self._check_type(other)
        return self.value == other.value


class DataValueOrdinal(DataValue):
    __mapper_args__ = {"polymorphic_identity": AttributeType.ordinal}

    def _check_type(self, other):
        if (
            isinstance(self.attribute, DataAttributeOrdinal) is False
            or self.attribute is not other.attribute
        ):
            raise TypeError

    def __hash__(self):
        return super().__hash__()

    def __lt__(self, other):
        """Check if self < other."""
        self._check_type(other)
        order = self.attribute.order
        return order.index(self.value) > order.index(other.value)

    def __eq__(self, other):
        """Check if self == other."""
        self._check_type(other)
        order = self.attribute.order
        return order.index(self.value) == order.index(other.value)


class FeatureType(enum.Enum):
    feature = 1
    geo_feature = 2


class ExplainabilityType(enum.Enum):
    categories_rule = 1
    continuous_rule = 2
    geo_buffer = 3
    mrsort = 4
    weighted_sum = 5
    merged_features = 6
    min = 7
    max = 8
    sum = 9
    average = 10


class Feature(ModelMixin, db.Model):
    """Table for the data features.

    Attributes:
        properties (dict): The properties or attributes of the feature.
        data_id (int): Reference to the data.
    """

    __tablename__ = "feature"

    type = db.Column(db.Enum(FeatureType))  # inheritance discriminator
    data_id = db.Column(
        db.Integer, db.ForeignKey("base_data.id"), nullable=False
    )
    explainability = db.Column(JSONB)

    data = db.relationship("BaseData", back_populates="features")
    properties = db.relationship(
        "DataValue", back_populates="feature", cascade="all,delete-orphan"
    )

    __mapper_args__ = {
        "polymorphic_on": type,
        "polymorphic_identity": FeatureType.feature,
    }

    def __init__(self, *args, properties=[], explainability=[], **kwargs):
        super().__init__(*args, **kwargs)
        self.properties = properties
        self.explainability = explainability

    def __deepcopy__(self, memo):
        cls = type(self)
        other = cls(explainability=copy(self.explainability))
        memo[id(self)] = other
        other.data = deepcopy(self.data, memo)
        other.properties = [deepcopy(p, memo) for p in self.properties]
        return other

    def get_property(self, prop):
        """Returns the value of the property"""
        for p in self.properties:
            if p.attribute.name == prop:
                return p.value


class GeoFeature(Feature):
    """Table for the geo-data features.

    Attributes:
        geom: The geometry of the feature.
        properties (dict): The properties or attributes of the feature.
        data_id (int): Reference to the geo-data.
    """

    __tablename__ = "geo_feature"

    id = db.Column(db.Integer, db.ForeignKey("feature.id"), primary_key=True)
    data = db.relationship("BaseGeoData", back_populates="features")
    geom = db.Column(Geometry)

    __mapper_args__ = {"polymorphic_identity": FeatureType.geo_feature}

    def __init__(self, geom, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.geom = geom

    def as_geojson(self):
        """Return the feature as a GeoJSON data."""
        geometry = mapping(to_shape(self.geom))
        geojson = {
            "type": "Feature",
            "geometry": geometry,
            "properties": {p.attribute.name: p.value for p in self.properties},
        }
        return geojson

    def __deepcopy__(self, memo):
        cls = type(self)
        other = cls(
            geom=copy(self.geom), explainability=copy(self.explainability)
        )
        memo[id(self)] = other
        other.data = deepcopy(self.data, memo)
        other.properties = [deepcopy(p, memo) for p in self.properties]
        return other
