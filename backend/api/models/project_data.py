import enum
from copy import deepcopy
from datetime import datetime

from . import db
from .geo_data import DataAttribute, GeoData, GlobalData
from .mixins import ModelMixin


class DataType(enum.Enum):
    geo_data = 1
    generator = 2
    global_data = 3


input_assoc = db.Table(
    "data_input_association",
    db.Model.metadata,
    db.Column(
        "project_data_id",
        db.Integer,
        db.ForeignKey("project_data.id"),
        primary_key=True,
    ),
    db.Column(
        "input_data_id",
        db.Integer,
        db.ForeignKey("project_data.id"),
        primary_key=True,
    ),
)


class ProjectData(ModelMixin, db.Model):
    __tablename__ = "project_data"

    data_type = db.Column(db.Enum(DataType))
    id = db.Column(db.Integer, primary_key=True)
    data_id = db.Column(db.Integer, db.ForeignKey("base_data.id"))
    last_update = db.Column(db.DateTime)
    project_id = db.Column(
        db.Integer, db.ForeignKey("project.id"), nullable=False
    )
    description = db.Column(db.String)
    name = db.Column(db.String)

    input_data = db.relationship(
        "ProjectData",
        secondary=input_assoc,
        primaryjoin=(id == input_assoc.c.project_data_id),
        secondaryjoin=(id == input_assoc.c.input_data_id),
        backref="referenced",
        order_by=input_assoc.c.input_data_id,
    )
    data = db.relationship("BaseData")
    shares = db.relationship(
        "DataShare", cascade="all,delete-orphan", back_populates="data"
    )

    __mapper_args__ = {"polymorphic_on": data_type}

    def __init__(self, name=None, project=None, description="", input_data=[]):
        self.project = project
        self.input_data = []
        for data in input_data:
            self.add_input(data)
        self.last_update = None
        self.name = name
        self.description = description

    def __deepcopy__(self, memo):
        other = type(self)()
        memo[id(self)] = other
        other.name = self.name
        other.description = self.description
        other.project = deepcopy(self.project, memo)
        other.input_data = [deepcopy(d, memo) for d in self.input_data]
        other.data = deepcopy(self.data, memo)
        return other

    def update_modification(self):
        """Set the update time to the current time."""
        self.last_update = datetime.utcnow()

    def add_input(self, project_data):
        """Add input data.

        :param project_data:
        :raises ValueError: if `project_data` is from a different project
        """
        if project_data.project != self.project:
            raise ValueError("cannot add input data from other projects")
        self.input_data.append(project_data)

    def is_outdated(self):
        """Checks if input data are more recent than this object"""
        for data in self.input_data:
            if data.last_update is not None:
                if (
                    self.last_update is None
                    or data.last_update > self.last_update
                ):
                    return True
        return False

    def get_attributes_list(self):
        """Returns the list of attributes of the features for this data.

        Returns:
            a dictionary with the following keys: "name", "attribute", "type"
        """
        if self.data is not None:
            attr_list = []
            for attribute in self.data.attributes:
                attr_list.append(
                    {
                        "name": self.name,
                        "attribute": attribute.name,
                        "type": attribute.type.name,
                    }
                )
            return attr_list
        else:
            return []

    def _get_used_input_attributes(self) -> set[DataAttribute]:
        """Return set of used input attributes of project data.

        :param data:
        :return: used input attributes
        """
        return set()

    def get_used_input_attributes(self) -> list[DataAttribute]:
        """Get used input attributes of project data if the user is authorized
        to access it.

        This list contains al inputl attributes used at least once in a
        preference model of the project data.

        Returns:
            Used input attributes.
        """
        return sorted(
            self._get_used_input_attributes(), key=lambda a: (a.data.id, a.id)
        )

    def _get_used_attributes(self) -> set[DataAttribute]:
        """Return set of used attributes of project data.

        This set contains all attributes used at least once in a preference
        model of the project data.

        :param data:
        :return: used input attributes
        """
        attributes = set()
        if self.data is None:
            return attributes
        for ref in self.referenced:
            attributes |= {
                attr
                for attr in ref._get_used_input_attributes()
                if attr.data_id == self.data.id
            }
        return attributes

    def get_used_attributes(self) -> list[DataAttribute]:
        """Get used attributes of project data if the user is authorized
        to access it.

        This list contains all attributes used at least once in a preference
        model of the downstream project data.

        Returns:
            Used attributes.
        """
        return sorted(self._get_used_attributes(), key=lambda a: a.id)


class DataGeo(ProjectData):
    data = db.relationship("GeoData")

    __mapper_args__ = {"polymorphic_identity": DataType.geo_data}

    def __init__(self, data=None, data_id=None, **kwargs):
        if data is None and data_id is not None:
            data = GeoData.get_by_id(data_id)
        elif data is not None and data_id is not None:
            raise ValueError(
                "Only one of the parameters data or data_id must be passed"
            )
        if kwargs.get("name") is None and data is not None:
            kwargs["name"] = data.name
        super().__init__(**kwargs)
        self.data = data  # TODO: Verify the data is not None
        self.last_update = datetime.utcnow()

    def __repr__(self):
        return (
            "<DataGeo(name={s.name}, project_id={s.project_id}, "
            "data_id={s.data_id})>".format(s=self)
        )

    def add_input(self, project_data):
        """Add input (always fail).

        :param project_data:
        :raises TypeError: data doesn't accept inputs
        """
        raise TypeError(f"cannot add input in a {type(self).__name__} object")


class ProjectGlobalData(ProjectData):
    data = db.relationship("GlobalData")

    __mapper_args__ = {"polymorphic_identity": DataType.global_data}

    def __init__(self, data=None, data_id=None, **kwargs):
        if data is None and data_id is not None:
            data = GlobalData.get_by_id(data_id)
        elif data is not None and data_id is not None:
            raise ValueError(
                "Only one of the parameters data or data_id must be passed"
            )
        if kwargs.get("name") is None and data is not None:
            kwargs["name"] = data.name
        super().__init__(**kwargs)
        self.data = data  # TODO: Verify the data is not None
        self.last_update = datetime.utcnow()

    def __repr__(self):
        return (
            "<ProjectGlobalData(name={s.name}, project_id={s.project_id}, "
            "data_id={s.data_id})>".format(s=self)
        )

    def add_input(self, project_data):
        """Add input (always fail).

        :param project_data:
        :raises TypeError: data doesn't accept inputs
        """
        raise TypeError(f"cannot add input in a {type(self).__name__} object")


class DataGenerator(ProjectData):
    active_model_id = db.Column(
        db.Integer, db.ForeignKey("preference_model.id")
    )

    data = db.relationship(
        "GeneratedGeoData", single_parent=True, cascade="all,delete-orphan"
    )
    _preference_models = db.relationship(
        "PreferenceModel",
        cascade="all,delete-orphan",
        back_populates="data_generator",
        foreign_keys="[PreferenceModel.data_generator_id]",
        order_by="PreferenceModel.id",
    )
    _preference_model = db.relationship(
        "PreferenceModel",
        cascade="all",
        primaryjoin="project_data.c.active_model_id == preference_model.c.id",
        post_update=True,
    )

    __mapper_args__ = {"polymorphic_identity": DataType.generator}

    def __init__(self, preference_model=None, **kwargs):
        super().__init__(**kwargs)
        if self.preference_model is None:
            self.preference_model = preference_model
        self.data = None

    def __deepcopy__(self, memo):
        other = super().__deepcopy__(memo)
        other._preference_model = deepcopy(self._preference_model, memo)
        other._preference_models = [
            deepcopy(p, memo) for p in self._preference_models
        ]
        return other

    @property
    def preference_model(self):
        return self._preference_model

    @preference_model.setter
    def preference_model(self, model):
        self._preference_model = model
        if model not in self._preference_models:
            self._preference_models.append(model)

    def _get_used_input_attributes(self) -> set[DataAttribute]:
        """Return set of used input attributes of project data.

        This set contains all input attributes used at least once in a
        preference model of the project data.

        :return: used input attributes
        """
        attributes = set()
        for model in self._preference_models:
            attributes |= model._get_used_input_attributes()
        return attributes


@db.event.listens_for(DataGenerator.name, "set", propagate=True)
def generator_name_set(target, value, oldvalue, initiator):
    """Event to modify the generated geo-data name when the project data name
    is modified"""
    if value != oldvalue and target.data is not None:
        target.data.name = value
