from copy import deepcopy

from .preference import ContinuousRule, DiscreteRules, MRSort, WeightedSum
from .project import Project
from .project_data import ProjectData


class ProjectDataProcessingService:
    def _topological_sort(self, graph):
        """Sort nodes topologically using Kahn's algorithm.

        :param graph: directed graph
        :raises TypeError: if `graph` is not acyclic
        :return: topologically ordered nodes
        """
        nodes = deepcopy(graph)
        sorted_data = []
        set_queue = []
        for node, edges in list(nodes.items()):
            if len(edges) == 0:
                del nodes[node]
                set_queue.append(node)
        while len(set_queue) > 0:
            current = set_queue.pop()
            sorted_data.append(current)
            for node, edges in list(nodes.items()):
                if current in edges:
                    edges.remove(current)
                    if len(edges) == 0:
                        del nodes[node]
                        set_queue.append(node)
        if len(nodes) > 0:
            raise TypeError("Graph is not acyclic")
        return sorted_data

    def get_project_data_graph(
        self, project: Project
    ) -> dict[ProjectData, list[ProjectData]]:
        """Return complete project data graph.

        :param project:
        :return:
        """
        return {data: list(data.input_data) for data in project.data_list}

    def get_input_graph(
        self, data: ProjectData
    ) -> dict[ProjectData, list[ProjectData]]:
        """Return graph of input data spanning from current project data.

        :param data:
        :return:
        """
        graph = {}
        nodes = [data]
        while len(nodes) > 0:
            node = nodes.pop(0)
            if node in graph:
                continue
            graph[node] = list(node.input_data)
            nodes += list(set(node.input_data) - set(nodes))
        return graph

    def get_output_graph(
        self, data: ProjectData
    ) -> dict[ProjectData, list[ProjectData]]:
        """Return graph of output data spanning from current project data.

        :param data:
        :return:
        """
        graph = {}
        nodes = [data]
        while len(nodes) > 0:
            node = nodes.pop(0)
            if node in graph:
                continue
            graph[node] = list(node.referenced)
            nodes += list(set(node.referenced) - set(nodes))
        return graph

    def replace_project_data(
        self, old_data: ProjectData, new_data: ProjectData
    ):
        """Replace project data in hierarchy with other one.

        :param old_data:
        :param new_data:
        """
        if old_data == new_data:
            # Useless to replace data by itself
            return

        if new_data in old_data.referenced:
            # Cannot replace old data if referenced by new data
            return

        old_used_attrs = old_data.get_used_attributes()
        attrs_mapping = {}
        if new_data.data is not None:
            for new_attr in new_data.data.attributes:
                for old_attr in old_used_attrs:
                    if old_attr.same_as(new_attr):
                        attrs_mapping[old_attr] = new_attr
                        break
        for ref in list(old_data.referenced):
            for model in ref._preference_models:
                match model:
                    case WeightedSum():
                        for op in model.operands:
                            op.attribute = attrs_mapping.get(
                                op.attribute, op.attribute
                            )
                    case MRSort():
                        for criterion in model.criteria:
                            criterion.attribute = attrs_mapping.get(
                                criterion.attribute, criterion.attribute
                            )
                    case DiscreteRules():
                        for category in model.categories:
                            for i, _ in enumerate(category.rules):
                                for (
                                    old_attr,
                                    new_attr,
                                ) in attrs_mapping.items():
                                    category.rules[i] = category.rules[
                                        i
                                    ].replace(
                                        f"{old_data.name}.{old_attr.name}",
                                        f"{new_data.name}.{new_attr.name}",
                                    )
                    case ContinuousRule():
                        for old_attr, new_attr in attrs_mapping.items():
                            model.rule = model.rule.replace(
                                f"{old_data.name}.{old_attr.name}",
                                f"{new_data.name}.{new_attr.name}",
                            )
                if new_data not in ref.input_data:
                    ref.input_data.append(new_data)
        new_data.update()
        old_data.delete()
