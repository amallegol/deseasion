import enum
from copy import deepcopy

import numpy as np
from sqlalchemy.dialects.postgresql import ARRAY, JSONB
from sqlalchemy.ext.orderinglist import ordering_list
from sqlalchemy.orm import backref, validates

from ..exceptions import GeometryTypeError, ProcessingError
from . import db
from .geo_data import AttributeType, DataAttribute
from .mixins import BaseModelMixin, ModelMixin
from .preference_model_utils import evaluate_discrete_rule


class KeepOverlap(enum.Enum):
    all = 1
    min = 2
    max = 3
    sum = 4
    average = 5


class PrefType(enum.Enum):
    categories_rule = 1
    continuous_rule = 2
    geo_buffer = 3
    mrsort = 4
    weighted_sum = 5


class PreferenceModel(ModelMixin, db.Model):
    __tablename__ = "preference_model"

    pref_type = db.Column(db.Enum(PrefType))
    name = db.Column(db.String)
    data_generator_id = db.Column(
        db.Integer, db.ForeignKey("project_data.id"), nullable=False
    )
    keep_overlap = db.Column(
        db.Enum(KeepOverlap), default=KeepOverlap.all, nullable=False
    )
    cut_to_extent = db.Column(db.Boolean)
    dissolve_adjacent = db.Column(db.Boolean)

    data_generator = db.relationship(
        "DataGenerator",
        back_populates="_preference_models",
        foreign_keys=[data_generator_id],
    )

    __mapper_args__ = {"polymorphic_on": pref_type}

    __allowed_overlap = (
        KeepOverlap.all,
        KeepOverlap.min,
        KeepOverlap.max,
        KeepOverlap.sum,
        KeepOverlap.average,
    )

    def __init__(
        self,
        keep_overlap=KeepOverlap.all,
        cut_to_extent=True,
        dissolve_adjacent=False,
        name="",
        **kwargs,
    ):
        self.keep_overlap = keep_overlap
        self.cut_to_extent = cut_to_extent
        self.dissolve_adjacent = dissolve_adjacent
        self.name = name

    def __deepcopy__(self, memo):
        other = type(self)()
        memo[id(self)] = other
        other.name = self.name
        other.keep_overlap = self.keep_overlap
        other.cut_to_extent = self.cut_to_extent
        other.dissolve_adjacent = self.dissolve_adjacent
        other.data_generator = deepcopy(self.data_generator, memo)
        return other

    @property
    def input_data(self):
        return self.data_generator.input_data

    def get_overlap_value(self, values):
        """Return the overlap value filtered through the overlap function.

        Args:
            values: A list of the values.
        """
        if self.keep_overlap == KeepOverlap.max:
            return max(values)
        elif self.keep_overlap == KeepOverlap.min:
            return min(values)
        elif self.keep_overlap == KeepOverlap.sum:
            return sum(values)
        elif self.keep_overlap == KeepOverlap.average:
            return sum(values) / len(values)

    def process_overlap(self, entity):
        """Decompose the overlapping attributes, and keep the chosen value
        (all, worse or best)"""
        if self.keep_overlap not in self.__allowed_overlap:
            raise ValueError(
                'Keep overlay is "{}" but should be in {}'
                "".format(self.keep_overlap.name, self.__allowed_overlap)
            )
        if self.keep_overlap == KeepOverlap.all:
            return entity
        else:
            try:
                result = entity.decompose_intersection()
            except GeometryTypeError:
                raise ProcessingError(
                    "The overlapping geometries are not polygons."
                )
            for feature in result.features:
                for prop, values in feature.data.attributes.items():
                    values = [v for v in values if v is not None]
                    if values is None or len(values) < 1:
                        val = values
                    elif len(values) == 1:
                        val = values[0]
                    else:
                        try:
                            val = self.get_overlap_value(values)
                        except TypeError:
                            raise ProcessingError(
                                "Error overlapping the value types."
                            )
                    feature.data.attributes[prop] = val
            return result

    def _get_used_input_attributes(self) -> set[DataAttribute]:
        """Get set of used input attributes in a model definition.

        :return:
        """
        return set()

    def get_used_input_attributes(self) -> list[DataAttribute]:
        """Get list of used input attributes in a model definition.

        :return:
        """
        return sorted(
            self._get_used_input_attributes(), key=lambda a: (a.data.id, a.id)
        )


def _extract_attributes_from_code(
    code: str, model: PreferenceModel
) -> set[DataAttribute]:
    """Extract attributes from code.

    :param code:
    :param model: preference model
    :return: set of attributes used in code
    """
    return {
        attr
        for data in model.input_data
        for attr in data.data.attributes
        if f"{data.name}.{attr.name}" in code
    }


class DefaultValue(BaseModelMixin, db.Model):
    attribute_id = db.Column(
        db.Integer,
        db.ForeignKey("data_attribute.id", ondelete="CASCADE"),
        nullable=False,
        primary_key=True,
    )
    model_id = db.Column(
        db.Integer,
        db.ForeignKey("preference_model.id", ondelete="CASCADE"),
        nullable=False,
        primary_key=True,
    )
    value = db.Column(JSONB)

    attribute = db.relationship("DataAttribute")
    model = db.relationship(
        "PrefDefaultValues", back_populates="default_values"
    )

    def __deepcopy__(self, memo):
        other = type(self)()
        memo[id(self)] = other
        other.attribute = deepcopy(self.attribute, memo)
        other.model = deepcopy(self.model, memo)
        other.value = self.value
        return other


class PrefDefaultValues(PreferenceModel):
    default_values = db.relationship(
        "DefaultValue", back_populates="model", cascade="all,delete-orphan"
    )

    def __deepcopy__(self, memo):
        other = super().__deepcopy__(memo)
        other.default_values = [deepcopy(d, memo) for d in self.default_values]
        return other

    # @validates('default_values')
    # def validate_default_values(self, key, default_value):
    # TODO: Check that the attribute is in the input data of the model
    # pass

    def get_default_value(self, attribute):
        """Returns the default value for the attribute.

        Returns:
            The default value if it exists, or None otherwise
        """
        try:
            return next(
                d.value
                for d in self.default_values
                if d.attribute_id == attribute.id
            )
        except StopIteration:
            return None


class GeoBuffer(PreferenceModel):
    """Model to create a buffer around the geometry."""

    radius = db.Column(db.BigInteger)

    __mapper_args__ = {"polymorphic_identity": PrefType.geo_buffer}

    def __init__(self, radius=None, **kwargs):
        super().__init__(**kwargs)
        self.pref_type = PrefType.geo_buffer
        self.radius = radius

    def __deepcopy__(self, memo):
        other = super().__deepcopy__(memo)
        other.radius = self.radius
        return other

    def _get_used_input_attributes(self) -> set[DataAttribute]:
        """Get set of used input attributes in a model definition.

        :return:
        """
        return {
            attr for data in self.input_data for attr in data.data.attributes
        }


class DiscreteCategory(ModelMixin, db.Model):
    """Model for creating data using discrete values (categories)."""

    __tablename__ = "discrete_category"

    type = db.Column(db.String)
    name = db.Column(db.String)
    position = db.Column(db.Integer)
    preference_model_id = db.Column(
        db.Integer, db.ForeignKey("preference_model.id", ondelete="CASCADE")
    )

    preference_model = db.relationship("DiscreteModel")

    __mapper_args__ = {
        "polymorphic_on": type,
        "polymorphic_identity": "discrete_category",
    }

    def __init__(self, name, **kwargs):
        super().__init__(**kwargs)
        self.name = name

    def __deepcopy__(self, memo):
        other = type(self)(name=self.name, position=self.position)
        memo[id(self)] = other
        other.preference_model = deepcopy(self.preference_model, memo)
        return other


class DiscreteRulesCategory(DiscreteCategory):
    rules = db.Column(ARRAY(db.String, dimensions=1))

    __mapper_args__ = {"polymorphic_identity": "discrete_rules_category"}

    def __init__(self, rules=[], **kwargs):
        super().__init__(**kwargs)
        self.rules = rules

    def __deepcopy__(self, memo):
        other = type(self)(name=self.name)
        memo[id(self)] = other
        other.preference_model = deepcopy(self.preference_model, memo)
        other.rules = deepcopy(self.rules, memo)
        return other

    def evaluate_data(self, data, stats=None):
        evaluation = np.full_like(
            data,
            fill_value=False,
            dtype=bool,
            shape=(len(self.rules), len(data)),
        )
        for i, rule in enumerate(self.rules):
            evaluation[i, :] = evaluate_discrete_rule(data, rule, stats)
        return evaluation


class DiscreteModel(PreferenceModel):
    categories = db.relationship(
        "DiscreteCategory",
        order_by="DiscreteCategory.position",
        cascade="all,delete-orphan",
        collection_class=ordering_list("position"),
        back_populates="preference_model",
    )

    __allow_overlap = (KeepOverlap.all, KeepOverlap.min, KeepOverlap.max)

    def get_overlap_value(self, values):
        """Returns the best of worse value from the list

        If the keep_overlap attribute is max, returns the best value.
        If the keep_overlap attribute is min, returns the worst value.

        Args:
            values: an list of the values to check
        """
        if self.keep_overlap == KeepOverlap.max:
            categs = [c.name for c in self.categories]
        elif self.keep_overlap == KeepOverlap.min:
            categs = [c.name for c in self.categories[::-1]]
        else:
            raise ValueError(
                'Keep overlay is "{}" but should be "min" or '
                '"max"'.format(self.keep_overlap.name)
            )
        for c in categs:
            if c in values:
                return c
        return None


class DiscreteRules(DiscreteModel):
    """Used to create a geo-data containing discrete values (ie. categories)
    from rules."""

    categories = db.relationship(
        "DiscreteRulesCategory",
        order_by="DiscreteRulesCategory.position",
        cascade="all,delete-orphan",
        collection_class=ordering_list("position"),
        back_populates="preference_model",
    )

    __mapper_args__ = {"polymorphic_identity": PrefType.categories_rule}

    def __init__(self, categories=[], **kwargs):
        super().__init__(**kwargs)
        self.categories = categories

    def __deepcopy__(self, memo):
        other = super().__deepcopy__(memo)
        other.categories = [deepcopy(c, memo) for c in self.categories]
        return other

    def _get_used_input_attributes(self) -> set[DataAttribute]:
        """Get set of used input attributes in a model definition.

        :return:
        """
        attributes = set()
        for category in self.categories:
            for rule in category.rules:
                attributes |= _extract_attributes_from_code(rule, self)
        return attributes


class MRSort(DiscreteModel):
    """Used to create a geo-data using the MR-Sort algorithm.

    Attributes:
        categories (list): The categories for the sorting algorithm.
            Best category first.
        majority_threshold (float): The cut threshold for the concordance
            condition. At least half the sum of the criteria weights.
    """

    majority_threshold = db.Column(db.Float)

    criteria = db.relationship(
        "MRSortCriterion", back_populates="mrsort", cascade="all,delete-orphan"
    )

    __mapper_args__ = {"polymorphic_identity": PrefType.mrsort}

    def __init__(self, criteria=[], categories=[], **kwargs):
        super().__init__(**kwargs)
        self.criteria = criteria
        self.categories = categories

    def __deepcopy__(self, memo):
        other = super().__deepcopy__(memo)
        other.majority_threshold = self.majority_threshold
        other.criteria = [deepcopy(c, memo) for c in self.criteria]
        other.categories = [deepcopy(c, memo) for c in self.categories]
        return other

    @validates("criteria")
    def validate_criteria(self, key, criterion):
        """Verify that the criterion references an input_data."""
        attributes = []
        for data in self.input_data:
            if data.data is not None:
                attributes += data.data.attributes
        assert (
            criterion.attribute in attributes
        ), "The criterion references an invalid data."
        return criterion

    def init_criteria(self):
        criteria = []
        for data in self.input_data:
            if data.data is not None:
                for attr in data.data.attributes:
                    if attr.type is AttributeType.quantitative:
                        for crit in self.criteria:
                            if crit.attribute is attr:
                                criterion = crit
                                break
                        else:
                            criterion = MRSortCriterion(
                                attribute=attr, mrsort_id=self.id
                            )
                        criteria.append(criterion)
        self.criteria = criteria

    def check_category(self, attributes):
        """Execute the MR-Sort algorithm on the attributes.

        Args:
            attributes (list): List of ('name', 'attribute', value).

        Raises:
            AttributeError: If the attribute ('name', 'attribute') does not
                correspond to any criterion.
        """
        categories = self.categories[::-1]  # reverse the order (worst first)
        result = categories[0]
        criteria_lookup = {
            (c.attribute.data.name, c.attribute.name): c for c in self.criteria
        }
        explainability = {
            "lower": None,
            "upper": None,
        }
        for index, category in enumerate(categories[1:]):
            weights = {}
            total = 0
            for name, attribute, value in attributes:
                criterion = criteria_lookup.get((name, attribute), None)
                if criterion is not None and criterion.is_better(value, index):
                    total += criterion.weight
                    weights[attribute] = criterion.weight
            if total >= self.majority_threshold:
                result = category
                explainability["lower"] = {
                    "profile": index,
                    "weights": weights,
                }
            else:
                explainability["upper"] = {
                    "profile": index,
                    "weights": weights,
                }
                break
        return result, explainability

    def _get_used_input_attributes(self) -> set[DataAttribute]:
        """Get set of used input attributes in a model definition.

        :return:
        """
        return {
            criterion.attribute
            for criterion in self.criteria
            if criterion.attribute is not None
        }


class MRSortCriterion(BaseModelMixin, db.Model):
    """Contain the criterion data for the MR-Sort algorithm.

    Attributes:
        profiles: The values for the profiles of the categories.
            Worst value first.
        weight (float):
            The weight of the criterion in the algorithm.
        maximize (bool):
            Preference direction (maximize if True, minimize if False).
            Default: True.
    """

    __tablename__ = "mrsort_criterion"

    profiles = db.Column(ARRAY(db.Float, dimensions=1))
    weight = db.Column(db.Float)
    maximize = db.Column(db.Boolean, nullable=False, default=False)
    mrsort_id = db.Column(
        db.Integer,
        db.ForeignKey("preference_model.id", ondelete="CASCADE"),
        primary_key=True,
    )
    attribute_id = db.Column(
        db.Integer,
        db.ForeignKey("data_attribute.id", ondelete="CASCADE"),
        primary_key=True,
    )

    attribute = db.relationship(
        "DataAttribute",
        backref=backref("mrsort_criteria", cascade="delete,delete-orphan"),
    )
    mrsort = db.relationship("MRSort", back_populates="criteria")

    def __init__(
        self, mrsort=None, profiles=[], weight=0, maximize=True, **kwargs
    ):
        super().__init__(**kwargs)
        self.profiles = profiles
        self.weight = weight
        self.maximize = maximize

    def __deepcopy__(self, memo):
        other = type(self)()
        memo[id(self)] = other
        other.profiles = deepcopy(self.profiles, memo)
        other.weight = self.weight
        other.maximize = self.maximize
        other.attribute = deepcopy(self.attribute, memo)
        other.mrsort = deepcopy(self.mrsort, memo)
        return other

    def is_better(self, value, profile_index):
        """Check if the value is better than the profile for the given index.

        Args:
            value (number): The value to check.
            profile_index (int): Index of the profile in the profiles list.
        """
        if value is None:
            return False
        if self.maximize:
            return value >= self.profiles[profile_index]
        else:
            return value <= self.profiles[profile_index]


class ContinuousRule(PrefDefaultValues):
    """Used to create a geo-data containing continuous values from a rule"""

    rule = db.Column(db.String)

    __mapper_args__ = {"polymorphic_identity": PrefType.continuous_rule}

    def __init__(self, rule="", default_values=[], **kwargs):
        super().__init__(**kwargs)
        self.rule = rule
        self.default_values = default_values
        self.pref_type = PrefType.continuous_rule

    def __deepcopy__(self, memo):
        other = super().__deepcopy__(memo)
        other.rule = self.rule
        return other

    def _get_used_input_attributes(self) -> set[DataAttribute]:
        """Get set of used input attributes in a model definition.

        :return:
        """
        return _extract_attributes_from_code(self.rule, self)


class WeightedSum(PrefDefaultValues):
    operands = db.relationship(
        "WeightedSumOperand",
        back_populates="model",
        cascade="all,delete-orphan",
    )

    __mapper_args__ = {"polymorphic_identity": PrefType.weighted_sum}

    def __init__(self, operands=None, **kwargs):
        super().__init__(**kwargs)
        if operands is None:
            self.operands = [WeightedSumOperand(weight=1)]
        else:
            self.operands = operands

    def __deepcopy__(self, memo):
        other = super().__deepcopy__(memo)
        other.operands = [deepcopy(o, memo) for o in self.operands]
        return other

    def _get_used_input_attributes(self) -> set[DataAttribute]:
        """Get set of used input attributes in a model definition.

        :return:
        """
        return {
            op.attribute for op in self.operands if op.attribute is not None
        }


class WeightedSumOperand(ModelMixin, db.Model):
    __tablename__ = "weighted_sum_operand"

    attribute_id = db.Column(
        db.Integer, db.ForeignKey("data_attribute.id", ondelete="SET NULL")
    )
    weight = db.Column(db.Float)
    model_id = db.Column(
        db.Integer,
        db.ForeignKey("preference_model.id", ondelete="CASCADE"),
        nullable=False,
    )

    attribute = db.relationship("DataAttribute")
    model = db.relationship("WeightedSum", back_populates="operands")

    def __deepcopy__(self, memo):
        other = type(self)()
        memo[id(self)] = other
        other.attribute = deepcopy(self.attribute, memo)
        other.model = deepcopy(self.model, memo)
        other.weight = self.weight
        return other
