from copy import copy, deepcopy

from geoalchemy2.shape import from_shape
from geoalchemy2.types import Geometry
from shapely.geometry import box
from sqlalchemy.ext.hybrid import hybrid_property

from . import db
from .mixins import TimestampMixin
from .permission import HasPermissions
from .preference import DiscreteRules
from .project_data import DataGenerator

# Use an association table to avoid circular dependencies between project and
# project_data
# Each project_data has a project, but the project also needs to know which is
# the root data
project_root = db.Table(
    "project_root",
    db.Model.metadata,
    db.Column(
        "project_id", db.Integer, db.ForeignKey("project.id"), primary_key=True
    ),
    db.Column(
        "project_root_data_id", db.Integer, db.ForeignKey("project_data.id")
    ),
)

project_shared_data = db.Table(
    "project_shared_data",
    db.Model.metadata,
    db.Column(
        "project_id", db.Integer, db.ForeignKey("project.id"), primary_key=True
    ),
    db.Column(
        "project_data_id",
        db.Integer,
        db.ForeignKey("project_data.id"),
        primary_key=True,
    ),
)


class ProjectBase(TimestampMixin, db.Model):
    __tablename__ = "project"

    is_template = db.Column(db.Boolean, nullable=False)
    name = db.Column(db.String, nullable=False)
    manager_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    description = db.Column(db.String)
    extent = db.Column(Geometry, nullable=False)

    _manager = db.relationship("User")
    root_data = db.relationship(
        "DataGenerator", secondary=project_root, uselist=False
    )
    proposition = db.relationship(
        "ZoneProposition",
        single_parent=True,
        uselist=False,
        cascade="all,delete-orphan",
        back_populates="project",
    )
    shared_data = db.relationship(
        "ProjectData", secondary=project_shared_data, order_by="ProjectData.id"
    )
    data_list = db.relationship(
        "ProjectData",
        cascade="all,delete-orphan",
        backref="project",
        primaryjoin="ProjectData.project_id == Project.id",
    )

    __mapper_args__ = {"polymorphic_on": is_template}

    def __init__(
        self, name, manager=None, description="", extent=None, is_public=False
    ):
        self.name = name
        self.description = description
        self.manager = manager
        self.root_data = DataGenerator(
            name="Root", preference_model=DiscreteRules(), project=self
        )
        if extent is None:
            shp_box = box(-180.0, -90.0, 180.0, 90.0)
            extent = from_shape(shp_box, srid=4326)
        self.extent = extent
        self.is_public = is_public

    @hybrid_property
    def manager(self):
        return self._manager

    @manager.setter
    def manager(self, user):
        self._manager = user


class Template(ProjectBase):
    """
    Represents the template of a project.

    A template does not have permissions.
    Only the manager of the template should have access to it.
    """

    __mapper_args__ = {"polymorphic_identity": True}
    __tablename__ = None

    def __repr__(self):
        return "<Template(name={})>".format(self.name)

    @classmethod
    def from_project(cls, project, manager):
        """Create a new project template from an existing project."""
        assert manager is not None, "No manager for the template"
        template = cls(
            name=project.name,
            manager=manager,
            description=project.description,
            extent=copy(project.extent),
            is_public=project.is_public,
        )
        # memo used by deepcopy
        # it would here consider that the references to the project
        # are in fact references to the template
        memo = {id(project): template}
        template.data_list = [deepcopy(d, memo) for d in project.data_list]
        template.root_data = deepcopy(project.root_data, memo)
        template.shared_data = [deepcopy(d, memo) for d in project.shared_data]
        return template


class Project(ProjectBase, HasPermissions):
    """Represents a project and its data"""

    __mapper_args__ = {"polymorphic_identity": False}
    __tablename__ = None

    def __repr__(self):
        return "<Project(name={}, manager_id={}>".format(
            self.name, self.manager_id
        )

    @hybrid_property
    def manager(self):
        return self._manager

    @manager.setter
    def manager(self, user):
        self._manager = user
        if user is not None:
            self.permissions.append(Project.Permission(user=user))

    @classmethod
    def from_template(cls, template):
        """
        Create a project from a template.

        The manager of the project will be the owner of the template.

        Args:
            template: the template to copy
        """
        project = cls(
            name=template.name,
            manager=template.manager,
            description=template.description,
            extent=copy(template.extent),
        )
        memo = {id(template): project}
        project.data_list = [deepcopy(d, memo) for d in template.data_list]
        project.root_data = deepcopy(template.root_data, memo)
        project.shared_data = [deepcopy(d, memo) for d in template.shared_data]
        return project
