from .geo_data import (
    DataService,
    GeoDataService,
    GlobalDataService,
    StreamGeoDataService,
)
from .preference_model import PreferenceModelService
from .project import ProjectService
from .project_data import ProjectDataService

project_service = ProjectService()
project_data_service = ProjectDataService()
data_service = DataService()
geo_data_service = GeoDataService()
geo_data_stream_service = StreamGeoDataService()
preference_model_service = PreferenceModelService()
get_global_data_service = GlobalDataService()
