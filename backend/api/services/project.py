from ..exceptions import PermissionError, RequestError
from ..models import Project, ProjectTaskModel, Template
from .permission_service import has_permission


class ProjectService:
    __model__ = Project

    def get_if_authorized(self, project_id, user=None):
        """
        Return the project if the user has the permissions to view it.

        Args:
            project_id (int): Id of the project to return.
            user: User object.

        Raises:
            PermissionError if the user is not authorized to view this project.
        """
        project = self.__model__.get_by_id(project_id)
        if project is None:
            raise RequestError("The project does not exist", 404)
        if not has_permission(project, user):
            raise PermissionError
        return project

    def get_template_if_authorized(self, template_id, manager):
        assert manager is not None
        template = Template.get_by_id(template_id)
        if template is None:
            raise RequestError("The template does not exist", 404)
        if template.manager is not manager:
            raise PermissionError
        return template

    def get_all_authorized_projects(self, user=None):
        """
        Return the list of all project viewable by the user.

        The templates are not returned by this function.
        """
        project_list = Project.query.order_by(Project.id).all()
        return list(filter(lambda p: has_permission(p, user), project_list))

    def get_if_manager(self, project_id, user):
        """Return the project if the user if the project's manager."""
        assert user is not None
        project = self.get_if_authorized(project_id, user)
        if project.manager is not user or project.manager is None:
            raise PermissionError
        return project

    def get_user_templates(self, user):
        """Return the templates of the user."""
        assert user is not None
        query = Template.query.filter_by(manager=user)
        return query.order_by(Template.id).all()

    def get_project_tasks(self, project_id, limit=None, offset=None):
        """Return the list of tasks started in the project.

        Args:
            project_id (int): The id of the project.
            limit (int):
                The number of tasks to return. Default: None (all the tasks).
            offset (int):
                Offset for the tasks list.
                Default: None (start from the beginning).
        """
        project = self.get_if_authorized(project_id)
        query = ProjectTaskModel.query.filter_by(project_id=project.id)
        query = query.order_by(ProjectTaskModel.started_at.desc())
        if limit is not None:
            query = query.limit(limit)
        if offset is not None:
            query = query.offset(offset)
        return query.all()

    def get_total_project_tasks(self, project_id):
        """Return the total number of tasks in this project."""
        project = self.get_if_authorized(project_id)
        query = ProjectTaskModel.query.filter_by(project_id=project.id)
        return query.count()

    def create_template_from_project(self, project_id, user):
        """Create a template from a project."""
        project = self.get_if_authorized(project_id, user)
        template = Template.from_project(project, user)
        return template

    def create_project_from_template(self, template_id, manager):
        """
        Create a new projet from a JSON template.

        Args:
            template (std): The JSON template of the project. Will be modified.
            manager (User): The user that will be the manager of the project.
        """
        template = self.get_template_if_authorized(template_id, manager)
        project = Project.from_template(template)
        return project
