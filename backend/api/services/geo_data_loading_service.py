import io
import os
import subprocess as sp
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Any
from uuid import uuid4
from zipfile import ZipFile, is_zipfile

import fiona
import fiona.errors
import geopandas as gpd
import numpy as np
import pyproj
import pyproj.crs
import rasterio
from fiona.crs import from_epsg
from flask import current_app as app
from geoalchemy2.shape import from_shape, to_shape
from rasterio.features import shapes
from scipy import ndimage
from shapely.geometry import (
    GeometryCollection,
    LinearRing,
    LineString,
    MultiLineString,
    MultiPoint,
    MultiPolygon,
    Point,
    Polygon,
    shape,
)
from shapely.geometry.base import BaseGeometry
from shapely.ops import transform
from werkzeug.utils import secure_filename

from ..exceptions import RequestError, UploadError
from ..models import db
from ..models.geo_data import (
    DataAttributeNominal,
    DataAttributeQuantitative,
    Feature,
    GeoData,
    GeoFeature,
    GlobalData,
    WFSGeoData,
    WMSGeoData,
)
from . import wfs, wms

DEFAULT_SRID = 4326
DEFAULT_PROJECTION = f"epsg:{DEFAULT_SRID}"
DEFAULT_DRIVER = "ESRI Shapefile"


def get_temporary_directory():
    """
    Return a TemporaryDirectory object.

    If the 'TEMP_DIR' defined in the application configuration exists,
    the TemporaryDirectory object will use this location. Otherwise,
    use the system temporary directory.

    Returns:
        A file object directory which can be used in a context manager and
        will be deleted on completion of the context.
    """
    temp_dir_conf = app.config.get("TEMP_DIR", None)
    if temp_dir_conf is None or os.path.isdir(temp_dir_conf) is False:
        temp_dir = None
    else:
        temp_dir = temp_dir_conf
    return TemporaryDirectory(dir=temp_dir)


def convert_to_polygon(geom: BaseGeometry) -> Polygon:
    """Try converting geometry to a polygon.

    :param geom:
    :raises TypeError:
        if `geom` cannot be converted to a :class:`shapely.Polygon`
    :return: result as a polygon
    """
    match geom:
        case Polygon():
            return geom
        case Point():
            return Polygon(list(geom.coords) * 4)
        case LinearRing():
            return Polygon(geom.coords)
        case LineString():
            coords = list(geom.coords)
            return Polygon(coords + coords[::-1])
        case _:
            raise TypeError(
                f"{type(geom).__name__} cannot be converted to Polygon"
            )


def convert_to_multipolygon(geom: BaseGeometry) -> MultiPolygon:
    """Convert geometry to a multi polygon.

    :param geom:
    :return: result as a multi polygon
    """
    match geom:
        case MultiPolygon():
            return geom
        case Polygon():
            return MultiPolygon([geom])
        case MultiLineString():
            return MultiPolygon([convert_to_polygon(g) for g in geom.geoms])
        case MultiPoint():
            return MultiPolygon([convert_to_polygon(g) for g in geom.geoms])
        case GeometryCollection():
            polygons = []
            for g in geom.geoms:
                try:
                    polygons.append(convert_to_polygon(g))
                except TypeError:
                    polygons += list(convert_to_multipolygon(g).geoms)
            return MultiPolygon(polygons)
        case _:
            return convert_to_multipolygon(convert_to_polygon(geom))


def normalize_geometry_types(df: gpd.GeoDataFrame):
    """Normalize geometry types of geopandas dataframe.

    One single geometry type will be chosen between:

    * source geomerty type (if source already has one type)
    * :class:`shapely.Polygon` if source geometry can all be converted to
      polygons
    * :class:`shapely.MultiPolygon` otherwise

    :param df:
    :return: dataframe with one type of geometry
    """
    geom_types = gpd.io.file.infer_schema(df)["geometry"]
    if isinstance(geom_types, str):
        geom_types = [geom_types]
    if len(geom_types) == 1:
        return df

    multipoly = False
    for g_type in geom_types:
        if g_type in [
            "MultiPolygon",
            "LinearString",
            "MultiPoint",
            "MultiLineString",
            "GeometryCollection",
        ]:
            multipoly = True
            break

    if multipoly:
        df.geometry = df.geometry.apply(convert_to_multipolygon)
    else:
        df.geometry = df.geometry.apply(convert_to_polygon)

    return df


def save_geo_data(geo_data, directory, file_ext=None):
    """
    Save a geo data to the directory.

    Returns:
        The name of the new directory.
    """
    properties = []
    for feature in geo_data.features:
        prop = {p.attribute.name: p.value for p in feature.properties}
        properties.append(prop)
    df = gpd.GeoDataFrame(
        properties,
        geometry=gpd.GeoSeries([to_shape(f.geom) for f in geo_data.features]),
        crs=from_epsg(DEFAULT_SRID),
    )

    normalize_geometry_types(df)

    # Convert the booleans to int (booleans are not valid shapefile values)
    # see https://github.com/geopandas/geopandas/issues/437
    coltypes = gpd.io.file.infer_schema(df)["properties"]
    for colname, coltype in coltypes.items():
        if coltype == "bool":
            app.logger.info(
                "bool converted as int in {} while saving the data {}"
                "".format(colname, geo_data.name)
            )
            df[colname] = df[colname].astype("int")

    filename = secure_filename(geo_data.name)
    if file_ext is not None and file_ext.lower() != "zip":
        filename += "." + file_ext
    path = os.path.join(directory, filename)
    try:
        df.to_file(path)
    except fiona.errors.GeometryTypeValidationError:
        raise RequestError("Error in the geometries types", 500)
    except fiona.errors.DriverError:
        raise RequestError(f"FIle format unsupported '{file_ext}'", 415)
    return filename


def get_geo_data_download(geo_data, working_dir, file_ext=None):
    """Write the geo data to a file-like object."""
    filename = save_geo_data(geo_data, working_dir, file_ext)
    path = os.path.join(working_dir, filename)
    print(filename, path)
    print(os.path.isfile(path), os.path.isdir(path))
    if os.path.isfile(path):
        return filename, path
    elif os.path.isdir(path):
        buff = io.BytesIO()
        with ZipFile(buff, "w") as zf:
            files = os.listdir(path)
            for f in files:
                zf.write(os.path.join(path, f), os.path.join(filename, f))
        buff.flush()
        buff.seek(0)
        return filename + ".zip", buff


def ogr2ogr_reproject(file_in, dir_out):
    """
    Reproject the file in WGS84 using ogr2ogr and return the name of the new
    file.

    Args:
        file_in (str): Path of the input shapefile.
        dir_out (str): Directory to save the reprojected file.

    Returns:
        The path of the reprojected file, or None if there was an error.
    """
    ogr2ogr_bin = app.config.get("OGR2OGR_BIN", None)
    if ogr2ogr_bin is None:
        return None
    if os.path.isfile(ogr2ogr_bin) is False:
        app.logger.warning("ogr2ogr not found at {}".format(ogr2ogr_bin))
        return None
    out_filename = str(uuid4())
    file_out = os.path.join(dir_out, out_filename)
    args = [
        ogr2ogr_bin,
        "-f",
        DEFAULT_DRIVER,
        file_out,
        file_in,
        "-t_srs",
        DEFAULT_PROJECTION.upper(),
    ]
    try:
        sp.check_call(args)
        return out_filename
    except FileNotFoundError:
        return None
    except sp.CalledProcessError:
        return None


def validate_shp_filenames(filenames):
    """
    Validate the file names.

    A shapefile data must have at least 3 files with the extensions .shp, .shx
    and .dbf.

    Raises:
        UploadError:
            if a required file (shp, dbf or shx) is missing, or if
            more than one file is found
    """
    if len(filenames) < 3:
        raise UploadError(
            "The shapefile should contain at least 3 files: "
            ".shp, .shx and .dbf"
        )
    f_exts = []
    for f in filenames:
        _, f_ext = os.path.splitext(f)
        f_exts.append(f_ext.lower())
    required = (".shp", ".shx", ".dbf")
    missing = list(filter(lambda x: x not in f_exts, required))
    if len(missing) > 0:
        raise UploadError(
            "Missing files extensions: {}".format(", ".join(missing))
        )


def create_global_data(
    name, properties: dict[str, Any], **kwargs
) -> GlobalData:
    """Create a global data object.

    :param name:
    :param properties: set of properties organized by attribute
    :return: created global data object
    """
    data = GlobalData(name=name, feature=None, **kwargs)
    data.update_created()
    db.session.add(data)
    feature = Feature(data=data)
    data.feature = feature
    db.session.add(feature)
    values = []
    for aname, avalue in properties.items():
        attr = data._choose_attribute_type(avalue)(name=aname, data=data)
        values.append(
            attr.get_value_class()(
                value=avalue, attribute=attr, feature=feature
            )
        )
        data.attributes.append(attr)
    db.session.add_all(values)
    db.session.commit()
    return data


def load_vector_file(path):
    """
    Create and return a GeoData object created from a vector file.

    Tries to reproject the geometries to EPSG:4326.
    """
    with fiona.open(path, "r") as source:
        project = None
        if source.crs != {}:
            proj_out = pyproj.crs.CRS(DEFAULT_PROJECTION)
            proj_in = pyproj.crs.CRS(source.crs)
            if not proj_in.equals(proj_out):
                project = pyproj.Transformer.from_crs(
                    proj_in, proj_out
                ).transform
        geo_data = GeoData(
            name=source.name,
            original_name=source.name,
            source_driver=source.driver,
        )
        geo_data.update_created()
        # Generate an id for the geo data
        db.session.add(geo_data)
        db.session.flush()
        features = []
        values = []
        for aname, atype in source.schema["properties"].items():
            proptype = fiona.prop_type(atype)
            if proptype in (int, float):
                attr = DataAttributeQuantitative(
                    name=aname, data_id=geo_data.id
                )
            else:
                attr = DataAttributeNominal(name=aname, data_id=geo_data.id)
            geo_data.attributes.append(attr)
        attributes_dict = {a.name: a for a in geo_data.attributes}
        for s_feature in source:
            geom = shape(s_feature["geometry"])
            if project is not None:
                geom = transform(project, geom)
            geom_geometry = from_shape(geom, srid=DEFAULT_SRID)
            feature = GeoFeature(geom=geom_geometry, data_id=geo_data.id)
            geo_data.features.append(feature)
            props = s_feature["properties"]
            for prop, val in props.items():
                cls = attributes_dict[prop].get_value_class()
                values.append(
                    cls(
                        value=val,
                        attribute=attributes_dict[prop],
                        feature=feature,
                    )
                )
        db.session.add_all(values)
        db.session.add_all(features)
        db.session.commit()
        return geo_data


def load_wfs(wfs_stream: WFSGeoData) -> WFSGeoData:
    """Create and return a StreamGeoData object created from a WFS URL.

    :param wfs_stream:
        WFS stream to load (can be partial as long as it contains at least
        the server URL and feature type)
    :raise KeyError: if `wfs_stream.feature_type` is not found on WFS server
    :return:

    .. note:: WFS object is not persisted!
    """
    capabilities = wfs.get_capabilities(wfs_stream.url, wfs_stream.version)
    if wfs_stream.feature_type not in capabilities.feature_types:
        raise KeyError(
            f"No feature type found on server {wfs_stream.url} with given "
            f"name '{wfs_stream.feature_type}'"
        )
    feature_info = capabilities.feature_types[wfs_stream.feature_type]
    # Do we really need original_name field?
    wfs_stream.original_name = wfs_stream.feature_type
    wfs_stream.name = secure_filename(feature_info.name)
    wfs_stream.title = feature_info.title
    wfs_stream.description = feature_info.description
    wfs_stream.keywords = feature_info.keywords
    wfs_stream.update_created()
    return wfs_stream


def load_from_wfs(wfs_stream: WFSGeoData) -> GeoData:
    """Create and return a GeoData object created from a WFS stream.

    Tries to reproject the geometries to EPSG:4326.

    :param wfs_stream:
    :return: geo data version of the `wfs_stream` stream
    """
    df = wfs.get_feature(
        wfs_stream.url, wfs_stream.feature_type, wfs_stream.version
    )
    if df.crs:
        df = df.to_crs(DEFAULT_PROJECTION)
    geo_data = GeoData(
        name=wfs_stream.name,
        original_name=wfs_stream.original_name,
        source_driver="WFS",
    )
    geo_data.update_created()
    geo_data.stream = wfs_stream
    # Generate an id for the geo data
    db.session.add(geo_data)
    db.session.flush()
    features = []
    values = []
    for aname, atype in df.dtypes.items():
        if aname == df.geometry.name:
            continue
        if atype in (float, int):
            attr = DataAttributeQuantitative(name=aname, data_id=geo_data.id)
        else:
            attr = DataAttributeNominal(name=aname, data_id=geo_data.id)
        geo_data.attributes.append(attr)
    attributes_dict = {a.name: a for a in geo_data.attributes}
    for s_feature, geom in zip(df.iterfeatures(), df.geometry.values):
        geom_geometry = from_shape(geom, srid=DEFAULT_SRID)
        feature = GeoFeature(geom=geom_geometry, data_id=geo_data.id)
        geo_data.features.append(feature)
        for prop, val in s_feature["properties"].items():
            cls = attributes_dict[prop].get_value_class()
            values.append(
                cls(
                    value=val, attribute=attributes_dict[prop], feature=feature
                )
            )
    db.session.add_all(values)
    db.session.add_all(features)
    db.session.commit()
    return geo_data


def load_vector_from_files(files):
    """
    Create a new GeoData from a list of files.

    Saves the files in a temporary directory and load them as a new GeoData
    object.
    """
    with get_temporary_directory() as temp_dir:
        temp_files = []
        for f in files:
            # This file datastructure can be read only once
            # Without it the is_zipfile test seem to empty it for later cases
            # so other file type are not read properly
            path = os.path.join(temp_dir, secure_filename(f.filename))
            f.save(path)
            temp_files.append(path)
        if len(temp_files) == 1 and is_zipfile(temp_files[0]):
            # Pre-treatment on zipped shapefile
            file = temp_files.pop(0)
            with ZipFile(file) as zip_file:
                zip_infos = []
                for zi in zip_file.infolist():
                    basename = os.path.basename(zi.filename)
                    if basename != "":
                        zi.filename = secure_filename(basename)
                        zip_infos.append(zi)
                for zi in zip_infos:
                    temp_files.append(zip_file.extract(zi, path=temp_dir))
        if len(temp_files) > 1:
            # Shapefile
            validate_shp_filenames(temp_files)
            shape_file = None
            for f_name in temp_files:
                f_root, f_ext = os.path.splitext(f_name)
                if f_ext == ".shp":
                    # Stop at the first .shp file
                    shape_file = f_name
                    break
            if shape_file is None:
                raise UploadError("No file .shp found.")
            file_in = shape_file
        else:
            # Any other format
            file_in = temp_files[0]

        out_filename = ogr2ogr_reproject(file_in, temp_dir)
        fname = out_filename if out_filename is not None else file_in
        try:
            return load_vector_file(os.path.join(temp_dir, fname))
        except fiona.errors.DriverError as exc:
            message = str(exc).replace(
                os.path.join(temp_dir, fname), Path(file_in).suffix
            )
            raise UploadError(message)


def reclassify(array, classes):
    """Reclassify the raster values."""
    result = np.ma.masked_invalid(array)
    min_value, max_value = float("inf"), -float("inf")
    min_label, max_label = None, None
    for c in classes:
        v_min, v_max = sorted((c[0], c[1]))
        v_val = c[2]
        if v_min < min_value:
            min_value = v_min
            min_label = v_val
        if v_max > max_value:
            max_value = v_max
            max_label = v_val
        result.data[
            np.where((array >= v_min) & (array < v_max) & ~result.mask)
        ] = v_val
    result.data[np.where((array < min_value) & ~result.mask)] = min_label
    result.data[np.where((array >= max_value) & ~result.mask)] = max_label
    return result


def load_from_geotiff(path, classes, smooth=False):
    """Create a new geo-data from a GeoTiff file.

    Args:
        path (str): The path of the GeoTIFF file.
        classes (list):
            The classes of the vector features, a list of (min, max, value).
        smooth (bool):
            Whether to smooth the raster using a percentile_filter.
            Default: False.
    """
    with rasterio.open(path) as src:
        image = src.read(1, masked=True)
    return load_from_raster(image, src, classes=classes, smooth=smooth)


def load_wms(wms_stream: WMSGeoData) -> WMSGeoData:
    """Create and return a StreamGeoData object created from a WFS URL.

    :param wms_stream:
        WMS stream to load (can be partial as long as it contains at least
        the server URL and layer)
    :raise KeyError: if `wms_stream.layer` is not found on WMS server
    :return:

    .. note:: WMS object is not persisted!
    """
    capabilities = wms.get_capabilities(wms_stream.url, wms_stream.version)
    if wms_stream.layer not in capabilities.queryable_layers:
        raise KeyError(
            f"No queryable layer found on server {wms_stream.url} "
            f"with given name '{wms_stream.layer}'"
        )
    layer_info = capabilities.layers[wms_stream.layer]
    # Do we really need original_name field?
    wms_stream.original_name = wms_stream.layer
    wms_stream.name = secure_filename(layer_info.name)
    wms_stream.title = layer_info.title
    wms_stream.description = layer_info.description
    wms_stream.keywords = layer_info.keywords
    wms_stream.update_created()
    return wms_stream


def load_from_wms(
    wms_stream: WMSGeoData,
    classes: list = None,
    smooth: bool = False,
    resolution: float = None,
):
    """Create a new geo-data from a WMS url.

    :param wms_stream: The WMS stream.
    :param classes:
        The classes of the vector features, a list of (min, max, value).
    :param smooth:
        Whether to smooth the raster using a percentile_filter.
        Default: False.
    """
    img, src = wms.get_map(
        wms_stream.url,
        wms_stream.layer,
        wms_stream.version,
        resolution=(
            wms_stream.resolution if resolution is None else resolution
        ),
    )
    classes = wms_stream.classes if classes is None else classes
    geodata = _load_from_raster(img, src, classes=classes, smooth=smooth)
    geodata.stream = wms_stream
    db.session.commit()
    return geodata


def _load_from_raster(
    image, src, classes, smooth: bool = False, name: str = None
) -> GeoData:
    """Create a new geo-data from a loaded raster image and its source.

    Does not commit to database but prepare transaction.

    Args:
        image: the loaded raster image (previously read from `src`)
        src: dataset reader
        classes (list):
            The classes of the vector features, a list of (min, max, value).
        smooth (bool):
            Whether to smooth the raster using a percentile_filter.
            Default: False.
        name (str): name to use for the geo-data. Default: `src.name`.
    """
    image = image.astype(np.float32)
    image[np.where(image.mask)] = np.nan
    image = reclassify(image, classes)

    if smooth:
        filtered = ndimage.percentile_filter(image, percentile=25, size=7)
        np.copyto(image.data, filtered, where=(~image.mask))

    results = [
        {"value": v, "geometry": s}
        for i, (s, v) in enumerate(
            shapes(
                image,
                mask=(~image.mask & np.isfinite(image.data)),
                transform=src.transform,
            )
        )
    ]

    project = None
    if src.crs != {}:
        proj_out = pyproj.crs.CRS(DEFAULT_PROJECTION)
        proj_in = pyproj.crs.CRS(src.crs)
        if not proj_in.equals(proj_out):
            project = pyproj.Transformer.from_crs(proj_in, proj_out).transform

    name = src.name if name is None else name
    name = os.path.basename(name)
    name = os.path.splitext(name)[0]
    geo_data = GeoData(name=name, original_name=name, source_driver=src.driver)
    geo_data.update_created()
    db.session.add(geo_data)
    db.session.flush()

    if all(
        (
            isinstance(r["value"], int) or isinstance(r["value"], float)
            for r in results
        )
    ):
        attr = DataAttributeQuantitative(name="value", data=geo_data)
    else:
        attr = DataAttributeNominal(name="value", data=geo_data)
    db.session.add(attr)
    db.session.flush()

    features = []
    values = []
    val_cls = attr.get_value_class()
    for result in results:
        geom = shape(result["geometry"])
        if project is not None:
            geom = transform(project, geom)
        geom_geometry = from_shape(geom, srid=DEFAULT_SRID)
        feature = GeoFeature(geom=geom_geometry, data_id=geo_data.id)
        values.append(
            val_cls(value=result["value"], attribute=attr, feature=feature)
        )
        features.append(feature)
    db.session.add_all(features)
    db.session.add_all(values)
    return geo_data


def load_from_raster(
    image, src, classes, smooth: bool = False, name: str = None
) -> GeoData:
    """Create a new geo-data from a loaded raster image and its source.

    Args:
        image: the loaded raster image (previously read from `src`)
        src: dataset reader
        classes (list):
            The classes of the vector features, a list of (min, max, value).
        smooth (bool):
            Whether to smooth the raster using a percentile_filter.
            Default: False.
        name (str): name to use for the geo-data. Default: `src.name`.
    """
    geo_data = _load_from_raster(image, src, classes, smooth=smooth, name=name)
    db.session.commit()
    return geo_data


def load_raster_from_files(files, classes):
    """Create a new vectorized GeoData from a raster file."""
    with get_temporary_directory() as temp_dir:
        if len(files) != 1:
            raise UploadError("A raster data should use only one GeoTiff file")
        file_obj = files[0]
        file_name = secure_filename(file_obj.filename)
        file_path = os.path.join(temp_dir, file_name)
        file_obj.save(file_path)
        geo_data = load_from_geotiff(file_path, classes=classes)
        return geo_data
