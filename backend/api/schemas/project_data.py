from marshmallow import fields, validates

from ..models import (
    BaseGeoData,
    DataGenerator,
    DataGeo,
    DataType,
    ProjectData,
    ProjectGlobalData,
)
from .base import (
    BaseSchema,
    FieldMethod,
    FieldPluck,
    OneOfSchema,
    OneOfSchemaWithType,
    constant_enum,
)
from .preference import (
    PrefDefaultValuesAttributeSchema,
    PreferenceModelCreationSchema,
    PrefModelBaseSchema,
)
from .project import ProjectSchema
from .utils import validate_name


class ProjectDataBaseSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = ProjectData
        exclude = ("referenced",)
        dump_only = ("id", "data_type", "data", "last_update", "project")

    project = fields.Nested(ProjectSchema)
    data = FieldPluck(BaseGeoData, "id")
    data_type = fields.Enum(DataType, required=True)
    attributes = FieldMethod(
        fields.Nested(
            PrefDefaultValuesAttributeSchema,
            only=("name", "attribute", "type"),
            many=True,
        ),
        "get_attributes_list",
    )

    @validates("name")
    def validate_name(self, value, **kwargs):
        validate_name(value)

    def get_attributes_list(self, obj):
        return obj.get_attributes_list()


class ProjectDataBaseCreationSchema(ProjectDataBaseSchema):
    pass


class DataGeoSchema(ProjectDataBaseSchema):
    class Meta(ProjectDataBaseSchema.Meta):
        model = DataGeo
        fields = (
            "id",
            "data_type",
            "last_update",
            "project",
            "description",
            "name",
            "data",
            "attributes",
            "data_id",
        )
        exclude = ()
        dump_only = ("id", "data_type", "data", "last_update", "project")
        load_only = ("data_id",)

    data_id = fields.Integer()
    data_type = fields.Enum(constant_enum(DataType.geo_data), required=True)


class DataGeoCreationSchema(DataGeoSchema):
    """Schema for the creation of a new project geo data"""

    data_id = fields.Integer(required=True)


class ProjectGlobalDataSchema(ProjectDataBaseSchema):
    class Meta(ProjectDataBaseSchema.Meta):
        model = ProjectGlobalData
        fields = (
            "id",
            "data_type",
            "last_update",
            "project",
            "description",
            "name",
            "data",
            "attributes",
            "data_id",
        )
        exclude = ()
        dump_only = ("id", "data_type", "data", "last_update", "project")
        load_only = ("data_id",)

    data_id = fields.Integer()
    data_type = fields.Enum(constant_enum(DataType.global_data), required=True)


class ProjectGlobalDataCreationSchema(ProjectGlobalDataSchema):
    """Schema for the creation of a new project global data"""

    data_id = fields.Integer(required=True)


class DataGeneratorSchema(ProjectDataBaseSchema):
    class Meta(ProjectDataBaseSchema.Meta):
        model = DataGenerator
        fields = (
            "id",
            "data_type",
            "last_update",
            "project",
            "description",
            "name",
            "input_data",
            "data",
            "preference_model",
            "attributes",
            "is_outdated",
            "pref_models_list",
        )
        exclude = ()
        dump_only = (
            "id",
            "data_type",
            "last_update",
            "project",
            "data",
            "is_outdated",
            "pref_models_list",
        )

    data_type = fields.Enum(constant_enum(DataType.generator), required=True)
    input_data = FieldPluck(ProjectData, "id", many=True)
    is_outdated = FieldMethod(fields.Boolean(), "check_outdated")
    # This field is used to create new preference models
    preference_model = fields.Nested(
        PreferenceModelCreationSchema(exclude=("data_generator",))
    )
    pref_models_list = fields.Nested(
        PrefModelBaseSchema,
        attribute="_preference_models",
        many=True,
        only=("id", "pref_type", "name"),
    )
    # is_modified = fields.Method('check_modified')

    def check_outdated(self, obj):
        return obj.is_outdated()


class DataGeneratorCreationSchema(DataGeneratorSchema):
    pass


# The inheritance from ProjectDataBaseSchema is necessary so fields.Pluck
# nesting this schema will work (they test field on ProjectDataSchema
# not on its type schemas)
class ProjectDataSchema(OneOfSchema, ProjectDataBaseSchema):
    type_field = "data_type"
    type_schemas = {
        DataType.geo_data.name: DataGeoSchema,
        DataType.generator.name: DataGeneratorSchema,
        DataType.global_data.name: ProjectGlobalDataSchema,
    }

    def get_obj_type(self, obj):
        return obj.data_type.name


class ProjectDataCreationSchema(
    OneOfSchemaWithType, ProjectDataBaseCreationSchema
):
    type_field = "data_type"
    type_schemas = {
        DataType.geo_data.name: DataGeoCreationSchema,
        DataType.generator.name: DataGeneratorCreationSchema,
        DataType.global_data.name: ProjectGlobalDataCreationSchema,
    }

    def get_obj_type(self, obj):
        return obj.data_type.name


class ProjectDataGeneratorCreationSchema(
    OneOfSchemaWithType, ProjectDataBaseCreationSchema
):
    type_field = "data_type"
    type_schemas = {DataType.generator.name: DataGeneratorCreationSchema}

    def get_obj_type(self, obj):
        return obj.data_type.name
