from flask import current_app as app
from marshmallow import Schema, fields, pre_dump

from ..models import DataAttribute
from ..models.preference import (
    AttributeType,
    ContinuousRule,
    DefaultValue,
    DiscreteCategory,
    DiscreteRules,
    DiscreteRulesCategory,
    GeoBuffer,
    KeepOverlap,
    MRSort,
    MRSortCriterion,
    PrefDefaultValues,
    PreferenceModel,
    PrefType,
    WeightedSum,
    WeightedSumOperand,
)
from .base import (
    BaseSchema,
    FieldMethod,
    FieldPluck,
    OneOfSchema,
    OneOfSchemaWithType,
    constant_enum,
)
from .geo_data import BaseDataAttributeSchema


class PrefModelBaseSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = PreferenceModel
        dump_only = ("id", "pref_type", "data_generator")

    pref_type = fields.Enum(PrefType, required=True)
    keep_overlap = fields.Enum(KeepOverlap)
    data_generator = fields.Pluck("DataGeneratorSchema", "id")


class PrefModelBaseCreationSchema(PrefModelBaseSchema):
    pass


class GeoBufferSchema(PrefModelBaseSchema):
    class Meta(PrefModelBaseSchema.Meta):
        model = GeoBuffer
        fields = (
            "id",
            "pref_type",
            "data_generator",
            "cut_to_extent",
            "dissolve_adjacent",
            "radius",
            "name",
        )

    pref_type = fields.Enum(constant_enum(PrefType.geo_buffer), required=True)


class GeoBufferCreationSchema(GeoBufferSchema):
    pass


class DiscreteCategorySchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = DiscreteCategory
        fields = ("id", "name", "position")
        load_only = ("position",)


class DiscreteRulesCategorySchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = DiscreteRulesCategory
        fields = ("id", "name", "position", "rules")


class DiscreteRulesSchema(PrefModelBaseSchema):
    class Meta(PrefModelBaseSchema.Meta):
        model = DiscreteRules
        fields = (
            "id",
            "pref_type",
            "data_generator",
            "cut_to_extent",
            "dissolve_adjacent",
            "categories",
            "name",
            "keep_overlap",
        )

    pref_type = fields.Enum(
        constant_enum(PrefType.categories_rule), required=True
    )
    categories = fields.Nested(
        DiscreteRulesCategorySchema, many=True, only=("name", "rules")
    )


class DiscreteRulesCreationSchema(DiscreteRulesSchema):
    pass


class MRSortCriterionSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = MRSortCriterion
        fields = (
            "attribute_id",
            "mrsort_id",
            "attribute",
            "profiles",
            "weight",
            "maximize",
        )
        load_only = ("attribute_id", "mrsort_id")
        dump_only = ("attribute",)

    attribute_id = fields.Integer()
    mrsort_id = fields.Integer()
    attribute = fields.Nested(
        BaseDataAttributeSchema, only=("id", "name", "data")
    )


class MRSortSchema(PrefModelBaseSchema):
    class Meta(PrefModelBaseSchema.Meta):
        model = MRSort
        fields = (
            "id",
            "pref_type",
            "data_generator",
            "cut_to_extent",
            "dissolve_adjacent",
            "categories",
            "criteria",
            "majority_threshold",
            "name",
            "keep_overlap",
        )

    pref_type = fields.Enum(constant_enum(PrefType.mrsort), required=True)
    categories = FieldMethod(
        fields.Nested(DiscreteCategorySchema, many=True),
        serialize="get_categories",
        deserialize="load_categories",
    )
    criteria = FieldMethod(
        fields.Nested(
            MRSortCriterionSchema(exclude=("mrsort_id",)), many=True
        ),
        "get_criteria",
        deserialize="load_criteria",
    )

    def get_categories(self, obj):
        schema = DiscreteCategorySchema(many=True)
        data = schema.dump(obj.categories)
        return data

    def load_categories(self, value):
        """Load the categories from a dictionary.

        If there is already an existing category with the same name,
        reuse the same object instance.
        """
        new_categories = []
        schema = DiscreteCategorySchema()
        # Check if the MR-Sort instance exists
        if self.instance is not None:
            categories = {c.name: c for c in self.instance.categories}
        else:
            categories = {}
        # Create the categories instances
        for category_data in value:
            name = category_data.get("name")
            # Get the category with the same name if it exists
            instance = categories.get(name, None)
            category_obj = schema.load(category_data, instance=instance)
            new_categories.append(category_obj)
        # Set the position used for the ordering in the database
        for position, category in enumerate(new_categories):
            category.position = position
        return new_categories

    def get_criteria(self, obj):
        schema = MRSortCriterionSchema(
            many=True, only=("attribute", "profiles", "weight", "maximize")
        )
        data = schema.dump(obj.criteria)
        return data

    def load_criteria(self, value):
        new_criteria = []
        schema = MRSortCriterionSchema()
        for criterion_data in value:
            criterion_data["mrsort_id"] = self.instance.id
            instance = schema.get_instance(criterion_data)
            criterion_obj = schema.load(criterion_data, instance=instance)
            new_criteria.append(criterion_obj)
        return new_criteria

    @pre_dump
    def init_criteria(self, mrsort, **kwargs):
        """Load the criteria of the MR-Sort data"""
        app.logger.info("Update the MR-Sort criteria")
        mrsort.init_criteria()
        mrsort.update()
        return mrsort


class MRSortCreationSchema(MRSortSchema):
    pass


class DefaultValueSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = DefaultValue
        fields = ("attribute", "value")

    attribute = FieldPluck(DataAttribute, "id")


class PrefDefaultValuesAttributeSchema(Schema):
    id = fields.Integer()
    name = fields.String()
    attribute = fields.String()
    type = fields.Enum(AttributeType, required=True)


class PrefDefaultValuesBaseSchema(PrefModelBaseSchema):
    class Meta(PrefModelBaseSchema):
        model = PrefDefaultValues

    # As it has no deserialize method we don't need to add it to the dump_only
    # fields
    attributes_list = FieldMethod(
        fields.Nested(PrefDefaultValuesAttributeSchema, many=True),
        "get_attributes_list",
    )
    default_values = fields.Nested(
        DefaultValueSchema, many=True, only=("attribute", "value")
    )

    def get_attributes_list(self, obj):
        attrs = []
        for input_data in obj.data_generator.input_data:
            if input_data.data is None:
                continue
            for attr in input_data.data.attributes:
                attrs.append(
                    {
                        "id": attr.id,
                        "name": input_data.name,
                        "attribute": attr.name,
                        "type": attr.type.name,
                    }
                )
        return attrs


class WeightedSumOperandSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = WeightedSumOperand

    attribute = FieldPluck(DataAttribute, "id")


class WeightedSumSchema(PrefDefaultValuesBaseSchema):
    class Meta(PrefModelBaseSchema.Meta):
        model = WeightedSum

    pref_type = fields.Enum(
        constant_enum(PrefType.weighted_sum), required=True
    )
    operands = fields.Nested(
        WeightedSumOperandSchema, many=True, only=("attribute", "weight")
    )


class WeightedSumCreationSchema(WeightedSumSchema):
    pass


class ContinuousRuleSchema(PrefDefaultValuesBaseSchema):
    class Meta(PrefModelBaseSchema.Meta):
        model = ContinuousRule

    pref_type = fields.Enum(
        constant_enum(PrefType.continuous_rule), required=True
    )
    keep_overlap = fields.Enum(KeepOverlap)


class ContinuousRuleCreationSchema(ContinuousRuleSchema):
    pass


class PreferenceModelSchema(OneOfSchema, PrefModelBaseSchema):
    type_field = "pref_type"
    type_schemas = {
        PrefType.categories_rule.name: DiscreteRulesSchema,
        PrefType.continuous_rule.name: ContinuousRuleSchema,
        PrefType.geo_buffer.name: GeoBufferSchema,
        PrefType.mrsort.name: MRSortSchema,
        PrefType.weighted_sum.name: WeightedSumSchema,
    }

    def get_obj_type(self, obj):
        return obj.pref_type.name


class PreferenceModelCreationSchema(
    OneOfSchemaWithType, PrefModelBaseCreationSchema
):
    type_field = "pref_type"
    type_schemas = {
        PrefType.categories_rule.name: DiscreteRulesCreationSchema,
        PrefType.continuous_rule.name: ContinuousRuleCreationSchema,
        PrefType.geo_buffer.name: GeoBufferCreationSchema,
        PrefType.mrsort.name: MRSortCreationSchema,
        PrefType.weighted_sum.name: WeightedSumCreationSchema,
    }

    def get_obj_type(self, obj):
        return obj.pref_type.name
