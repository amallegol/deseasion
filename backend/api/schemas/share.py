from marshmallow import fields

from ..models import DataShare
from .base import BaseSchema
from .project_data import ProjectDataSchema


class DataShareSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        # This schema is used for creation/retrieval only
        model = DataShare
        dump_only = ("id", "uid", "data")

    data = fields.Pluck(ProjectDataSchema, "id")
