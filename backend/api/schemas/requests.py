"""This module contains all schemas used as response/requestBody types.
"""

from marshmallow import Schema, fields

from ..models import DataType, PreferenceModel, ProjectTaskType
from .base import FieldPluck, OneOfSchema, constant_enum
from .geo_data import (
    BaseGeoDataSchema,
    DataAttributeSchema,
    DataAttributeSchemaWithValues,
    FeatureSchema,
    GeoDataAccessSchema,
    GeoDataSchema,
    GlobalDataAccessSchema,
    StreamGeoDataAccessSchema,
    StreamGeoDataSchema,
)
from .mrsort_inference import MRSortInferenceSchema
from .preference import PreferenceModelSchema
from .project import (
    ProjectAccessSchema,
    ProjectHierarchySchema,
    ProjectSchema,
    TemplateSchema,
)
from .project_data import ProjectDataBaseSchema, ProjectDataSchema
from .schemas import GeneticAlgorithmSchema, ProjectTaskSchema, UserSchema
from .share import DataShareSchema


class MessageSchema(Schema):
    """This represents a simple message.

    Mainly used for error responses.
    """

    message = fields.String()


class DataAttributeGetResponse(Schema):
    attribute = fields.Nested(DataAttributeSchema)


class DataAttributeWithValuesGetResponse(Schema):
    attribute = fields.Nested(DataAttributeSchemaWithValues)


class GeoDataGetResponse(Schema):
    geodata = fields.Nested(GeoDataSchema())


class GeoDataListResponseSchema(Schema):
    geodata = fields.Nested(GeoDataSchema, many=True)


class GeoDataAccessResponseSchema(Schema):
    access = fields.Nested(GeoDataAccessSchema)


class StreamGeoDataResponseSchema(Schema):
    stream = fields.Nested(StreamGeoDataSchema)


class StreamGeoDataListResponseSchema(Schema):
    stream = fields.Nested(StreamGeoDataSchema, many=True)


class StreamGeoDataVersionsSchema(Schema):
    versions = fields.Nested(
        GeoDataSchema,
        only=("id", "name", "description", "created_at"),
        many=True,
    )


class StreamGeoDataAccessResponseSchema(Schema):
    access = fields.Nested(StreamGeoDataAccessSchema)


class StreamGeoDataPostRequestSchema(Schema):
    url = fields.String(required=True)


class WMSGeoDataPostRequestSchema(StreamGeoDataPostRequestSchema):
    classes = fields.List(fields.List(fields.Float), required=False)


class WMSStreamClassesSchema(Schema):
    classes = fields.List(fields.List(fields.Float), required=False)
    resolution = fields.Float(required=False)


class FeatureResponseSchema(Schema):
    feature = fields.Nested(FeatureSchema)


class UserAccessTokenResponseSchema(Schema):
    access_token = fields.String()
    token_type = fields.String()


class UserTokenResponseSchema(UserAccessTokenResponseSchema):
    refresh_token = fields.String()


class ProjectDataListGetResponseSchema(Schema):
    project_data = fields.Nested(ProjectDataSchema, many=True)


class ProjectProcessTaskSchema(ProjectTaskSchema):
    type = fields.Enum(constant_enum(ProjectTaskType.process_project_data))


class ProjectZonePropositionTaskSchema(ProjectTaskSchema):
    type = fields.Enum(constant_enum(ProjectTaskType.process_zone_proposition))


class ProjectDataPostResponseSchema(Schema):
    task = fields.Nested(ProjectProcessTaskSchema)


class ProjectDataGetResponseSchema(Schema):
    project_data = fields.Nested(ProjectDataSchema)


class ProjectDataInputListPostResponseSchema(Schema):
    input_data = fields.Nested(
        ProjectDataBaseSchema, only=("id", "name", "data_type"), many=True
    )


class ProjectDataActiveModelPutResponseSchema(Schema):
    preference_model = fields.Nested(PreferenceModelSchema)


class ProjectDataModelChangePostRequestBodySchema(Schema):
    id = FieldPluck(PreferenceModel, "id", required=True)


class MRSortInferenceGetResponseSchema(Schema):
    inference_data = fields.Nested(MRSortInferenceSchema)


class ProjectListGetResponseSchema(Schema):
    projects = fields.Nested(ProjectSchema, many=True)


class ProjectFromTemplateRequestBody(Schema):
    template = fields.Integer(required=True)
    name = fields.String(allow_none=True)
    description = fields.String(allow_none=True)
    extent = fields.List(
        fields.Float(),
        metadata={"minItems": 4, "maxItems": 4},
        allow_none=True,
    )


class ProjectGetResponseSchema(Schema):
    project = fields.Nested(ProjectSchema)


class ProjectPermissionGetResponseSchema(Schema):
    access = fields.Nested(ProjectAccessSchema)


class ProjectSharedDataListPostResponseSchema(Schema):
    shared_data = fields.Nested(
        ProjectDataBaseSchema,
        only=("name", "id", "data", "data_type"),
        many=True,
    )


class ProjectSharedBaseDataSchema(Schema):
    id = fields.Integer()
    name = fields.String()
    data = fields.Pluck(BaseGeoDataSchema, "id")
    data_type = fields.Enum(DataType)


class ProjectSharedGeoDataSchema(ProjectSharedBaseDataSchema):
    data_type = fields.Enum(constant_enum(DataType.geo_data))


class ProjectSharedGeneratedDataSchema(ProjectSharedBaseDataSchema):
    data_type = fields.Enum(constant_enum(DataType.generator))
    preference_model = fields.Nested(
        PreferenceModelSchema(exclude=("data_generator",))
    )


class ProjectSharedDataSchema(OneOfSchema, ProjectSharedBaseDataSchema):
    type_field = "data_type"
    type_schemas = {
        DataType.geo_data.name: ProjectSharedGeoDataSchema,
        DataType.generator.name: ProjectSharedGeneratedDataSchema,
    }

    def get_obj_type(self, obj):
        return obj.data_type.name


class ProjectSharedDataListGetResponseSchema(Schema):
    shared_data = fields.Nested(ProjectSharedDataSchema, many=True)


class ProjectHierarchyGetResponse(Schema):
    hierarchy = fields.Nested(ProjectHierarchySchema)


class ProjectObjectiveRequestBody(Schema):
    geo_size = fields.Number(metadata={"minimum": 0, "exclusiveMinimum": True})
    iterations = fields.Integer(required=False, metadata={"minimum": 0})
    duration = fields.Number(required=False, metadata={"minimum": 0})
    genetic_algorithm_params = fields.Nested(GeneticAlgorithmSchema)


class ProjectObjectivePostResponseSchema(Schema):
    task = fields.Nested(ProjectZonePropositionTaskSchema)


class ProjectTemplatePostResponseSchema(Schema):
    template = fields.Nested(TemplateSchema)


class ProjectTemplateListGetResponseSchema(Schema):
    templates = fields.Nested(TemplateSchema, many=True)


class ProjectSharesGetResponseSchema(Schema):
    shared = fields.Nested(DataShareSchema, many=True)


class ProjectSharePostResponseSchema(Schema):
    shared = fields.Nested(DataShareSchema)


class ProjectTaskListGetResponseSchema(Schema):
    tasks = fields.Nested(ProjectTaskSchema, many=True)
    total = fields.Integer()


class TaskGetResponseSchema(Schema):
    task = fields.Nested(ProjectTaskSchema)


class UserListGetResponseSchema(Schema):
    users = fields.Nested(UserSchema, only=("id", "username"), many=True)


class GlobalDataAccessResponseSchema(Schema):
    access = fields.Nested(GlobalDataAccessSchema)
