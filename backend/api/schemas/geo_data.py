from geoalchemy2.shape import to_shape
from marshmallow import Schema, fields, pre_dump, validates

from ..models import (
    AttributeType,
    BaseData,
    BaseGeoData,
    DataAttribute,
    DataAttributeNominal,
    DataAttributeOrdinal,
    DataAttributeQuantitative,
    DataType,
    ExplainabilityType,
    Feature,
    FeatureType,
    GeneratedGeoData,
    GeoData,
    GlobalData,
    StreamDataType,
    StreamGeoData,
    WFSGeoData,
    WMSGeoData,
)
from .base import (
    BaseSchema,
    FieldMethod,
    FieldPluck,
    OneOfSchema,
    constant_enum,
)
from .schemas import UserSchema
from .utils import validate_name


class BaseDataAttributeSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = DataAttribute
        fields = ("id", "name", "statistics", "data", "type")
        # This schema is never use for creation nor update
        # so basically all fields are not updatable nor creatable (no write)
        dump_only = ("id", "name", "statistics", "data", "type")

    data = fields.Nested("BaseDataSchema", only=("id", "name"))
    type = fields.Enum(
        AttributeType,
        required=True,
        metadata={"description": "Type of attribute"},
    )

    def get_values(self, obj):
        return [v.value for v in obj.values]


class BaseDataAttributeSchemaWithValues(BaseDataAttributeSchema):
    class Meta(BaseDataAttributeSchema.Meta):
        model = DataAttribute
        fields = ("id", "name", "statistics", "data", "values", "type")
        # This schema is never use for creation nor update
        # so basically all fields are not updatable nor creatable (no write)
        dump_only = ("id", "name", "statistics", "data", "values", "type")

    values = FieldMethod(fields.List(fields.Dict), "get_values")

    def get_values(self, obj):
        return [v.value for v in obj.values]


class DataAttributeQuantitativeSchema(BaseDataAttributeSchema):
    class Meta(BaseDataAttributeSchema.Meta):
        model = DataAttributeQuantitative

    type = fields.Enum(
        constant_enum(AttributeType.quantitative), required=True
    )


class DataAttributeNominalSchema(BaseDataAttributeSchema):
    class Meta(BaseDataAttributeSchema.Meta):
        model = DataAttributeNominal

    type = fields.Enum(constant_enum(AttributeType.nominal), required=True)


class DataAttributeOrdinalSchema(BaseDataAttributeSchema):
    class Meta(BaseDataAttributeSchema.Meta):
        model = DataAttributeOrdinal
        fields = ("id", "name", "statistics", "order", "data", "type")
        # This schema is never use for creation nor update
        # so basically all fields are not updatable nor creatable (no write)
        dump_only = ("id", "name", "statistics", "order", "data", "type")

    type = fields.Enum(constant_enum(AttributeType.ordinal), required=True)


class DataAttributeQuantitativeSchemaWithValues(
    BaseDataAttributeSchemaWithValues
):
    class Meta(BaseDataAttributeSchemaWithValues.Meta):
        model = DataAttributeQuantitative

    type = fields.Enum(
        constant_enum(AttributeType.quantitative), required=True
    )


class DataAttributeNominalSchemaWithValues(BaseDataAttributeSchemaWithValues):
    class Meta(BaseDataAttributeSchemaWithValues.Meta):
        model = DataAttributeNominal

    type = fields.Enum(constant_enum(AttributeType.nominal), required=True)


class DataAttributeOrdinalSchemaWithValues(BaseDataAttributeSchemaWithValues):
    class Meta(BaseDataAttributeSchemaWithValues.Meta):
        model = DataAttributeOrdinal
        fields = (
            "id",
            "name",
            "statistics",
            "order",
            "data",
            "values",
            "type",
        )
        # This schema is never use for creation nor update
        # so basically all fields are not updatable nor creatable (no write)
        dump_only = (
            "id",
            "name",
            "statistics",
            "order",
            "data",
            "values",
            "type",
        )

    type = fields.Enum(constant_enum(AttributeType.ordinal), required=True)


class DataAttributeSchema(OneOfSchema, BaseDataAttributeSchema):
    type_schemas = {
        AttributeType.quantitative.name: DataAttributeQuantitativeSchema,
        AttributeType.nominal.name: DataAttributeNominalSchema,
        AttributeType.ordinal.name: DataAttributeOrdinalSchema,
    }

    def get_obj_type(self, obj):
        return obj.type.name


class DataAttributeSchemaWithValues(
    OneOfSchema, BaseDataAttributeSchemaWithValues
):
    type_schemas = {
        AttributeType.quantitative.name: (
            DataAttributeQuantitativeSchemaWithValues
        ),
        AttributeType.nominal.name: DataAttributeNominalSchemaWithValues,
        AttributeType.ordinal.name: DataAttributeOrdinalSchemaWithValues,
    }

    def get_obj_type(self, obj):
        return obj.type.name


class BaseStreamGeoDataSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = StreamGeoData
        exclude = ("_upload_user",)
        # This schema is never use for creation, so all below fields
        # are the non-updatable
        # is_public can only be changed through permission schema
        dump_only = (
            "id",
            "url",
            "type",
            "is_public",
            "versions",
            "upload_user",
            "original_name",
            "created_at",
            "modified_at",
        )

    name = fields.String()  # prevent the name field from being required
    type = fields.Enum(StreamDataType)
    upload_user = fields.Nested(UserSchema, only=("id", "username"))
    versions = FieldPluck(GeoData, "id", many=True)

    @validates("name")
    def validate_name(self, value, **kwargs):
        validate_name(value)


class WFSGeoDataSchema(BaseStreamGeoDataSchema):
    class Meta(BaseStreamGeoDataSchema.Meta):
        model = WFSGeoData
        # This schema is never use for creation, so all below fields are the
        # non-updatable
        # is_public can only be changed through permission schema
        dump_only = (
            "id",
            "url",
            "type",
            "is_public",
            "versions",
            "feature_type",
            "upload_user",
            "original_name",
            "created_at",
            "modified_at",
        )

    type = fields.Enum(constant_enum(StreamDataType.wfs))


class WFSGeoDataCreationSchema(WFSGeoDataSchema):
    class Meta(WFSGeoDataSchema.Meta):
        exclude = (
            "id",
            "type",
            "name",
            "original_name",
            "title",
            "description",
            "keywords",
            "_upload_user",
            "upload_user",
            "versions",
            "is_public",
            "created_at",
            "modified_at",
        )
        dump_only = ()


class WMSGeoDataSchema(BaseStreamGeoDataSchema):
    class Meta(BaseStreamGeoDataSchema.Meta):
        model = WMSGeoData
        dump_only = (
            "id",
            "url",
            "type",
            "is_public",
            "versions",
            "layer",
            "upload_user",
            "original_name",
            "created_at",
            "modified_at",
        )

    type = fields.Enum(constant_enum(StreamDataType.wms))
    classes = fields.List(fields.List(fields.Float))


class WMSGeoDataCreationSchema(WMSGeoDataSchema):
    class Meta(WMSGeoDataSchema.Meta):
        exclude = (
            "id",
            "type",
            "name",
            "original_name",
            "title",
            "description",
            "keywords",
            "_upload_user",
            "upload_user",
            "versions",
            "is_public",
            "created_at",
            "modified_at",
        )
        dump_only = ()


class StreamGeoDataSchema(OneOfSchema, BaseStreamGeoDataSchema):
    type_field = "type"
    type_schemas = {
        StreamDataType.wfs.name: WFSGeoDataSchema,
        StreamDataType.wms.name: WMSGeoDataSchema,
    }

    def get_obj_type(self, obj):
        return obj.type.name


class StreamGeoDataPermissionSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = StreamGeoData.Permission
        # All fields can be used for creation, update and retrieval
        fields = ("user",)

    user = fields.Pluck(UserSchema, "id")


class StreamGeoDataAccessSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = StreamGeoData
        # All fields can be used for creation, update and retrieval
        fields = ("is_public", "permissions")

    permissions = fields.Nested(StreamGeoDataPermissionSchema, many=True)


class BaseDataSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = BaseData
        fields = (
            "id",
            "type",
            "name",
            "attributes",
            "created_at",
            "modified_at",
        )
        # This schema is never use for creation, so all below fields are the
        # non-updatable
        # is_public can only be changed through permission schema
        dump_only = ("id", "type", "attributes", "created_at", "modified_at")

    type = fields.Enum(DataType)
    name = fields.String(required=False)
    attributes = fields.Nested(
        DataAttributeSchema, many=True, exclude=("data",)
    )

    @validates("name")
    def validate_name(self, value, **kwargs):
        validate_name(value)

    @pre_dump
    def load_properties(self, data, **kwargs):
        """Load the computed properties of the object (stats, attributes...)"""
        data.load_properties()
        return data


class BaseGeoDataSchema(BaseDataSchema):
    class Meta(BaseDataSchema.Meta):
        model = BaseGeoData
        fields = (
            "id",
            "type",
            "name",
            "attributes",
            "extent",
            "created_at",
            "modified_at",
        )
        # This schema is never use for creation, so all below fields are the
        # non-updatable
        # is_public can only be changed through permission schema
        dump_only = (
            "id",
            "type",
            "attributes",
            "extent",
            "created_at",
            "modified_at",
        )

    extent = FieldMethod(
        fields.List(fields.Float()),
        "get_extent",
        metadata={"minItems": 4, "maxItems": 4},
    )

    @validates("name")
    def validate_name(self, value, **kwargs):
        validate_name(value)

    @pre_dump
    def load_properties(self, data, **kwargs):
        """Load the computed properties of the object (stats, attributes...)"""
        data.load_properties()
        return data

    def get_extent(self, obj):
        """Returns the extent of the data (xmin,ymin,xmax,ymax)"""
        extent = to_shape(obj.extent)
        return extent.bounds


class GlobalDataSchema(BaseDataSchema):
    class Meta(BaseDataSchema.Meta):
        model = GlobalData
        fields = (
            "id",
            "type",
            "name",
            "description",
            "attributes",
            "properties",
            "upload_user",
            "is_public",
            "created_at",
            "modified_at",
        )
        # This schema is never use for creation, so all below fields are the
        # non-updatable
        # is_public can only be changed through permission schema
        dump_only = (
            "id",
            "type",
            "attributes",
            "upload_user",
            "is_public",
            "created_at",
            "modified_at",
        )

    type = fields.Enum(constant_enum(DataType.global_data))
    upload_user = fields.Nested(UserSchema, only=("id", "username"))
    properties = FieldMethod(
        fields.Dict(),
        "get_properties",
        deserialize="set_properties",
        metadata={"description": "properties organized by property name"},
        required=False,
    )

    def get_properties(self, obj):
        return {p.attribute.name: p.value for p in obj.properties}

    def set_properties(self, properties):
        # If persisting changes
        #    need to delete old properties beforehand
        #    and add attribute with properties afterwards
        attributes = {attr.name: attr for attr in self.instance.attributes}
        new_properties = []
        for key, value in properties.items():
            old_attr = attributes.get(key, None)
            attr_type = self.instance._choose_attribute_type(value)
            attr = (
                old_attr
                if isinstance(old_attr, attr_type)
                else attr_type(name=key, data=self.instance)
            )
            new_properties.append(
                attr.get_value_class()(
                    value=value, attribute=attr, feature=self.instance.feature
                )
            )
        return new_properties


class GlobalDataCreationSchema(Schema):
    name = fields.String(required=True)
    description = fields.String(required=False)
    properties = fields.Dict(
        metadata={"description": "properties organized by property name"},
        required=True,
    )

    @validates("name")
    def validate_name(self, value, **kwargs):
        validate_name(value)


class GeoDataSchema(BaseGeoDataSchema):
    class Meta(BaseGeoDataSchema.Meta):
        model = GeoData
        update_fields = ("name", "description")
        fields = (
            "id",
            "type",
            "name",
            "original_name",
            "source_driver",
            "upload_user",
            "description",
            "attributes",
            "extent",
            "created_at",
            "modified_at",
            "stream",
            "is_public",
        )
        # This schema is never use for creation, so all below fields are the
        # non-updatable
        # is_public can only be changed through permission schema
        dump_only = (
            "id",
            "type",
            "original_name",
            "source_driver",
            "upload_user",
            "attributes",
            "extent",
            "created_at",
            "modified_at",
            "is_public",
        )

    type = fields.Enum(constant_enum(DataType.geo_data))
    upload_user = fields.Nested(UserSchema, only=("id", "username"))
    stream = FieldPluck(StreamGeoData, "id", allow_none=True)


class DataSchema(OneOfSchema, BaseDataSchema):
    type_field = "type"
    type_schemas = {
        DataType.global_data.name: GlobalDataSchema,
        DataType.geo_data.name: GeoDataSchema,
        DataType.generator.name: BaseGeoDataSchema,
    }

    def get_obj_type(self, obj):
        return obj.type.name


class GeoDataPermissionSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = GeoData.Permission
        # All fields can be used for creation, update and retrieval
        fields = ("user",)

    user = fields.Pluck(UserSchema, "id")


class GeoDataAccessSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = GeoData
        # All fields can be used for creation, update and retrieval
        fields = ("is_public", "permissions")

    permissions = fields.Nested(GeoDataPermissionSchema, many=True)


class GlobalDataPermissionSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = GlobalData.Permission
        # All fields can be used for creation, update and retrieval
        fields = ("user",)

    user = fields.Pluck(UserSchema, "id")


class GlobalDataAccessSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = GlobalData
        # All fields can be used for creation, update and retrieval
        fields = ("is_public", "permissions")

    permissions = fields.Nested(GlobalDataPermissionSchema, many=True)


class GeneratedGeoDataSchema(BaseGeoDataSchema):
    class Meta(BaseGeoDataSchema.Meta):
        model = GeneratedGeoData


class BaseDisaggregationCriterionSchema(Schema):
    data = fields.String()
    attribute = fields.String()
    value = fields.Raw()


class DisaggregationCriterionWeightedSumSchema(
    BaseDisaggregationCriterionSchema
):
    weight = fields.Float()


class BaseExplainabilitySchema(Schema):
    pref_type = fields.Enum(ExplainabilityType)


class BaseModelExplainabilitySchema(Schema):
    disaggregation = fields.Nested(
        BaseDisaggregationCriterionSchema, many=True
    )


class ExplainabilityContinuousRuleSchema(BaseModelExplainabilitySchema):
    pref_type = fields.Enum(constant_enum(ExplainabilityType.continuous_rule))


class ExplainabilityBufferSchema(BaseModelExplainabilitySchema):
    pref_type = fields.Enum(constant_enum(ExplainabilityType.geo_buffer))


class ExplainabilityWeightedSumSchema(BaseModelExplainabilitySchema):
    pref_type = fields.Enum(constant_enum(ExplainabilityType.weighted_sum))
    disaggregation = fields.Nested(
        DisaggregationCriterionWeightedSumSchema, many=True
    )


class ExplainabilityDiscreteModelSchema(BaseModelExplainabilitySchema):
    pref_type = fields.Enum(constant_enum(ExplainabilityType.categories_rule))
    rule = fields.String()


class ProfileCriterionSchema(Schema):
    attribute = fields.String(allow_none=False)
    value = fields.Raw(allow_none=False)
    weight = fields.Float(allow_none=False)


class ExplainabilityMRSortSchema(BaseModelExplainabilitySchema):
    pref_type = fields.Enum(constant_enum(ExplainabilityType.mrsort))
    lower = fields.Nested(ProfileCriterionSchema, many=True, allow_none=True)
    upper = fields.Nested(ProfileCriterionSchema, many=True, allow_none=True)


class ExplainabilityAttributeWithValues(Schema):
    attribute = fields.String()
    values = fields.List(fields.Raw(allow_none=False))


class ExplainabilityOverlap(BaseExplainabilitySchema):
    aggregated = fields.Nested(ExplainabilityAttributeWithValues, many=True)


class ExplainabilityMergedFeaturesSchema(BaseExplainabilitySchema):
    pref_type = fields.Enum(constant_enum(ExplainabilityType.merged_features))


class ExplainabilityOverlapMinSchema(ExplainabilityOverlap):
    pref_type = fields.Enum(constant_enum(ExplainabilityType.min))


class ExplainabilityOverlapMaxSchema(ExplainabilityOverlap):
    pref_type = fields.Enum(constant_enum(ExplainabilityType.max))


class ExplainabilityOverlapSumSchema(ExplainabilityOverlap):
    pref_type = fields.Enum(constant_enum(ExplainabilityType.sum))


class ExplainabilityOverlapAverageSchema(ExplainabilityOverlap):
    pref_type = fields.Enum(constant_enum(ExplainabilityType.average))


class ExplainabilitySchema(OneOfSchema, BaseExplainabilitySchema):
    type_field = "pref_type"
    type_schemas = {
        ExplainabilityType.categories_rule.name: ExplainabilityDiscreteModelSchema,  # noqa: E501
        ExplainabilityType.continuous_rule.name: ExplainabilityContinuousRuleSchema,  # noqa: E501
        ExplainabilityType.geo_buffer.name: ExplainabilityBufferSchema,
        ExplainabilityType.mrsort.name: ExplainabilityMRSortSchema,
        ExplainabilityType.weighted_sum.name: ExplainabilityWeightedSumSchema,
        ExplainabilityType.merged_features.name: ExplainabilityMergedFeaturesSchema,  # noqa: E501
        ExplainabilityType.min.name: ExplainabilityOverlapMinSchema,
        ExplainabilityType.max.name: ExplainabilityOverlapMaxSchema,
        ExplainabilityType.sum.name: ExplainabilityOverlapSumSchema,
        ExplainabilityType.average.name: ExplainabilityOverlapAverageSchema,
    }

    def get_obj_type(self, obj):
        return obj.pref_type.name


class FeatureSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = Feature
        fields = ("id", "type", "properties", "explainability")
        # This schema is never use for creation nor update
        # so basically all fields are not updatable nor creatable (no write)
        dump_only = ("id", "type", "properties", "explainability")

    type = fields.Enum(FeatureType)
    properties = FieldMethod(
        fields.Dict(),
        "get_properties",
        metadata={
            "description": "feature properties organized by property name"
        },
    )
    explainability = FieldMethod(
        fields.Nested(ExplainabilitySchema), serialize="get_explainability"
    )

    def get_properties(self, obj):
        return {p.attribute.name: p.value for p in obj.properties}

    def get_explainability(self, obj):
        return obj.explainability
