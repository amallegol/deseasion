#!/bin/sh

if [ -z "${API_TOKEN}" ]; then
    API_TOKEN=$(head -c 1024 /dev/urandom | tr -dc '[:alnum:]' | head -c 32)
fi
if [ -z "${PROFILE_DIR}" ]; then
    PROFILE_DIR="$(pwd)/profiler"
fi
if [ -z "${CELERY_RESULT_BACKEND}" ]; then
    CELERY_RESULT_BACKEND=$CELERY_BROKER_URL
fi

cp instance-example/* instance/.
cd instance

DB_KEY="SQLALCHEMY_DATABASE_URI"
sed -i -e "s#${DB_KEY} *= *'[^']*'#${DB_KEY} = '${DB_URI}'#" config.py

TOKEN_KEY="API_TOKEN_SECRET_KEY"
sed -i -e "s/${TOKEN_KEY} *= *'[^']*'/${TOKEN_KEY} = '${API_TOKEN}'/" config.py

PROFILE_DIR_KEY="PROFILE_DIR"
sed -i -e "s#${PROFILE_DIR_KEY} *= *'[^']*'#${PROFILE_DIR_KEY} = '${PROFILE_DIR}'#" config.py

if ! [ -z "${CELERY_BROKER_URL}" ]; then
    CELERY_BROKER_KEY="CELERY_BROKER_URL"
    sed -i -e "s#${CELERY_BROKER_KEY} *= *'[^']*'#${CELERY_BROKER_KEY} = '${CELERY_BROKER_URL}'#" config.py
fi

if ! [ -z "${CELERY_RESULT_BACKEND}" ]; then
    CELERY_RESULT_KEY="CELERY_RESULT_BACKEND"
    sed -i -e "s#${CELERY_RESULT_KEY} *= *'[^']*'#${CELERY_RESULT_KEY} = '${CELERY_RESULT_BACKEND}'#" config.py
fi

unset DB_KEY TOKEN_KEY PROFILE_DIR_KEY CELERY_BROKER_KEY CELERY_RESULT_KEY
cd ..