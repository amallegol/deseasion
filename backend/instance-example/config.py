# Create a file `config.py` in this directory to store specific configuration
# (eg: debug options, database access...)

DEBUG = False

SQLALCHEMY_DATABASE_URI = 'postgresql://aileron:pass@database/aileron'
SQLALCHEMY_ECHO = False

API_TOKEN_SECRET_KEY = 'mysecretkey'  # salt for the JWT encryption
API_TOKEN_VALIDITY = 300  # number of seconds
API_REFRESH_VALIDITY = 604800  # validity in seconds (7 days)
API_BASIC_AUTHENTICATION_HEADER = 'Authorization'
API_JWT_AUTHENTICATION_HEADER = 'Authorization'

CELERY_BROKER_URL = 'redis://redis:6379/0'
CELERY_RESULT_BACKEND = 'redis://redis:6379/0'

PROFILE = False  # activates the profiling of the execution
PROFILE_DIR = '/home/<user>/aileron/profiler'  # directory to save the pstats dump files

PYPY_PATH = '/home/<user>/pypy'  # path of pypy for the sandbox
SANDBOX_ROOT = '/home/<user>/aileron/sandbox'  # directory containing the sandbox scripts

FEATURES_CACHE_LIMIT = 200000  # the limit of the features cache table

# path for the ogr2ogr executable
# the application use a slower python fallback if this option is not set
OGR2OGR_BIN = '/usr/bin/ogr2ogr'

# directory for the temporary files
# use the default system directories if this option is not set
#TEMP_DIR = '/var/tmp'