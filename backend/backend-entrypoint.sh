#!/bin/sh

if ! [ -f ./instance/config.py ]; then
    ./configure-backend.sh
fi

# Wait for postgresql database to be online
until psql "${DB_URI}" -c "select 1" > /dev/null 2>&1; do
    echo "Waiting for postgres server..."
    sleep 1
done

# Initialize database if need be
flask db create

# Upgrade database if need be
flask db upgrade

# wait for docker to initialize
while (! docker info >/dev/null 2>&1 ); do
    sleep 1
done

if [ -z "$(docker image ls | grep registry.gitlab.com/decide.imt-atlantique/deseasion/sandbox)" ]; then
    docker pull registry.gitlab.com/decide.imt-atlantique/deseasion/sandbox:latest
fi

exec "$@"