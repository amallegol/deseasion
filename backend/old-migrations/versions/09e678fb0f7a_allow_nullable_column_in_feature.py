"""Allow nullable column in feature

Revision ID: 09e678fb0f7a
Revises: e1eb0ff4e51a
Create Date: 2020-09-14 11:04:15.433211

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '09e678fb0f7a'
down_revision = 'e1eb0ff4e51a'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('feature', 'geo_data_id',
               existing_type=sa.INTEGER(),
               nullable=True)


def downgrade():
    op.alter_column('feature', 'geo_data_id',
               existing_type=sa.INTEGER(),
               nullable=False)
