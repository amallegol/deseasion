"""Use inheritance for projects and templates

Revision ID: be12515ced1f
Revises: 41396ffb32ef
Create Date: 2020-01-13 16:57:35.899371

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'be12515ced1f'
down_revision = '41396ffb32ef'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('project', sa.Column('is_template', sa.Boolean(), nullable=True))
    op.execute('UPDATE project SET is_template = false')
    op.alter_column('project', 'is_template', nullable=False)


def downgrade():
    op.execute('DELETE FROM project WHERE is_template = true')
    op.drop_column('project', 'is_template')
