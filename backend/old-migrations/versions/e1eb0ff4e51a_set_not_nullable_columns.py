"""Set not nullable columns

Revision ID: e1eb0ff4e51a
Revises: fda8f0da8728
Create Date: 2020-08-03 15:39:04.208200

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e1eb0ff4e51a'
down_revision = 'fda8f0da8728'
branch_labels = None
depends_on = None


def upgrade():
    op.execute('DELETE FROM feature WHERE geo_data_id IS NULL')
    op.alter_column('feature', 'geo_data_id',
               existing_type=sa.INTEGER(),
               nullable=False)


def downgrade():
    op.alter_column('feature', 'geo_data_id',
               existing_type=sa.INTEGER(),
               nullable=True)
