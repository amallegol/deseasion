"""Add a name to the preference model

Revision ID: ead40bb1a7d4
Revises: 64ec2a0432dc
Create Date: 2019-11-05 11:59:33.914901

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ead40bb1a7d4'
down_revision = '64ec2a0432dc'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('preference_model', sa.Column('name', sa.String(), nullable=True))


def downgrade():
    op.drop_column('preference_model', 'name')
