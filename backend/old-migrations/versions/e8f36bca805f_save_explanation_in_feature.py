"""Save explanation in Feature

Revision ID: e8f36bca805f
Revises: f87978d85fee
Create Date: 2020-05-27 10:25:40.094999

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'e8f36bca805f'
down_revision = 'f87978d85fee'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('feature', sa.Column('explanation', postgresql.JSONB(astext_type=sa.Text()), nullable=True))


def downgrade():
    op.drop_column('feature', 'explanation')
