"""Use a composite primary key for mrsort_criterion

Revision ID: ba899c3814f3
Revises: 631639c52a31
Create Date: 2018-09-13 16:52:40.755988

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ba899c3814f3'
down_revision = '631639c52a31'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('mrsort_inference_value', sa.Column('attribute_id', sa.Integer()))
    op.add_column('mrsort_inference_value', sa.Column('mrsort_id', sa.Integer()))
    op.execute('UPDATE mrsort_inference_value v '
               'SET attribute_id = c.attribute_id, mrsort_id = c.mrsort_id '
               'FROM mrsort_criterion c '
               'WHERE v.criterion_id = c.id')
    op.alter_column('mrsort_inference_value', 'attribute_id', nullable=False)
    op.alter_column('mrsort_inference_value', 'mrsort_id', nullable=False)
    op.drop_constraint('mrsort_inference_value_pkey', 'mrsort_inference_value', type_='primary')
    op.create_primary_key('pk_mrsort_inference_value', 'mrsort_inference_value', ['mrsort_id', 'attribute_id', 'alternative_id'])
    op.drop_constraint('mrsort_inference_value_criterion_id_fkey', 'mrsort_inference_value', type_='foreignkey')
    op.drop_column('mrsort_inference_value', 'criterion_id')

    op.drop_constraint('mrsort_criterion_pkey', 'mrsort_criterion', type_='primary')
    op.create_primary_key('pk_mrsort_criterion', 'mrsort_criterion', ['mrsort_id', 'attribute_id'])
    op.drop_column('mrsort_criterion', 'id')
    op.create_foreign_key(op.f('fk_mrsort_inference_value_mrsort_id_mrsort_criterion'), 'mrsort_inference_value', 'mrsort_criterion', ['mrsort_id', 'attribute_id'], ['mrsort_id', 'attribute_id'], ondelete='CASCADE')




def downgrade():
    op.add_column('mrsort_criterion', sa.Column('id', sa.INTEGER()))
    op.add_column('mrsort_inference_value', sa.Column('criterion_id', sa.INTEGER()))
    op.drop_constraint('fk_mrsort_inference_value_mrsort_id_mrsort_criterion', 'mrsort_inference_value', type_='foreignkey')
    op.drop_constraint('pk_mrsort_inference_value', 'mrsort_inference_value', type_='primary')
    op.drop_constraint('pk_mrsort_criterion', 'mrsort_criterion', type_='primary')
    op.execute('WITH cte AS ( '
                   'SELECT ROW_NUMBER() OVER() AS rn, mrsort_id, attribute_id '
                   'FROM mrsort_criterion) '
               'UPDATE mrsort_criterion c SET id = cte.rn '
               'FROM cte '
               'WHERE cte.mrsort_id = c.mrsort_id AND cte.attribute_id = c.attribute_id')
    op.create_primary_key('mrsort_criterion_pkey', 'mrsort_criterion', ['id'])
    op.execute('UPDATE mrsort_inference_value v '
               'SET criterion_id = c.id '
               'FROM mrsort_criterion c '
               'WHERE v.attribute_id = c.attribute_id AND v.mrsort_id = c.mrsort_id')
    op.create_primary_key('mrsort_inference_value_pkey', 'mrsort_inference_value', ['criterion_id', 'alternative_id'])
    op.create_foreign_key('mrsort_inference_value_criterion_id_fkey', 'mrsort_inference_value', 'mrsort_criterion', ['criterion_id'], ['id'], ondelete='CASCADE')
    op.drop_column('mrsort_inference_value', 'mrsort_id')
    op.drop_column('mrsort_inference_value', 'attribute_id')
