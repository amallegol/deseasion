"""Add a model for sharing project data

Revision ID: 41396ffb32ef
Revises: ead40bb1a7d4
Create Date: 2019-11-18 15:43:24.515614

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '41396ffb32ef'
down_revision = 'ead40bb1a7d4'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('data_share',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('data_id', sa.Integer(), nullable=False),
        sa.Column('uid', postgresql.UUID(), nullable=False),
        sa.Column('expiration', sa.DateTime(), nullable=True),
        sa.Column('expired', sa.Boolean(), nullable=True),
        sa.ForeignKeyConstraint(['data_id'], ['project_data.id'], name=op.f('fk_data_share_data_id_project_data')),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_data_share')),
        sa.UniqueConstraint('uid', name=op.f('uq_data_share_uid'))
    )


def downgrade():
    op.drop_table('data_share')
