"""Add table for default attribute values

Revision ID: f87978d85fee
Revises: 80d6b19113b9
Create Date: 2020-03-20 10:23:21.600728

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'f87978d85fee'
down_revision = '80d6b19113b9'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('default_value',
        sa.Column('attribute_id', sa.Integer(), nullable=False),
        sa.Column('model_id', sa.Integer(), nullable=False),
        sa.Column('value', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
        sa.ForeignKeyConstraint(['attribute_id'], ['data_attribute.id'], name=op.f('fk_default_value_attribute_id_data_attribute'), ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['model_id'], ['preference_model.id'], name=op.f('fk_default_value_model_id_preference_model'), ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('attribute_id', 'model_id', name=op.f('pk_default_value'))
    )
    op.drop_column('preference_model', 'default_values')


def downgrade():
    op.add_column('preference_model', sa.Column('default_values', postgresql.JSONB(astext_type=sa.Text()), autoincrement=False, nullable=True))
    op.drop_table('default_value')
