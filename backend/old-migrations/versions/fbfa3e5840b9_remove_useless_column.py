"""Remove useless column

Revision ID: fbfa3e5840b9
Revises: ad737130a5f9
Create Date: 2020-03-03 15:48:49.005392

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'fbfa3e5840b9'
down_revision = 'ad737130a5f9'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_column('project_data', 'in_hierarchy')


def downgrade():
    op.add_column('project_data', sa.Column('in_hierarchy', sa.BOOLEAN(), autoincrement=False, nullable=True))
    op.execute("UPDATE project_data AS d SET in_hierarchy = TRUE FROM project_shared_data AS s WHERE d.id = s.project_data_id")
    op.execute("UPDATE project_data SET in_hierarchy = FALSE WHERE in_hierarchy IS NULL")
    op.alter_column('project_data', 'in_hierarchy', nullable=False)
