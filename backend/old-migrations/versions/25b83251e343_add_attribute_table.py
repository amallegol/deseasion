"""Use attribute and value tables for the data properties

Revision ID: 25b83251e343
Revises: a2a73d813e8c
Create Date: 2018-04-13 16:29:50.443475

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '25b83251e343'
down_revision = 'a2a73d813e8c'
branch_labels = None
depends_on = None


t_feature = sa.Table(
        'feature',
        sa.MetaData(),
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('geo_data_id', sa.Integer, sa.ForeignKey('geo_data.id')),
        sa.Column('properties', postgresql.JSONB)
        )

t_attribute = sa.Table(
        'data_attribute',
        sa.MetaData(),
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('type', sa.Enum('number', 'string', 'ordinal', name='attributetype'), nullable=False),
        sa.Column('name', sa.String, nullable=False),
        sa.Column('geo_data_id', sa.Integer, sa.ForeignKey('geo_data.id'))
        )

t_value = sa.Table(
        'data_value',
        sa.MetaData(),
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('attribute_id', sa.Integer, sa.ForeignKey('data_attribute.id'), index=True),
        sa.Column('feature_id', sa.Integer, sa.ForeignKey('feature.id'), index=True),
        sa.Column('value', postgresql.JSONB)
        )


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('data_attribute',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('type', sa.Enum('number', 'string', 'ordinal', name='attributetype'), nullable=True),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('statistics', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
        sa.Column('geo_data_id', sa.Integer(), nullable=True),
        sa.Column('order', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
        sa.ForeignKeyConstraint(['geo_data_id'], ['base_geo_data.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_data_attribute_geo_data_id'), 'data_attribute', ['geo_data_id'], unique=False)
    op.create_table('data_value',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('attribute_id', sa.Integer(), nullable=True),
        sa.Column('feature_id', sa.Integer(), nullable=True),
        sa.Column('value', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
        sa.ForeignKeyConstraint(['attribute_id'], ['data_attribute.id'], ),
        sa.ForeignKeyConstraint(['feature_id'], ['feature.id'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('attribute_id', 'feature_id')
    )
    op.create_index(op.f('ix_data_value_attribute_id'), 'data_value', ['attribute_id'], unique=False)
    op.create_index(op.f('ix_data_value_feature_id'), 'data_value', ['feature_id'], unique=False)

    # migrate data
    connection = op.get_bind()
    q = sa.select([
        t_feature.c.geo_data_id,
        sa.func.jsonb_object_keys(t_feature.c.properties).label('key'),
        sa.func.array_agg(sa.func.distinct(sa.func.jsonb_typeof(t_feature.c.properties[sa.func.jsonb_object_keys(t_feature.c.properties)])))
    ]).group_by(t_feature.c.geo_data_id, 'key')
    attributes = connection.execute(q)
    for geo_data_id, key, types in attributes:
        print(geo_data_id, key, types)
        dtype = 'string'
        if 'number' in types:
            dtype = 'number'
        row = connection.execute(t_attribute.insert().values(type=dtype, name=key, geo_data_id=geo_data_id).returning(t_attribute.c.id)).fetchone()
        attr_id = row[0]
        features_val = connection.execute(sa.select([
            t_feature.c.id,
            t_feature.c.properties[key]
        ]).where(t_feature.c.geo_data_id == geo_data_id))
        op.bulk_insert(t_value, [dict(attribute_id=attr_id, feature_id=f[0], value=f[1]) for f in features_val])

    op.drop_column('base_geo_data', 'attributes')
    op.drop_column('feature', 'properties')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('feature', sa.Column('properties', postgresql.JSONB(astext_type=sa.Text()), autoincrement=False, nullable=True))
    op.add_column('base_geo_data', sa.Column('attributes', postgresql.JSONB(astext_type=sa.Text()), autoincrement=False, nullable=True))

    # migrate data
    connection = op.get_bind()
    j = sa.join(t_attribute, t_value, t_attribute.c.id == t_value.c.attribute_id)
    q = sa.select([
        t_value.c.feature_id,
        sa.func.jsonb_object_agg(t_attribute.c.name, t_value.c.value)
    ]).select_from(j).group_by(t_value.c.feature_id)
    features_props = connection.execute(q)
    u = t_feature.update().where(t_feature.c.id == sa.bindparam('feature_id')).values(properties=sa.bindparam('properties'))
    connection.execute(u, [{'feature_id': f[0], 'properties': f[1]} for f in features_props])

    op.drop_index(op.f('ix_data_value_feature_id'), table_name='data_value')
    op.drop_index(op.f('ix_data_value_attribute_id'), table_name='data_value')
    op.drop_table('data_value')
    op.drop_index(op.f('ix_data_attribute_geo_data_id'), table_name='data_attribute')
    op.drop_table('data_attribute')
    # ### end Alembic commands ###
