"""Use another table for weighted sum operands

Revision ID: 80d6b19113b9
Revises: ee3fffe2caeb
Create Date: 2020-03-11 14:41:39.440319

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '80d6b19113b9'
down_revision = 'ee3fffe2caeb'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('weighted_sum_operand',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('attribute_id', sa.Integer(), nullable=True),
        sa.Column('weight', sa.Float(), nullable=True),
        sa.Column('model_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['attribute_id'], ['data_attribute.id'], name=op.f('fk_weighted_sum_operand_attribute_id_data_attribute'), ondelete='SET NULL'),
        sa.ForeignKeyConstraint(['model_id'], ['preference_model.id'], name=op.f('fk_weighted_sum_operand_model_id_preference_model'), ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_weighted_sum_operand'))
    )
    op.execute("INSERT INTO weighted_sum_operand (model_id, attribute_id, weight) "
               "    SELECT id, attribute_1_id, weight_1 FROM preference_model WHERE pref_type = 'weighted_sum'::preftype")
    op.execute("INSERT INTO weighted_sum_operand (model_id, attribute_id, weight) "
               "    SELECT id, attribute_2_id, weight_2 FROM preference_model WHERE pref_type = 'weighted_sum'::preftype")
    op.drop_constraint('fk_preference_model_attribute_2_id_data_attribute', 'preference_model', type_='foreignkey')
    op.drop_constraint('fk_preference_model_attribute_1_id_data_attribute', 'preference_model', type_='foreignkey')
    op.drop_column('preference_model', 'attribute_1_id')
    op.drop_column('preference_model', 'weight_1')
    op.drop_column('preference_model', 'attribute_2_id')
    op.drop_column('preference_model', 'weight_2')


def downgrade():
    op.add_column('preference_model', sa.Column('weight_2', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    op.add_column('preference_model', sa.Column('attribute_2_id', sa.INTEGER(), autoincrement=False, nullable=True))
    op.add_column('preference_model', sa.Column('weight_1', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    op.add_column('preference_model', sa.Column('attribute_1_id', sa.INTEGER(), autoincrement=False, nullable=True))
    op.create_foreign_key('fk_preference_model_attribute_1_id_data_attribute', 'preference_model', 'data_attribute', ['attribute_1_id'], ['id'])
    op.create_foreign_key('fk_preference_model_attribute_2_id_data_attribute', 'preference_model', 'data_attribute', ['attribute_2_id'], ['id'])
    # Will only keep the first two operands
    op.execute("UPDATE preference_model SET attribute_1_id = q.attribute_id, weight_1 = q.weight FROM "
               "    (SELECT DISTINCT ON (model_id) * FROM weighted_sum_operand) AS q "
               "    WHERE preference_model.id = q.model_id")
    op.execute("DELETE FROM weighted_sum_operand WHERE id IN "
               "    (SELECT DISTINCT ON (model_id) id FROM weighted_sum_operand)")
    op.execute("UPDATE preference_model SET attribute_2_id = q.attribute_id, weight_2 = q.weight FROM "
               "    (SELECT DISTINCT ON (model_id) * FROM weighted_sum_operand) AS q "
               "    WHERE preference_model.id = q.model_id")
    op.drop_table('weighted_sum_operand')
