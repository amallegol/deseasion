"""Remove criterion, decision map and intermediate data

Revision ID: ad737130a5f9
Revises: be12515ced1f
Create Date: 2020-02-28 14:55:50.540382

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ad737130a5f9'
down_revision = 'be12515ced1f'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('project_data', sa.Column('in_hierarchy', sa.Boolean(), nullable=True))
    op.execute("UPDATE project_data SET in_hierarchy = CASE "
               "    WHEN data_type IN ('criterion', 'decision_map') THEN TRUE "
               "    ELSE FALSE "
               "END")
    op.alter_column('project_data', 'in_hierarchy', nullable=False)
    op.execute("ALTER TYPE datatype RENAME TO datatype_old")
    op.execute("CREATE TYPE datatype AS ENUM ('geo_data', 'generator')")
    op.execute("ALTER TABLE project_data ALTER COLUMN data_type TYPE datatype USING CASE data_type "
               "    WHEN 'geo_data'::datatype_old THEN 'geo_data'::datatype "
               "    WHEN 'criterion'::datatype_old THEN 'generator'::datatype "
               "    WHEN 'intermediate_data'::datatype_old THEN 'generator'::datatype "
               "    WHEN 'decision_map'::datatype_old THEN 'generator'::datatype "
               "END")
    op.execute("DROP TYPE datatype_old")


def downgrade():
    op.execute("ALTER TYPE datatype RENAME TO datatype_old")
    op.execute("CREATE TYPE datatype AS ENUM ('geo_data', 'criterion', 'intermediate_data', 'decision_map')")
    op.execute("ALTER TABLE project_data ALTER COLUMN data_type TYPE datatype USING CASE "
               "    WHEN data_type = 'geo_data'::datatype_old THEN 'geo_data'::datatype "
               "    WHEN data_type = 'generator'::datatype_old AND in_hierarchy = 't' THEN 'criterion'::datatype "
               "    WHEN data_type = 'generator'::datatype_old AND in_hierarchy = 'f' THEN 'intermediate_data'::datatype "
               "END")
    op.execute("UPDATE project_data SET data_type = 'decision_map'::datatype WHERE id IN (SELECT project_root_data_id FROM project_root)")
    op.execute("DROP TYPE datatype_old")
    op.drop_column('project_data', 'in_hierarchy')
