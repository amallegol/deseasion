"""Add ondelete CASCADE for data values

Revision ID: 3d7c02032312
Revises: ba899c3814f3
Create Date: 2018-10-29 16:28:32.325861

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3d7c02032312'
down_revision = 'ba899c3814f3'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('data_value_feature_id_fkey', 'data_value', type_='foreignkey')
    op.create_foreign_key(op.f('fk_data_value_feature_id_feature'), 'data_value', 'feature', ['feature_id'], ['id'], ondelete='CASCADE')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(op.f('fk_data_value_feature_id_feature'), 'data_value', type_='foreignkey')
    op.create_foreign_key('data_value_feature_id_fkey', 'data_value', 'feature', ['feature_id'], ['id'])
    # ### end Alembic commands ###
