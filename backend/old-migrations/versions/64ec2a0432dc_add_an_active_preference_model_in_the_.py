"""Add an active preference model in the project data

Revision ID: 64ec2a0432dc
Revises: 3e5f4067f39a
Create Date: 2019-09-11 15:19:39.191266

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '64ec2a0432dc'
down_revision = '3e5f4067f39a'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('project_data', sa.Column('active_model_id', sa.Integer(), nullable=True))
    op.create_foreign_key(op.f('fk_project_data_active_model_id_preference_model'), 'project_data', 'preference_model', ['active_model_id'], ['id'])

    # Set the existing preference models as the active models
    op.execute("UPDATE project_data d SET active_model_id = m.id FROM preference_model m WHERE d.id = m.data_generator_id")



def downgrade():
    # Delete the preference models that are not active
    op.execute("DELETE FROM preference_model WHERE id NOT IN (SELECT active_model_id FROM project_data)")

    op.drop_constraint(op.f('fk_project_data_active_model_id_preference_model'), 'project_data', type_='foreignkey')
    op.drop_column('project_data', 'active_model_id')
