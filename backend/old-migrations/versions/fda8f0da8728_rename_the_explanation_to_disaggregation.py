"""Rename the explanation to disaggregation

Revision ID: fda8f0da8728
Revises: e8f36bca805f
Create Date: 2020-06-05 11:21:35.034257

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'fda8f0da8728'
down_revision = 'e8f36bca805f'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("ALTER TABLE feature RENAME explanation TO disaggregation")


def downgrade():
    op.execute("ALTER TABLE feature RENAME disaggregation TO explanation")
