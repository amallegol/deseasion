import json
import sys


def read_stdin():
    if sys.version_info >= (3, 0):
        return sys.stdin.buffer.read()
    else:
        return sys.stdin.read()


def write_stdout(message):
    if sys.version_info >= (3, 0):
        sys.stdout.buffer.write(message)
    else:
        sys.stdout.write(message)


def write_stderr(message):
    if sys.version_info >= (3, 0):
        sys.stderr.buffer.write(message)
    else:
        sys.stderr.write(message)


class GeoAttribute(object):
    """Used to access the attributes in the Criterion rules

    Allow the users to access the data as object attributes instead of using a
    dictionary. The user can type ``data.property`` instead of accessing the
    property through a dictionary like ``data['property']``.
    """

    def __init__(self, attribute_name, attributes):
        attr = attributes if attributes is not None else {}
        object.__setattr__(self, "_attributes", attr)
        object.__setattr__(self, "_attribute_name", attribute_name)

    def __getattr__(self, name):
        """Returns the value of the attribute

        Args:
            name (str): name of the attribute

        Returns:
            the value of the attribute if it exists in the list, else None
        """
        data = object.__getattribute__(self, "_attributes")
        if name not in data:
            raise AttributeError(
                'The attribute "{}" does not exist in "{}"'
                "".format(
                    name, object.__getattribute__(self, "_attribute_name")
                )
            )
        return data.get(name, None)

    def __setattr__(self, name, value):
        """Set an attribute to a value

        Args:
            name (str): the name of the attribute to set
            value: the value to set the attribute
        """
        data = object.__getattribute__(self, "_attributes")
        data[name] = value

    def to_dict(self):
        """Returns the list of attributes and their values as a dictionary"""
        return object.__getattribute__(self, "_attributes")


class Environment(object):
    """Environment to execute the Python code.

    The environment read the parameters from stdin, as a JSON object:
      {
        "code": <str>,
        "data": [
          {
            "Data1": {
              "attributes": { "attr1": ..., "attr2": ... },
              "properties": { "p1": ..., "p2": ... }
            },
            "Data2": ...
          },
          ...
        ],
        "statistics": {
            "Data1": {
                "min": 0,
                "max": 100,
                "mean": 50,
                "std": ...,
                "percentile_0": ...,
                "percentile_1": ...,
                ...,
                "percentile_100": ...
            },
            "Data2": ...
        }
      }

    The results will be written to stdout as a JSON object:
      {
        "data": ...
      }

    The errors will also be written to stdout as a JSON object:
      {
        "error": ...
      }

    Attributes:
        code (str): The Python code to execute
        data (list): The list of data used as input for the code.
    """

    def __init__(self, code="", data={}):
        self.code = code
        self.data = data

    def get_data_as_namespace(self, data):
        """Return the data dict as a Python namespace dict."""
        ns = {}
        for input_name, input_data in data.items():
            props = input_data.get("attributes", {})
            for key, prop in input_data.get("properties", {}).items():
                props["_{}".format(key)] = prop
            ns[input_name] = GeoAttribute(input_name, props)
        stats = {}
        for data_name, attr in self.stats.items():
            data_stats = {}
            for attr_name, attr_stats in attr.items():
                data_stats[attr_name] = GeoAttribute(attr_name, attr_stats)
            stats[data_name] = GeoAttribute(data_name, data_stats)
        if len(stats) > 0:
            ns["_stats"] = GeoAttribute("_stats", stats)
        return ns

    def load(self):
        """Load the data from stdin."""
        input_json = read_stdin()
        params = json.loads(input_json.decode())
        self.data = params["data"]
        self.code = params["code"]
        self.stats = params.get("statistics", {})

    def evaluate(self, namespace):
        """Evaluate the code with the namespace.

        Args:
            namespace (dict): The global namespace to use in the evaluation.
        """
        code_obj = compile(self.code, "<string>", "eval")
        return eval(code_obj, namespace)

    def execute(self, namespace):
        """Execute the code in exec.

        Args:
            namespace (dict): The global namespace to use in the execution.

        Returns:
            The local namespace modified by the executed code.
        """
        code_obj = compile(self.code, "<string>", "exec")
        local = {}
        exec(code_obj, namespace, local)
        return local

    def write_data(self, data):
        """Write the data as json to stdout.

        Args:
            data: JSON-serializable object.
        """
        json_data = json.dumps({"data": data}).encode()
        write_stdout(json_data)

    def write_error(self, error):
        """Write the data to stdout as json using the 'error' key.

        Args:
            error: JSON-serializable object.
        """
        json_error = json.dumps({"error": error}).encode()
        write_stdout(json_error)
