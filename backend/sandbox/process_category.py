import sys

from utils import Environment


class CategoryEnvironment(Environment):
    def process(self):
        """Process the code for each of the data"""
        result = []
        for data in self.data:
            ns = self.get_data_as_namespace(data)
            try:
                res = self.evaluate(ns)
            except Exception as error:
                self.write_error(str(error))
                sys.exit(1)
            result.append(res)
        self.write_data(result)


env = CategoryEnvironment()
env.load()
env.process()
