import sys

from utils import Environment


class RuleEnvironment(Environment):
    def execute_filtered(self, namespace):
        """Execute the code and filter the returned variables to ignore those
        beginning with '_'."""
        # TODO: Find a way to specify which variables to save.
        # Currently all the local namespace is returned, which creates problems
        # when importing modules.
        try:
            local = self.execute(namespace)
        except Exception as error:
            self.write_error(str(error))
            sys.exit(1)
        filtered_local = {k: v for k, v in local.items() if k[0] != "_"}
        return filtered_local

    def process(self):
        """Process the code for each of the data"""
        result = []
        for data in self.data:
            ns = self.get_data_as_namespace(data)
            res = self.execute_filtered(ns)
            result.append(res)
        self.write_data(result)


env = RuleEnvironment()
env.load()
env.process()
