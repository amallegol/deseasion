from flask import Flask
from flasgger import Swagger, APISpec
from flask_restful import Api, Resource
from marshmallow import Schema, fields
from marshmallow_oneofschema import OneOfSchema
# from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin


class BaseUser(Schema):
    username = fields.String(metadata={"description": "The name of the user"}, required=True)
    is_admin = fields.Boolean()


class Admin(BaseUser):
    id = fields.Int(required=True)


class User(BaseUser, OneOfSchema):
    """One of the following schemas"""
    type_field = "is_admin"
    type_schemas = {
        True: Admin,
        False: BaseUser
    }

    def get_obj_type(self, obj):
        return obj.is_admin


class _User:
    def __init__(self, username: str, is_admin: bool, id: int = None):
        self.username = username
        self.is_admin = is_admin
        self.id = id


users = [
   _User("admin", True, 1),
    _User("guest", False)
]


app = Flask(__name__)
api = Api(app)
plugins = [MarshmallowPlugin(), FlaskPlugin()]
spec = APISpec("My api docs", '1.0', "3.0", plugins=plugins)
spec.components.schema("BaseUser", schema=BaseUser)
spec.components.schema("Admin", schema=Admin)

# print(spec.to_yaml())

template = spec.to_flasgger(app)

print(template)
