from flask import Flask
from flasgger import Swagger
from flask_restful import Api, Resource
from marshmallow import Schema, fields
from marshmallow_oneofschema import OneOfSchema


class BaseUser(Schema):
    username = fields.String(description="The name of the user")
    is_admin = fields.Boolean()


class Admin(BaseUser):
    id = fields.Int()


class User(BaseUser, OneOfSchema):
    """One of the following schemas"""
    type_field = "is_admin"
    type_schemas = {
        True: Admin,
        False: BaseUser
    }

    def get_obj_type(self, obj):
        return obj.is_admin


class _User:
    def __init__(self, username: str, is_admin: bool, id: int = None):
        self.username = username
        self.is_admin = is_admin
        self.id = id


users = [
   _User("admin", True, 1),
    _User("guest", False)
]


app = Flask(__name__)
api = Api(app)

app.config['SWAGGER'] = {
    'title': 'My api docs',
    'openapi': '3.0.2'
}
Swagger(app, template_file="app3_spec/full.yml")
# This full.yml spec file is a bundle made from all app3_spec folder yml files

class Username(Resource):
    def get(self, username):
        for user in users:
            if user.username == username:
                return User().dump(user), 200
        return "Unknown user", 404


api.add_resource(Username, '/username/<username>')

if __name__ == "__main__":
    app.run(debug=True)