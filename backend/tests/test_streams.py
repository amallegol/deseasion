import requests
from requests.auth import HTTPBasicAuth, AuthBase

URL = "http://localhost"
USER = "admin"
PASSWORD = "admin"


class BearerAuth(AuthBase):
    def __init__(self, token: str) -> None:
        self.token = token

    def __call__(self, req):
        req.headers["Authorization"] = f"Bearer {self.token}"
        return req


if __name__ == "__main__":
    basic = HTTPBasicAuth(USER, PASSWORD)
    tokens = requests.get(f"{URL}/api/login", auth=basic).json()


    bearer_auth = BearerAuth(tokens["access_token"])
    print(requests.get(f"{URL}/api/user", auth=bearer_auth).json())

    _WFS_STREAMS = [
        {'url': 'https://services.data.shom.fr/INSPIRE/wfs', 'feature_type': 'ARCHIVES_CARTES_MARINES_GRILLES:grilles_archipel_cm_30'},
        {'url': 'https://ows.emodnet-bathymetry.eu/wfs', 'feature_type': 'emodnet:hr_bathymetry_area'},
        {'url': 'https://ows.emodnet-bathymetry.eu/wfs', 'feature_type': 'world:sea_names'},
    ]
    versions = [None, '1.0.0', '1.1.0', '2.0.0']
    WFS_STREAMS = [
        {'version': version, **stream}
        for stream in _WFS_STREAMS
        for version in versions
    ]

    _WMS_STREAMS = [
        {
            'url': 'https://services.data.shom.fr/INSPIRE/wms/r',
            'layer': 'BATHYELLI_ZH_PYR_PNG_3857_WMSR',
            'classes': [[i*10, (i+1)*10, i*10] for i in range(26)],
        },
        {
            'url': 'https://ows.emodnet-bathymetry.eu/wms',
            'layer': 'emodnet:mean',
            'resolution': 10000,
            'classes': [[i*10, (i+1)*10, i*10] for i in range(26)],
        },
        {
            'url': 'https://ows.emodnet-bathymetry.eu/wms',
            'layer': 'emodnet:hr_bathymetry_area',
            'resolution': 10000,
            'classes': [[i*10, (i+1)*10, i*10] for i in range(26)],
        },
    ]
    versions = [None, '1.0.0', '1.1.0', '1.3.0']
    WMS_STREAMS = [
        {'version': version, **stream}
        for stream in _WMS_STREAMS
        for version in versions
    ]

    for wfs in WFS_STREAMS:
        print("------------------")
        res = requests.post(f"{URL}/api/stream-geo-data/upload-wfs", json=wfs, auth=bearer_auth)
        print(res.status_code, res.json())
        stream_id = (res.json() or {}).get('stream', {}).get('id', None)
        if stream_id is None:
            continue
        res = requests.post(f"{URL}/api/stream-geo-data/{stream_id}/versions", auth=bearer_auth)
        print(res.status_code, res.json())

    for wms in WMS_STREAMS:
        print("------------------")
        res = requests.post(f"{URL}/api/stream-geo-data/upload-wms", json=wms, auth=bearer_auth)
        print(res.status_code, res.json())
        stream_id = (res.json() or {}).get('stream', {}).get('id', None)
        if stream_id is None:
            continue
        res = requests.post(f"{URL}/api/stream-geo-data/{stream_id}/versions", json={'classes': None}, auth=bearer_auth)
        print(res.status_code, res.json())
