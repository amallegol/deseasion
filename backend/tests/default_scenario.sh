#!/bin/sh

url="http://localhost"
user="admin"
password="admin"

docker compose exec backend flask db create

docker compose exec backend docker build -t registry.gitlab.com/decide.imt-atlantique/deseasion/sandbox:latest -f docker/sandbox/Dockerfile .


printf '%s\n' "${user}@deseasion.org" "${user}" "${password}" "${password}" "y" "y" "" | docker compose exec -T backend flask user create

: '
tokens=$(curl --user "${user}:${password}" ${url}/api/login)
auth_token=$(printf "${tokens}" | python -c "import sys, json; print(json.load(sys.stdin)['access_token'])")


curl ${url}/api/user \
    -H "Accept: application/json" \
    -H "Authorization: Bearer ${auth_token}"

curl -X POST ${url}/api/projects \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}" \
    -d '{"name": "Test","description": "Blabla"}'

curl -X PUT ${url}/api/project/1/permissions \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}" \
    -d '{"is_public": false}'

curl -X POST ${url}/api/geo-data/upload \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}" \
    -d @DATA_SAMPLES/courant_sup2m.zip

curl -X PUT ${url}/api/geo-data/1/permissions \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}" \
    -d '{"is_public": false}'

curl -X POST ${url}/api/project/1/shared-data \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}" \
    -d '{"data_id": 1,"data_type": "geo_data"}'

curl -X POST ${url}/api/stream-geo-data/upload-wfs \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}" \
    -d '{"url": "https://services.data.shom.fr/INSPIRE/wfs?service=WFS&version=1.0.0&request=GetFeature&typeName=ARCHIVES_CARTES_MARINES_GRILLES%3Agrilles_archipel_cm_30&outputFormat=json"}'

curl -X POST ${url}/api/stream-geo-data/upload-wms \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}" \
    -d '{"url": "https://services.data.shom.fr/INSPIRE/wms/r?request=GetMap&layers=BATHYELLI_ZH_PYR_PNG_3857_WMSR&format=image%2Fgeotiff&srs=EPSG%3A4326&bbox=-8.03132%2C40.9064%2C10.6362%2C51.6499&width=1000&height=600&styles=Bathyelli&version=1.1.1", "classes": [[0, 50, 0], [50, 100, 50], [100, 200, 100], [200, 250, 200]]}'

curl -X PUT ${url}/api/stream-geo-data/1 \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}" \
    -d '{"name": "TestWFS"}'

curl -X PUT ${url}/api/stream-geo-data/1/permissions \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}" \
    -d '{"is_public": true}'

curl ${url}/api/stream-geo-data/1/permissions \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}"

curl ${url}/api/stream-geo-data/1 \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}"

curl ${url}/api/stream-geo-data/1/versions \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}"

curl -X POST ${url}/api/stream-geo-data/1/versions \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}"

curl -X POST ${url}/api/stream-geo-data/2/versions \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}" \
    -d '{"classes": [[0, 50, 0], [50, 100, 50], [100, 200, 100], [200, 250, 200]]}'

curl -X PUT ${url}/api/geo-data/1 \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}" \
    -d '{"stream": null}'

curl -X DELETE ${url}/api/stream-geo-data/2/version/2 \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}"

curl -X DELETE ${url}/api/stream-geo-data/2 \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}"

curl ${url}/api/geo-data \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer ${auth_token}"

'
