import sys, inspect
import json
from pathlib import Path
from typing import Type

root = Path(__file__).parent.parent

sys.path.append(str(root))

from api.schemas import *


def get_schemas_classes() -> dict[str, Type[BaseSchema]]:
    res = {}
    for name, obj in inspect.getmembers(sys.modules[__name__]):
        if inspect.isclass(obj) and (issubclass(obj, BaseSchema) or issubclass(obj, OneOfSchema)):
            res[name] = obj
    return res


schema_classes = get_schemas_classes()


def schema_only_fields(klass: Type[BaseSchema], only: tuple = None, exclude: tuple = None) -> set:
    declared_fields = {k: v for k,v in klass._declared_fields.items() if v is not None}
    # Fields that can be referenced by other schema nesting it or instanciation
    allowed_ref_nested_fields = set(klass.Meta.fields) if hasattr(klass.Meta, "fields") else set(declared_fields.keys())
    only_fields = set(declared_fields.keys())
    if only:
        only_fields = set(only)
    if exclude:
        only_fields -= set(exclude)
    return only_fields


def field_to_schema(field, field_name: str, ancestor_schemas : list[Type[BaseSchema]]):
    parent = ancestor_schemas[-1]
    match field:
        case fields.Pluck():
            nested = parent if field.nested == "self" else field.nested
            nested = nested if not isinstance(nested, str) else schema_classes[nested]
            nested = type(nested) if not inspect.isclass(nested) else nested
            return schema_to_json(nested, only=field.only, many=field.many, pluck=True)
        case fields.Nested():
            nested = parent if field.nested == "self" else field.nested
            nested = nested if not isinstance(nested, str) else schema_classes[nested]
            nested = type(nested) if not inspect.isclass(nested) else nested
            res = schema_to_json(nested, only=field.only, exclude=field.exclude, many=field.many, ancestor_schemas=ancestor_schemas)
            key = getattr(field, "data_key", None)
            key = getattr(field, "attribute", key)
            if key is not None:
                return {key: res}
            return res
        case fields.List():
            return [field_to_schema(field.inner, field_name, ancestor_schemas)]
        case fields.Enum():
            return field.enum.__name__
        case fields.String():
            return "str"
        case fields.UUID():
            return "uuid"
        case fields.Float():
            return "float"
        case fields.Integer():
            return "int"
        case fields.Date():
            return "date"
        case fields.DateTime():
            return "datetime"
        case fields.Boolean():
            return "bool"
        case fields.Raw():
            db_field = getattr(parent.Meta.model, field_name, None)
            if db_field:
                return f"Raw: {db_field.type}"
            return f"{type(field).__name__} ???"
        case _:
            return f"{type(field).__name__} ???"


def schema_to_json(klass: Type[BaseSchema], only: tuple = None, exclude: tuple = None, many: bool = False, pluck: bool = False, ancestor_schemas: list[Type[BaseSchema]] = None) -> dict:
    ancestor_schemas = [] if ancestor_schemas is None else ancestor_schemas
    declared_fields = {k: v for k,v in klass._declared_fields.items() if v is not None}
    only_fields = schema_only_fields(klass, only=only, exclude=exclude)
    if ancestor_schemas.count(klass) > 1:
        return f"{klass.__name__}({only_fields})"
    res = {"schema": klass.__name__}
    #if hasattr(klass, "type_schemas"):
    #    print(f"{klass.__name__} has choices: {klass.type_schemas}")
    if only:
        only_fields = set(only)
    if exclude:
        only_fields -= set(exclude)
    for k in only_fields:
        if k not in declared_fields:
            res[k] = "ValidationError: unknown field"
            continue
        field = declared_fields[k]
        res[k] = field_to_schema(field, k, ancestor_schemas + [klass])
    if pluck and len(res) != 2:
        res = f"Bad pluck {klass.__name__} ???"
    elif pluck:
        field = only_fields.pop()
        res = f"{klass.__name__}.{field}:{res[field]}"
    if many:
        res = [res]
    return res


json_schemas = []
for name, klass in schema_classes.items():
    json_schemas.append(schema_to_json(klass))


print(json.dumps(json_schemas, indent=2))


from sqlalchemy import inspect as sql_inspect
from api.models import *


def get_model_classes() -> list[Type]:
    res = []
    for name, obj in inspect.getmembers(sys.modules[__name__]):
        if inspect.isclass(obj) and issubclass(obj, db.Model):
            res.append(obj)
    return res

"""
print(get_model_classes())

print(list(sql_inspect(Project).attrs))

print(list(Project.__table__.columns))
print(list(Project.__table__.foreign_keys))
"""