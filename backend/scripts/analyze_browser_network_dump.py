"""
Usage
-----

This script filters useful data from a Chromium network dump file (.har)

.. code:: bash

    python analyze_browser_network_dump.py FILE

    FILE is the input .har dump file.

It produces the filtered data on STDOUT.
"""
import json


def parse_dict(dico: dict):
    """Parse a Chromium network dump.

    :param dico:
    :return: filtered data from the dump
    """
    res_list = []
    for entry in dico["log"]["entries"]:
        res = {}
        request = entry["request"]
        res["request"] = {
            "method": request["method"],
            "url": request["url"],
            "headers": [],
        }
        res["request"]["queryString"] = request.get("queryString", [])
        res["request"]["postData"] = request.get("postData", {})
        if "json" in res["request"]["postData"].get("mimeType", ""):
            res["request"]["postData"]["text"] = json.loads(
                res["request"]["postData"]["text"]
            )
        headers = request["headers"]
        for header in headers:
            if header.get("name", None) == "Authorization":
                res["request"]["headers"].append(header)
        response = entry["response"]
        res["response"] = {
            "status": response["status"],
            "content": response["content"]
        }
        if "json" in res["response"]["content"]["mimeType"]:
            res["response"]["content"]["text"] = json.loads(
                res["response"]["content"]["text"]
            )
        res_list.append(res)
    return res_list


if __name__ == "__main__":
    import json
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("file", type=str)

    args = parser.parse_args()

    with open(args.file) as f:
        data = json.load(f)

    print(
        json.dumps(parse_dict(data), indent=2)
    )

