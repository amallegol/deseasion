import app_config from '../config';
import axios from 'axios';

//let customGeoDataActions = { download: { method: 'GET', url: 'geo-data{/id}/export', responseType: 'blob' }}

export default {
    getUserDetails: function () {
        return axios.get('user');
    },
    loginBasic: function (username, password) {
        let auth = 'Basic ' + window.btoa(username + ':' + password);
        return axios.get('login', { headers: { [app_config.BASIC_AUTHENTICATION_HEADER]: auth }});
    },
    refreshToken: function (token) {
        return axios.get('refresh', { headers: { [app_config.JWT_AUTHENTICATION_HEADER]: token }});
    },
    /**
     * Returns the list of all the users
     */
    getAllUsers: function (token) {
        return axios.get('users');
    },
    /**
     * Returns the list of project data
     * @param {array} ids - the ids of the data to load
     * @param {array} fields - the list of fields to load for each data
     */
    getProjectDataList: function (ids, fields=null) {
        let args = { ids: ids.join(',') };
        if (fields !== null && 'length' in fields) {
            args = Object.assign(args, { fields: fields.join(',') });
        }
        return axios.get('project-data', {params: args});
    },
    getProjectDataDetails: function (id) {
        return axios.get(`project-data/${id}`);
    },
    updateProjectData: function (id, data) {
        return axios.put(`project-data/${id}`, data=data);
    },
    deleteProjectData: function (id) {
        return axios.delete(`project-data/${id}`);
    },
    processProjectData: function (id) {
        return axios.post(`project-data/${id}`, {}, {params: {action: 'process', chain: false}});
    },
    getMRSortInferenceData: function (projectDataId) {
        return axios.get(`project-data/${projectDataId}/mr-sort-inference`);
    },
    loadMRSortInferenceAlternatives: function (projectDataId) {
        return axios.post(`project-data/${projectDataId}/mr-sort-inference`, {}, {params: {action: 'load'}});
    },
    inferMRSortData: function (projectDataId) {
        return axios.post(`project-data/${projectDataId}/mr-sort-inference`, {}, {params: {action: 'infer'}});
    },
    updateMRSortInferenceData: function (projectDataId, data) {
        return axios.put(`project-data/${projectDataId}/mr-sort-inference`, data);
    },
    shareProjectData: function (projectDataId, expiration) {
        let payload = {
            expiration: expiration
        };
        return axios.post(`project-data/${projectDataId}/share`, payload);
    },
    deleteSharedGeoData: function(uid) {
        return axios.delete(`shares/${uid}`);
    },
    /**
     * Returns the permissions of the project
     * @param {int} id - the id of the project
     */
    getProjectPermissions: function (id) {
        return axios.get(`project/${id}/permissions`);
    },
    /**
     * Modifies the permissions of the project
     * @param {int} id - the id of the project
     * @param {Object} data - the new permissions
     */
    updateProjectPermissions: function (id, data) {
        return axios.put(`project/${id}/permissions`, data);
    },
    getProjectShares: function (projectId) {
        return axios.get(`project/${projectId}/shared`)
    },
    createProjectDataInput: function (id, data) {
        return axios.post(`project-data/${id}/input-data`, data)
    },
    updateProjectDataActiveModel: function (id, data) {
        return axios.put(`project-data/${id}/model`, data);
    },
    updateProjectDataModel: function (id, modelId, data) {
        return axios.put(`project-data/${id}/model/${modelId}`, data);
    },
    deleteProjectDataModel: function (id, modelId) {
        return axios.delete(`project-data/${id}/model/${modelId}`);
    },
    changeProjectDataModel: function (dataId, modelId) {
        return axios.post(`project-data/${dataId}/model/change`, {id: modelId});
    },
    getZonePropositionDetails: function (id) {
        return axios.get(`zone-proposition/${id}`);
    },
    downloadZoneProposition: function (id, format=null) {
        return axios.get(`zone-proposition/${id}/export`, {responseType: 'blob', params: {outputFormat: format}});
    },
    /**
     * Create a template from a project
     * @param {int) projectId - the id of the project
     */
    createProjectTemplate: function (projectId, data) {
        return axios.post(`project/${projectId}/template`, data);
    },
    /**
     * Returns the full list of projects
     */
    getProjectsList: function () {
        return axios.get(`projects`);
    },
    /**
     * Creates a new project
     * @param {object} data - the data of the project (name and description)
     */
    createProject: function (data) {
        return axios.post(`projects`, data);
    },
    /**
     * Returns the list of templates of the user
     */
    getTemplatesList: function () {
        return axios.get('templates');
    },
    /**
     * Update the information of a template
     */
    updateTemplate: function (id, data) {
        return axios.put(`template/${id}`, data);
    },
    /**
     * Delete a template
     */
    deleteTemplate: function (id) {
        return axios.delete(`template/${id}`);
    },
    getProjectDetails: function (id) {
        return axios.get(`project/${id}`);
    },
    updateProject: function (id, data) {
        return axios.put(`project/${id}`, data);
    },
    deleteProject: function (id) {
        return axios.delete(`project/${id}`);
    },
    createProjectSharedData: function (id, data) {
        return axios.post(`project/${id}/shared-data`, data);
    },
    deleteProjectSharedData: function (id, data_id) {
        return axios.delete(`project/${id}/shared-data/${data_id}`);
    },
    getProjectSharedData: function (id) {
        return axios.get(`project/${id}/shared-data`);
    },
    getProjectHierarchy: function (id) {
        return axios.get(`project/${id}/hierarchy`);
    },
    processProjectObjective: function (projectId, data) {
        return axios.post(`project/${projectId}/objective`, data);
    },
    getProjectTasksList: function (projectId, limit, offset) {
        let args = {};
        if (limit !== undefined) {
            args['limit'] = limit;
        }
        if (offset !== undefined) {
            args['offset'] = offset;
        }
        return axios.get(`project/${projectId}/tasks`, {params: args});
    },
    getTask: function (taskId) {
        return axios.get(`task/${taskId}`);
    },
    revokeTask: function (taskId) {
        return axios.delete(`task/${taskId}`);
    },
    getDataDetails: function (id) {
        return axios.get(`data/${id}`);
    },
    getGlobalDataList: function () {
        return axios.get(`global-data`);
    },
    postGlobalData: function (data) {
        return axios.post(`global-data`, data);
    },
    getGlobalDataDetails: function (id) {
        return axios.get(`global-data/${id}`);
    },
    updateGlobalData: function (id, data) {
        return axios.put(`global-data/${id}`, data);
    },
    deleteGlobalData: function (id) {
        return axios.delete(`global-data/${id}`);
    },
    /**
     * Returns the permissions of the global data
     * @param {int} id - the id of the global data
     */
    getGlobalDataPermissions: function (id) {
        return axios.get(`global-data/${id}/permissions`);
    },
    /**
     * Modifies the permissions of the global data
     * @param {int} id - the id of the global data
     * @param {Object} data - the new permissions
     */
    updateGlobalDataPermissions: function (id, data) {
        return axios.put(`global-data/${id}/permissions`, data);
    },
    getGeoDataList: function () {
        return axios.get(`geo-data`);
    },
    getGeoDataDetails: function (id) {
        return axios.get(`geo-data/${id}`);
    },
    updateGeoData: function (id, data) {
        return axios.put(`geo-data/${id}`, data);
    },
    deleteGeoData: function (id) {
        return axios.delete(`geo-data/${id}`);
    },
    /**
     * Returns the permissions of the geo-data
     * @param {int} id - the id of the geo-data
     */
    getGeoDataPermissions: function (id) {
        return axios.get(`geo-data/${id}/permissions`);
    },
    /**
     * Modifies the permissions of the geo-data
     * @param {int} id - the id of the geo-data
     * @param {Object} data - the new permissions
     */
    updateGeoDataPermissions: function (id, data) {
        return axios.put(`geo-data/${id}/permissions`, data);
    },
    downloadGeoData: function (id, format=null) {
        return axios.get(`geo-data/${id}/export`, {responseType: 'blob', params: {outputFormat: format}});
    },
    /**
     * Returns the details of a data attribute.
     * @param {int} id - The id of the data attribute.
     * @param {boolean} values - Wether to return the values or not.
     */
    getDataAttributeDetails: function (id, values) {
        let params = { id: id };
        if (values !== undefined) {
            params['values'] = values;
        }
        return axios.get(`attribute/${id}`, {params: params});
    },
    getFeatureDetails: function (id) {
        return axios.get(`feature/${id}`);
    },
    uploadGeoData: function (data) {
        return axios.post(`geo-data/upload`, data);
    },
    uploadRasterGeoData: function (data) {
        return axios.post(`geo-data/upload-raster`, data);
    },
    getStreamList: function () {
        return axios.get(`stream-geo-data`);
    },
    deleteStream: function (id) {
        return axios.delete(`stream-geo-data/${id}`);
    },
    updateStream: function (id, data) {
        return axios.put(`stream-geo-data/${id}`, data);
    },
    getStreamDetails: function (id) {
        return axios.get(`stream-geo-data/${id}`);
    },
    updateStreamPermissions: function (id, data) {
        return axios.put(`stream-geo-data/${id}/permissions`, data);
    },
    getStreamPermissions: function (id) {
        return axios.get(`stream-geo-data/${id}/permissions`);
    },
    /**
     * Add a new version of a geo-data stream.
     * @param {int} id - The id of the stream.
     * @param {Object} classes - Like rasters, WMS streams versions need classes.
     */
    addStreamVersion: function (id, data) {
        return axios.post(`stream-geo-data/${id}/versions`, data);
    },
    getStreamVersions: function (id) {
        return axios.get(`stream-geo-data/${id}/versions`);
    },
    detachStreamVersion: function (streamId, geoDataId) {
        return axios.delete(`stream-geo-data/${streamId}/version/${geoDataId}`);
    },
    uploadStreamWFS: function (data) {
        return axios.post(`stream-geo-data/upload-wfs`, data);
    },
    uploadStreamWMS: function (data) {
        return axios.post(`stream-geo-data/upload-wms`, data);
    },
};
