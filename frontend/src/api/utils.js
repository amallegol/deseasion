import app_config from '../config';
import router from '../routes';
import store from '../vuex/store';
import axios from 'axios';

function setAxios(){
    axios.defaults.baseURL = app_config.API_ROOT;
    axios.interceptors.request.use(
        function (config){
            if (store.getters.userAuthenticated && !config.headers.get(app_config.JWT_AUTHENTICATION_HEADER)){
                config.headers.set(app_config.JWT_AUTHENTICATION_HEADER, store.getters.jwtToken);
            }
            return config;
        }
    )
    
    axios.interceptors.response.use(
        function (response) {
            response.headers.delete();
            return response;
        },
        function (error){
            if (error.response.status === 401) {
                error.config.headers.delete(app_config.JWT_AUTHENTICATION_HEADER);
                console.error('Error with the authentication:', error.message);
                if (store.getters.userAuthenticated && error.request.responseURL !== app_config.API_ROOT+'/refresh') {
                    return store.dispatch('refreshUserToken')
                    .then(() => {
                        return axios.request(error.config)
                        .then((resp) => { return resp; });
                    })
                    .catch((err) => {
                        console.error('Refresh token invalid, logging the user out');
                        store.dispatch('logoutUser');
                        router.replace({ path: '/login', query: {redirect: router.currentRoute.value.path }});
                    });
                }
            }
            return Promise.reject(error);
        }
    )
}

export default setAxios;