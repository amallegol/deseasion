import chroma from 'chroma-js';

const getDivergingColors = function (nbr, baseColor) {
    const colorBrBG = new Map([
        [1, ['#f5f5f5']],
        [2, ['#d8b365', '#5ab4ac']],
        [3, ['#d8b365', '#f5f5f5', '#5ab4ac']],
        [4, ['#a6611a', '#dfc27d', '#80cdc1', '#018571']],
        [5, ['#a6611a', '#dfc27d', '#f5f5f5', '#80cdc1', '#018571']],
        [6, ['#8c510a', '#d8b365', '#f6e8c3', '#c7eae5', '#5ab4ac', '#01665e']],
        [7, ['#8c510a', '#d8b365', '#f6e8c3', '#f5f5f5', '#c7eae5', '#5ab4ac', '#01665e']],
        [8, ['#8c510a', '#bf812d', '#dfc27d', '#f6e8c3', '#c7eae5', '#80cdc1', '#35978f', '#01665e']],
        [9, ['#8c510a', '#bf812d', '#dfc27d', '#f6e8c3', '#f5f5f5', '#c7eae5', '#80cdc1', '#35978f', '#01665e']],
        [10, ['#543005', '#8c510a', '#bf812d', '#dfc27d', '#f6e8c3', '#c7eae5', '#80cdc1', '#35978f', '#01665e', '#003c30']],
        [11, ['#543005', '#8c510a', '#bf812d', '#dfc27d', '#f6e8c3', '#f5f5f5', '#c7eae5', '#80cdc1', '#35978f', '#01665e', '#003c30']]
    ]);
    const colorPRGn = new Map([
        [3, ['#af8dc3', '#f7f7f7', '#7fbf7b']],
        [4, ['#7b3294', '#c2a5cf','#a6dba0', '#008837']],
        [5, ['#7b3294', '#c2a5cf', '#f7f7f7', '#a6dba0', '#008837']],
        [6, ['#762a83', '#af8dc3', '#e7d4e8', '#d9f0d3', '#7fbf7b', '#1b7837']],
        [7, ['#762a83', '#af8dc3', '#e7d4e8', '#f7f7f7', '#d9f0d3', '#7fbf7b', '#1b7837']],
        [8, ['#762a83', '#9970ab', '#c2a5cf', '#e7d4e8', '#d9f0d3', '#a6dba0', '#5aae61', '#1b7837']],
        [9, ['#762a83', '#9970ab', '#c2a5cf', '#e7d4e8', '#f7f7f7', '#d9f0d3', '#a6dba0', '#5aae61', '#1b7837']],
        [10, ['#40004b', '#762a83', '#9970ab', '#c2a5cf', '#e7d4e8', '#d9f0d3', '#a6dba0', '#5aae61', '#1b7837', '#00441b']],
        [11, ['#40004b', '#762a83', '#9970ab', '#c2a5cf', '#e7d4e8', '#f7f7f7', '#d9f0d3', '#a6dba0', '#5aae61', '#1b7837', '#00441b']]
    ]);
    const colorRdBu = new Map([
        [1, ['#f7f7f7']],
        [2, ['#ef8a62', '#67a9cf']],
        [3, ['#ef8a62', '#f7f7f7', '#67a9cf']],
        [4, ['#ca0020', '#f4a582', '#92c5de', '#0571b0']],
        [5, ['#ca0020', '#f4a582', '#f7f7f7', '#92c5de', '#0571b0']],
        [6, ['#b2182b', '#ef8a62', '#fddbc7', '#d1e5f0', '#67a9cf', '#2166ac']],
        [7, ['#b2182b', '#ef8a62', '#fddbc7', '#f7f7f7', '#d1e5f0', '#67a9cf', '#2166ac']],
        [8, ['#b2182b', '#d6604d', '#f4a582', '#fddbc7', '#d1e5f0', '#92c5de', '#4393c3', '#2166ac']],
        [9, ['#b2182b', '#d6604d', '#f4a582', '#fddbc7', '#f7f7f7', '#d1e5f0', '#92c5de', '#4393c3', '#2166ac']],
        [10, ['#67001f', '#b2182b', '#d6604d', '#f4a582', '#fddbc7', '#d1e5f0', '#92c5de', '#4393c3', '#2166ac', '#053061']],
        [11, ['#67001f', '#b2182b', '#d6604d', '#f4a582', '#fddbc7', '#f7f7f7', '#d1e5f0', '#92c5de', '#4393c3', '#2166ac', '#053061']]
    ]);
    let color = undefined;
    if (baseColor === 'RdBu') { color = colorRdBu; }
    else if (baseColor === 'BrBG') { color = colorBrBG; }
    else if (baseColor === 'PRGn') { color = colorPRGn; }
    if (color !== undefined && color.has(nbr)) {
        return color.get(nbr);
    } else {
        return chroma.scale(baseColor).colors(nbr);
    }
};

const getRandomColor = function (alphaColor=1){
    let randColors = [];
    for (var i = 0; i < 3; i++) {
        randColors.push(Math.floor(Math.random() * 256));
    }
    return `rgba(${randColors[0]}, ${randColors[1]}, ${randColors[2]}, ${alphaColor})`;
}

export default {getDivergingColors, getRandomColor};