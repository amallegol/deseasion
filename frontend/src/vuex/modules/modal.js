import * as types from '../types';


const state = {
    modalName: undefined,
    showModal: false,
    data: {},
    promiseResolve: undefined,
    promiseReject: undefined
};

const getters = {
    modalName: state => state.modalName,
    showModal: state => state.showModal,
    modalData: state => state.data
};

const mutations = {
    [types.MODAL_SHOW] (state, showModal) {
        state.showModal = showModal;
    },
    [types.MODAL_SET_DATA] (state, data) {
        state.data = data;
    },
    [types.MODAL_SET_TEMPLATE] (state, name) {
        state.modalName = name;
    },
    [types.MODAL_SET_RESOLVE_CB] (state, resolve) {
        state.promiseResolve = resolve;
    },
    [types.MODAL_SET_REJECT_CB] (state, reject) {
        state.promiseReject = reject;
    }
};

const actions = {
    /**
     * Display a new modal window
     * @param {Object} params - the parameters of the modal
     * @param {string} params.template - the name of the template component to use, can be one of the following:
     *                                   'criterion-edit', 'intermediate-data-edit', 'geo-data-view'
     * @param {Object} params.data - the data to pass to the template component (view the details on each component)
     */
    showModal: ({ commit }, { template, data = null }) => {
        return new Promise((resolve, reject) => {
            commit(types.MODAL_SET_RESOLVE_CB, resolve);
            commit(types.MODAL_SET_REJECT_CB, reject);
            commit(types.MODAL_SET_DATA, data);
            commit(types.MODAL_SET_TEMPLATE, template);
            commit(types.MODAL_SHOW, true);
        });
    },
    resolveModal: ({ state }, data) => {
        if (state.promiseResolve !== undefined && state.promiseResolve !== null) {
            state.promiseResolve(data);
        }
    },
    rejectModal: ({ state }, data) => {
        if (state.promiseReject !== undefined && state.promiseReject !== null) {
            state.promiseReject(data);
        }
    },
    closeModal: ({ commit }) => {
        commit(types.MODAL_SHOW, false);
    }
};

const modal = {
    state,
    getters,
    mutations,
    actions
};

export default modal;