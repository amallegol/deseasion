import * as types from '../types';
import api from '../../api';

const state = {
    globalData: {},
    globalDataOrder: [],
    globalDataPermissions: undefined
};


const getters = {
    globalData: state => state.globalData,
    globalDataOrder: state => state.globalDataOrder,
    globalDataPermissions: state => state.globalDataPermissions
}


const mutations = {
    [types.ADD_GLOBAL_DATA] (state, globalData) {
        state.globalData = Object.assign({}, state.globalData);
        state.globalData[globalData.id] = globalData
        if (state.globalDataOrder.indexOf(globalData.id) === -1) {
            state.globalDataOrder.push(globalData.id);
        }
    },
    [types.REMOVE_GLOBAL_DATA] (state, id) {
        state.globalData = Object.assign({}, state.globalData);
        delete state.globalData[id];
        let index = state.globalDataOrder.indexOf(id);
        if (index !== -1) {
            state.globalDataOrder.splice(index, 1);
        }
    },
    [types.SET_GLOBAL_DATA_ORDER] (state, order) {
        state.globalDataOrder = order;
    },
    [types.CLEAR_GLOBAL_DATA] (state) {
        state.globalData = {};
        state.globalDataOrder = [];
    },
    [types.SET_GLOBAL_DATA_PERMISSIONS] (state, permissions) {
        state.globalDataPermissions = permissions;
    }
}


const actions = {
    clearGlobalData: ({ commit }) => {
        commit(types.CLEAR_GLOBAL_DATA);
        return Promise.resolve();
    },
    loadGlobalData: ({ commit, dispatch }, id) => {
        return new Promise((resolve, reject) => {
            api.getGlobalDataDetails(id)
            .then((resp) => {
                let data = resp.data;
                if (data.properties.length === 0) {
                    let msg = 'The data "' + data.name + '" does not contain any properties';
                    dispatch('displayNotification', { content: msg, type: 'warning' });
                }
                commit(types.ADD_GLOBAL_DATA, data);
                resolve();
            })
            .catch((resp) => {
                dispatch('displayNotification', { content: resp.data && resp.message || 'Error loading the global data' });
                reject();
            });
        });
    },
    moveGlobalDataOrder: ({ commit, getters }, payload) => {
        let oldId = payload.oldIndex;
        let newId = payload.newIndex;
        let order = getters.globalDataOrder.slice(0);
        if (oldId >= 0 && oldId < order.length && newId >= 0 && newId < order.length) {
            order.splice(newId, 0, order.splice(oldId, 1)[0]);
            commit(types.SET_GLOBAL_DATA_ORDER, order);
        }
    },
    removeGlobalData: ({ commit }, id) => {
        commit(types.REMOVE_GLOBAL_DATA, id);
    },
    updateGlobalData: ({ commit, dispatch }, params) => {
        if ('id' in params && 'data' in params) {
            return new Promise((resolve, reject) => {
                api.updateGlobalData(params.id, params.data).then(() => {
                    resolve();
                }, (resp) => {
                    dispatch('displayNotification', { content: resp.data.message || 'Error updating the GlobalData' });
                    reject(resp.data);
                });
            });
        }
    },
    loadGlobalDataPermissions: ({ commit, dispatch }, globalDataId) => {
        return api.getGlobalDataPermissions(globalDataId)
        .then((resp) => {
            commit(types.SET_GLOBAL_DATA_PERMISSIONS, resp.data.access);
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error loading the global data permissions' });
            commit(types.SET_GLOBAL_DATA_PERMISSIONS, undefined);
            throw resp;
        });
    },
    updateGlobalDataPermissions: ({ dispatch }, params) => {
        return api.updateGlobalDataPermissions(params.globalDataId, params.data)
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error updating the global data permissions' });
            throw error;
        });
    }
}

const globalData = {
    state,
    getters,
    mutations,
    actions
};

export default globalData;
