import * as types from '../types';
import api from '../../api';

const state = {
    project: {},
    hierarchy: undefined,
    sharedData: [],
    projectPermissions: undefined,
    projectTemplates: []
};

const getters = {
    projectDetails: state => state.project,
    projectSharedData: state => state.sharedData,
    projectHierarchyFull: state => state.hierarchy,
    projectExtent: state => {
        if (state.project != undefined && state.project.extent != undefined) {
            return state.project.extent;
        } else {
            return undefined;
        }
    },
    projectHierarchy: (state) => {
        const sharedIds = state.sharedData.map((data) => data.id);
        const filterHierarchy = (root) => {
            let copy = Object.assign({}, root, { input_data: [] });
            for (let input of root.input_data) {
                if (sharedIds.includes(input.id) === false) {
                    copy.input_data.push(filterHierarchy(input));
                }
            }
            return copy;
        };
        if (state.hierarchy) {
            return filterHierarchy(state.hierarchy);
        } else {
            return undefined;
        }
    },
    projectHierarchyFlat: (state) => {
        const sharedIds = state.sharedData.map((data) => data.id);
        const getFlatTree = (root, parent=null, parentId=null) => {
            let isOutdated = false;
            if (root.last_update === null) {
                isOutdated = true;
            } else {
                isOutdated = root.input_data.some((input) => input.last_update !== null && input.last_update > root.last_update);
            }
            let rootData = {
                name: root.name,
                id: root.id,
                outdated: isOutdated,
                parentId: parentId,
                hasChildren: false,
                level: parent !== null ? parent.level + 1 : 0,
            };
            let flatTree = [ rootData ];
            for (let child of root.input_data) {
                if (sharedIds.includes(child.id) === false) {
                    rootData.hasChildren = true;
                    flatTree.push(...getFlatTree(child, rootData, root.id));
                }
            }
            return flatTree;
        };
        if (state.hierarchy) {
            return getFlatTree(state.hierarchy);
        } else {
            return null;
        }
    },
    projectPermissions: state => state.projectPermissions,
    projectTemplates: state => state.projectTemplates
};

const mutations = {
    [types.SET_PROJECT] (state, project) {
        state.project = project;
    },
    [types.SET_PROJECT_HIERARCHY] (state, hierarchy) {
        state.hierarchy = hierarchy;
    },
    [types.SET_PROJECT_SHARED_DATA] (state, data) {
        state.sharedData = data;
    },
    [types.SET_PROJECT_PERMISSIONS] (state, permissions) {
        state.projectPermissions = permissions;
    },
    [types.SET_PROJECT_TEMPLATES] (state, templates) {
        state.projectTemplates = templates;
    }
};

const actions = {
    loadProject: ({ commit, dispatch, getters }, id) => {
        return api.getProjectDetails(id)
        .then((resp) => {
            commit(types.SET_PROJECT, resp.data.project);
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error loading the project details' });
            throw resp;
        });
    },
    deleteProject: ({ commit, dispatch, getters }, id) => {
        return api.deleteProject(id)
        .then(() => {
            if (getters.projectDetails && getters.projectDetails.id === id) {
                this.commit(types.SET_PROJECT, {});
            }
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error deleting project' });
            throw resp;
        });
    },
    setProjectDetails: ({ commit }, data) => {
        commit(types.SET_PROJECT, data);
    },
    updateProject: ({ commit, dispatch, getters }, data) => {
        return api.updateProject(getters.projectDetails.id, data)
        .then((resp) => {
            commit(types.SET_PROJECT, resp.data.project);
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error updating the project' });
            throw resp;
        });
    },
    loadProjectHierarchy: ({ commit, dispatch, getters }) => {
        // load the shared data first to know which data we should display in the hierarchy tree
        return dispatch('loadProjectSharedData')
        .then(() => api.getProjectHierarchy(getters.projectDetails.id))
        .then((resp) => commit(types.SET_PROJECT_HIERARCHY, resp.data.hierarchy))
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error loading the project hierarchy' });
            throw error;
        });
    },
    /* Reset the hierarchy to an empty hierarchy. */
    resetProjectHierarchy: ({ commit }) => {
        commit(types.SET_PROJECT_HIERARCHY, undefined);
    },
    /* Create several shared data. */
    createProjectMultipleSharedData: ({ dispatch, getters }, dataList) => {
        let promises = [];
        for (let data of dataList) {
            promises.push(api.createProjectSharedData(getters.projectDetails.id, data));
        }
        Promise.all(promises).then(() => dispatch('loadProjectSharedData'))
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error creating a data in the project' });
            throw error;
        });
    },
    /* Create a shared data. */
    createProjectSharedData: ({ dispatch, getters }, data) => {
        return api.createProjectSharedData(getters.projectDetails.id, data)
        .then(() => {
            return dispatch('loadProjectSharedData');
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error creating a data in the project' });
            throw resp;
        });
    },
    deleteProjectSharedData: ({ dispatch, getters }, dataId) => {
        // TODO: delete a datageo object in a project
        return api.deleteProjectSharedData(getters.projectDetails.id, dataId)
        .then(() => {
            return dispatch('loadProjectSharedData');
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error deleting a data in the project' });
            throw resp;
        });
    },
    loadProjectSharedData: ({ commit, dispatch, getters }) => {
        return api.getProjectSharedData(getters.projectDetails.id)
        .then((resp) => {
            commit(types.SET_PROJECT_SHARED_DATA, resp.data.shared_data);
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error loading the project shared data' });
            throw resp;
        });
    },
    loadProjectPermissions: ({ commit, dispatch }, projectId) => {
        return api.getProjectPermissions(projectId)
        .then((resp) => {
            commit(types.SET_PROJECT_PERMISSIONS, resp.data.access);
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error loading the project permissions' });
            throw resp;
        });
    },
    updateProjectPermissions: ({ dispatch }, params) => {
        return api.updateProjectPermissions(params.projectId, params.data)
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error updating the project permissions' });
            throw error;
        });
    },
    processProjectObjective: ({ dispatch }, params) => {
        return api.processProjectObjective(params.projectId, params.params)
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error processing the project objective' });
            throw error;
        });
    },
    loadProjectTemplates: ({ commit, dispatch }) => {
        return api.getTemplatesList()
        .then((resp) => commit(types.SET_PROJECT_TEMPLATES, resp.data.templates))
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error loading the templates' });
            throw error;
        });
    },
    deleteProjectTemplate: ({ dispatch }, templateId) => {
        return api.deleteTemplate(templateId)
        .then(() => dispatch('loadProjectTemplates'))
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error deleting the template' });
            throw error;
        });
    },
    updateProjectTemplate: ({ dispatch }, params) => {
        return api.updateTemplate(params.templateId, params.data)
        .then(() => dispatch('loadProjectTemplates'))
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error updating the template' });
            throw error;
        });
    }
};

const project = {
    state,
    getters,
    mutations,
    actions
};

export default project;