import * as types from '../types';


const state = {
    notifications: []
};

const getters = {
    notifications: state => state.notifications
};

const mutations = {
    [types.ADD_NOTIFICATION] (state, notification) {
        state.notifications.push(notification);
    },
    [types.REMOVE_NOTIFICATION] (state, notification) {
        let index = state.notifications.indexOf(notification);
        if (index > -1) {
            state.notifications.splice(index, 1);
        }
    }
};

const actions = {
    /**
     * Display a new notification on the screen
     * @param {Object} data - the data for the notification
     * @param {string} data.content - the content of the notification, html or text
     * @param {string} data.type - the type of notification, can be 'danger' (default), 'warning', 'info' or 'success'
     * @param {number} data.delay - the duration of the notification in ms, 0 for unlimited (default: 5000)
     */
    displayNotification: ({ commit }, data) => {
        data = Object.assign({
            type: 'danger',
            delay: 5000
        }, data);
        var notification = {
            content: data.content,
            type: data.type
        };
        commit(types.ADD_NOTIFICATION, notification);
        if (data.delay > 0) {
            window.setTimeout(() => {
                commit(types.REMOVE_NOTIFICATION, notification);
            }, data.delay);
        }
    },
    /**
     * Close the given notification
     */
    closeNotification: ({ commit }, notification) => {
        commit(types.REMOVE_NOTIFICATION, notification);
    }
};

const notifier = {
    state,
    getters,
    mutations,
    actions
};

export default notifier;