import * as types from '../types';
import api from '../../api';

const state = {
    userTasks: {},
    promises: {},
    isRefreshing: false,
    timeoutId: null,
    timeoutStartDate: null
};

const getters = {
    userTasks: state => state.tasksStarted
};

const mutations = {
    [types.SET_USER_TASK] (state, task) {
        state.userTasks[String(task.task_id)] = task;
        let pResolve, pReject;
        let promise = new Promise((resolve, reject) => {
            pResolve = resolve;
            pReject = reject;
        });
        state.promises[String(task.task_id)] = { promise: promise, resolve: pResolve, reject: pReject };
    },
    [types.REMOVE_USER_TASK] (state, taskId) {
        if (String(taskId) in state.userTasks) {
            delete state.userTasks[String(taskId)];
        }
        if (String(taskId) in state.promises) {
            delete state.promises[String(taskId)];
        }
    },
    [types.SET_TASK_TIMEOUT_ID] (state, timeoutId) {
        state.timeoutId = timeoutId;
    },
    [types.SET_TASK_TIMEOUT_DATE] (state, timeoutDate) {
        state.timeoutStartDate = timeoutDate;
    }
};

const actions = {
    /**
     * Add a task to monitor and notify the user.
     *
     * @return {Promise} A promise which will be resolved when the task is successfull processed,
     *     or rejected if the task fails.
     */
    addUserTask: ({ commit, dispatch, state }, task) => {
        commit(types.SET_USER_TASK, task);
        dispatch('displayNotification', { content: 'Task ' + task.task_id + ' started', type: 'info' });
        dispatch('initTasksRefresh');
        return state.promises[String(task.task_id)].promise;
    },
    /**
     * Poll the tasks in the tasks list.
     */
    checkUserTasks: ({ commit, dispatch, state }) => {
        for (let taskId in state.userTasks) {
            let task = state.userTasks[taskId];
            api.getTask(taskId)
            .then((resp) => {
                let newTask = resp.data.task;
                if (newTask.state !== task.state) {
                    if (newTask.state === 'SUCCESS') {
                        state.promises[String(taskId)].resolve();
                        let desc = 'Task ' + newTask.task_id + ' finished successfully';
                        dispatch('displayNotification', { content: desc, type: 'success', delay: 0 });
                    } else if (newTask.state === 'FAILURE') {
                        state.promises[String(taskId)].reject();
                        let desc = 'Task ' + newTask.task_id + ' failed';
                        if (newTask.error_message !== null) {
                            desc = desc + ' with error: ' + newTask.error_message;
                        }
                        dispatch('displayNotification', { content: desc, type: 'danger', delay: 0 });
                    } else if (newTask.state === 'REVOKED') {
                        state.promises[String(taskId)].reject();
                        let desc = 'Task ' + newTask.task_id + ' revoked';
                        dispatch('displayNotification', { content: desc, type: 'warning' });
                    }
                    if (newTask.state !== 'PENDING' && newTask.state !== 'STARTED') {
                        commit(types.REMOVE_USER_TASK, newTask.task_id);
                    }
                }
            })
            .catch((resp) => {
                if (resp.status !== 404) {
                    state.promises[String(taskId)].reject(resp);
                    commit(types.REMOVE_USER_TASK, taskId);
                    throw resp;
                }
            });
        }
    },
    /**
     * Reinitialise the timeout delay for the tasks polling.
     */
    initTasksRefresh: ({ commit, dispatch, state }) => {
        commit(types.SET_TASK_TIMEOUT_DATE, Date.now());
        if (state.timeoutId !== null) {
            window.clearTimeout(state.timeoutId);
            commit(types.SET_TASK_TIMEOUT_ID, null);
        }
        dispatch('nextTasksTimeout');
    },
    /**
     * Set the timeout to check the tasks.
     *
     * The timeout delay depends on the time elapsed since the tasks list was initialised:
     *     every second for the first 10 seconds
     *     every 2 seconds for the first 30 seconds
     *     every 5 seconds for the first minute
     *     every 10 seconds after one minute
     */
    nextTasksTimeout: ({ commit, dispatch, state }) => {
        let timeout = 1000;
        let date = Date.now();
        if (date - state.timeoutStartDate > 60000) {
            timeout = 10000;
        } else if (date - state.timeoutStartDate > 30000) {
            timeout = 5000;
        } else if (date - state.timeoutStartDate > 10000) {
            timeout = 2000;
        }
        let timeoutId = window.setTimeout(() => {
            if (Object.keys(state.userTasks).length > 0) {
                dispatch('checkUserTasks');
                dispatch('nextTasksTimeout');
            } else {
                commit(types.SET_TASK_TIMEOUT_ID, null);
                commit(types.SET_TASK_TIMEOUT_DATE, null);
            }
        }, timeout);
        commit(types.SET_TASK_TIMEOUT_ID, timeoutId);
    }
};

const tasks = {
    state,
    getters,
    mutations,
    actions
};

export default tasks;