import * as types from '../types';
import api from '../../api';
import chroma from 'chroma-js';
import contentDisposition from 'content-disposition';
import colors from '@/colors';

const state = {
    geoData: {},
    geoDataOrder: [],
    geoDataParameters: {},
    geoDataFeatures: {},
    geoDataPermissions: undefined,
    feature: {}
};


const getters = {
    geoData: state => state.geoData,
    geoDataOrder: state => state.geoDataOrder,
    geoDataParameters: state => state.geoDataParameters,
    geoDataArray: state => {
        let array = [];
        for (let dataId of state.geoDataOrder) {
            array.push({
                data: state.geoData[dataId],
                params: state.geoDataParameters[dataId]
            });
        }
        return array;
    },
    geoDataDivergingColors: () => ['RdBu', 'BrBG', 'PRGn'],
    geoDataFeatures: state => state.geoDataFeatures,
    geoDataPermissions: state => state.geoDataPermissions,
    feature: state => state.feature
};


const getAttributeColor = function (geoData, attributeIndex, baseColor) {
    let attribute = geoData.attributes[attributeIndex];
    if (attribute && attribute.type === 'ordinal') {
        let base = baseColor ? baseColor : 'RdBu';
        let divColors = colors.getDivergingColors(attribute.order.length, base);
        divColors.reverse();
        let array = [];
        for (let i = 0; i < attribute.order.length; i++) {
            array.push([attribute.order[i], divColors[i]]);
        }
        return {
            type: 'map',
            values: array,
            base: base
        };
    } else if (attribute && attribute.type === 'quantitative') {
        let color = {
            type: 'scale',
            base: baseColor ? baseColor : chroma.random().hex()
        };
        if ('min' in attribute.statistics && 'max' in attribute.statistics) {
            color.domain = [attribute.statistics.min, attribute.statistics.max];
        }
        return color;
    } else {
        return {
            type: 'color',
            base: baseColor ? baseColor : chroma.random().hex()
        };
    }
};

const mutations = {
    [types.ADD_GEO_DATA] (state, geoData) {
        state.geoData = Object.assign({}, state.geoData);
        state.geoData[geoData.id] = geoData
        let attributeIndex = geoData.attributes.length > 0 ? 0 : null;
        state.geoDataParameters[geoData.id] = {
            attribute: attributeIndex,
            color: getAttributeColor(geoData, attributeIndex),
            opacity: 0.6
        }
        if (state.geoDataOrder.indexOf(geoData.id) === -1) {
            state.geoDataOrder.push(geoData.id);
        }
    },
    [types.REMOVE_GEO_DATA] (state, id) {
        state.geoData = Object.assign({}, state.geoData);
        delete state.geoData[id];
        delete state.geoDataParameters[id];
        let index = state.geoDataOrder.indexOf(id);
        if (index !== -1) {
            state.geoDataOrder.splice(index, 1);
        }
    },
    [types.SET_GEO_DATA_ORDER] (state, order) {
        state.geoDataOrder = order;
    },
    [types.SET_GEO_DATA_PARAMETERS] (state, payload) {
        if (payload.geoDataId in state.geoDataParameters) {
            state.geoDataParameters = Object.assign({}, state.geoDataParameters);
            state.geoDataParameters[payload.geoDataId] = payload.parameters;
        }
    },
    [types.CLEAR_GEO_DATA] (state) {
        state.geoData = {};
        state.geoDataOrder = [];
        state.geoDataParameters = {};
    },
    [types.SET_GEO_DATA_FEATURES] (state, features) {
        state.geoDataFeatures = features;
    },
    [types.SET_GEO_DATA_PERMISSIONS] (state, permissions) {
        state.geoDataPermissions = permissions;
    },
    [types.SET_FEATURE] (state, feature) {
        state.feature = feature;
    }
};

const actions = {
    clearGeoData: ({ commit }) => {
        commit(types.CLEAR_GEO_DATA);
        return Promise.resolve();
    },
    loadGeoData: ({ commit, dispatch }, id) => {
        return new Promise((resolve, reject) => {
            api.getGeoDataDetails(id)
            .then((resp) => {
                let data = resp.data.geodata;
                if (data.attributes.length === 0) {
                    let msg = 'The data "' + data.name + '" does not contain any attributes';
                    dispatch('displayNotification', { content: msg, type: 'warning' });
                }
                commit(types.ADD_GEO_DATA, data);
                resolve();
            })
            .catch((resp) => {
                dispatch('displayNotification', { content: resp.data && resp.message || 'Error loading the geo data' });
                reject();
            });
        });
    },
    moveGeoDataOrder: ({ commit, getters }, payload) => {
        let oldId = payload.oldIndex;
        let newId = payload.newIndex;
        let order = getters.geoDataOrder.slice(0);
        if (oldId >= 0 && oldId < order.length && newId >= 0 && newId < order.length) {
            order.splice(newId, 0, order.splice(oldId, 1)[0]);
            commit(types.SET_GEO_DATA_ORDER, order);
        }
    },
    removeGeoData: ({ commit }, id) => {
        commit(types.REMOVE_GEO_DATA, id);
    },
    loadFeature: ({ commit, dispatch, getters }, id) => {
        return api.getFeatureDetails(id)
        .then((resp) => commit(types.SET_FEATURE, resp.data.feature))
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error loading the feature' });
            throw error;
        });
    },
    clearFeature: ({ commit }) => {
        commit(types.SET_FEATURE, {});
    },
    setGeoDataParameters: ({ commit }, payload) => {
        commit(types.SET_GEO_DATA_PARAMETERS,
              { geoDataId: payload.geoDataId, parameters: payload.parameters });
    },
    setGeoDataColorAttribute: ({ commit, getters }, payload) => {
        let p = Object.assign({
            geoDataId: null,
            attributeIndex: null
        }, payload);
        let geoData = getters.geoData[p.geoDataId];
        let baseColor = getters.geoDataParameters[p.geoDataId].color.base;
        let color = getAttributeColor(geoData, p.attributeIndex, baseColor);
        let params = Object.assign(
            {},
            getters.geoDataParameters[p.geoDataId],
            { attribute: p.attributeIndex }
        );
        if (color) { params = Object.assign(params, { color: color }); }
        commit(types.SET_GEO_DATA_PARAMETERS, { geoDataId: p.geoDataId, parameters: params });
    },
    setGeoDataBaseColor: ({ commit, getters }, payload) => {
        let p = Object.assign({
            geoDataId: null,
            baseColor: null
        }, payload);
        let geoData = getters.geoData[p.geoDataId];
        let params = getters.geoDataParameters[p.geoDataId];
        let color = getAttributeColor(geoData, params.attribute, p.baseColor);
        commit(types.SET_GEO_DATA_PARAMETERS,
               { geoDataId: p.geoDataId, parameters: Object.assign({}, params, { color: color }) });
    },
    updateGeoData: ({ commit, dispatch }, params) => {
        if ('id' in params && 'data' in params) {
            return new Promise((resolve, reject) => {
                api.updateGeoData(params.id, params.data).then(() => {
                    resolve();
                }, (resp) => {
                    dispatch('displayNotification', { content: resp.data.message || 'Error updating the GeoData' });
                    reject(resp.data);
                });
            });
        }
    },
    loadGeoDataPermissions: ({ commit, dispatch }, geoDataId) => {
        return api.getGeoDataPermissions(geoDataId)
        .then((resp) => {
            commit(types.SET_GEO_DATA_PERMISSIONS, resp.data.access);
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error loading the geo data permissions' });
            commit(types.SET_GEO_DATA_PERMISSIONS, undefined);
            throw resp;
        });
    },
    updateGeoDataPermissions: ({ dispatch }, params) => {
        return api.updateGeoDataPermissions(params.geoDataId, params.data)
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error updating the geo data permissions' });
            throw error;
        });
    },
    downloadGeoData: ({ dispatch }, {id, format}) => {
        dispatch('displayNotification', { content: 'Export started', type: 'info' });
        api.downloadGeoData(id, format)
        .then((resp) => {
            download(resp);
            dispatch('displayNotification', { content: 'Data exported', type: 'success' });
        })
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error exporting the data' });
        });
    },
    downloadZoneProposition: ({ dispatch }, {id, format}) => {
        dispatch('displayNotification', { content: 'Export started', type: 'info' });
        api.downloadZoneProposition(id, format)
        .then((resp) => {
            download(resp);
            dispatch('displayNotification', { content: 'Data exported', type: 'success' });
        })
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error exporting the data' });
        });
    }
};

function download(response){
    let disposition = contentDisposition.parse(response.headers.get('content-disposition'));
    let link = document.createElement('a');
    link.href = window.URL.createObjectURL(response.data);
    let filename = disposition.parameters.filename;
    if (filename !== undefined) {
        link.download = filename;
    }
    link.onclick = function (event) { setTimeout(() => event.target.remove()); };
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

const geoData = {
    state,
    getters,
    mutations,
    actions
};

export default geoData;