import * as types from '../types';
import api from '../../api';


const state = {
    projectData: {},
    inputData: {}
};


const getters = {
    projectDataId: state => state.projectData.id,
    projectDataDetails: state => state.projectData,
    projectDataInput: state => state.inputData
};


const mutations = {
    [types.SET_PROJECT_DATA] (state, data) {
        state.projectData = data;
    },
    [types.SET_PROJECT_DATA_INPUT] (state, inputData) {
        state.inputData = inputData;
    }
};


const actions = {
    setProjectData: ({ commit }, data) => {
        commit(types.SET_PROJECT_DATA, data);
    },
    loadProjectData: ({ commit, dispatch }, id) => {
        return api.getProjectDataDetails(id)
        .then((resp) => {
            let data = resp.data.project_data;
            commit(types.SET_PROJECT_DATA, data);
            return dispatch('loadInputData', { ids: data.input_data });
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error loading the project data' });
            throw resp;
        });
    },
    updateProjectData: ({ dispatch }, params) => {
        return api.updateProjectData(params.id, params.data)
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error updating the project data' });
            throw error;
        });
    },
    deleteProjectData: ({ dispatch }, params) => {
        return api.deleteProjectData(params.id)
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error deleting the project data' });
            throw error;
        });
    },
    processProjectData: ({ dispatch, getters }, params) => {
        let projectDataId = params.id;
        return api.processProjectData(projectDataId)
        .then((resp) => dispatch('addUserTask', resp.data.task))
        .then(() => {
            if (getters.projectHierarchyFlat.some((data) => data.id === projectDataId)) {
                return dispatch('loadProjectHierarchy');
            }
        })
        .then(() => {
            if (projectDataId === getters.projectDataDetails.id) {
                return dispatch('loadProjectData', projectDataId);
            }
        })
        .catch((error) => {
            dispatch('displayNotification', { content: 'Error processing the project data' });
            throw error;
        });
    },
    createProjectDataInput: ({ dispatch }, params) => {
        return api.createProjectDataInput(params.id, params.data)
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error creating an input for the project data' });
            throw error;
        });
    },
    updateProjectDataModel: ({ dispatch }, params) => {
        if (params.modelId == undefined) {
            return api.updateProjectDataActiveModel(params.id, params.data)
            .catch((error) => {
                dispatch('displayNotification', { content: error.message || 'Error updating the model of the project data' });
                throw error;
            });
        } else {
            return api.updateProjectDataModel(params.id, params.modelId, params.data)
            .catch((error) => {
                dispatch('displayNotification', { content: error.message || 'Error updating the preference model' });
                throw error;
            });
        }
    },
    loadInputData: ({ commit, dispatch }, params) => {
        params = Object.assign({ fields: ['id', 'name', 'attributes', 'data_type'] }, params);
        return new Promise((resolve, reject) => {
            if (params.ids !== undefined && params.ids.length > 0) {
                api.getProjectDataList(params.ids, params.fields).then((resp) => {
                    let inputList = resp.data.project_data;
                    let inputData = {};
                    for (let data of inputList) {
                        inputData[data.id] = data;
                    }
                    commit(types.SET_PROJECT_DATA_INPUT, inputData);
                    resolve();
                }, (resp) => {
                    dispatch('displayNotification', { content: resp.data.message || 'Error loading the input data' });
                    reject();
                });
            } else {
                commit(types.SET_PROJECT_DATA_INPUT, []);
                resolve();
            }
        });
    }
};

const projectData = {
    state,
    getters,
    mutations,
    actions
};

export default projectData;