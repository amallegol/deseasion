import * as types from '../types';
import api from '../../api';

const state = {
    user: {},
    jwtToken: undefined,
    refreshToken: undefined,
    userInvalidCredentials: false
};

const getters = {
    userDetails: state => state.user,
    jwtToken: state => state.jwtToken,
    refreshToken: state => state.refreshToken,
    userAuthenticated: state => {
        return typeof state.jwtToken === 'string';
    },
    userInvalidCredentials: state => state.userInvalidCredentials
};

const mutations = {
    [types.SET_USER_DATA] (state, data) {
        state.user = data;
        localStorage.setItem('userData', JSON.stringify(data));
    },
    [types.SET_JWT_TOKEN] (state, token) {
        state.jwtToken = token;
        if (typeof token === 'string') {
            localStorage.setItem('jwtToken', token);
        } else {
            localStorage.removeItem('jwtToken');
        }
    },
    [types.SET_REFRESH_TOKEN] (state, token) {
        state.refreshToken = token;
        if (typeof token === 'string') {
            localStorage.setItem('refreshToken', token);
        } else {
            localStorage.removeItem('refreshToken');
        }
    },
    [types.LOAD_LOCAL_USER_DATA] (state) {
        state.jwtToken = localStorage.getItem('jwtToken');
        state.refreshToken = localStorage.getItem('refreshToken');
        state.user = JSON.parse(localStorage.getItem('userData'));
    },
    [types.SET_INVALID_CREDENTIALS] (state, value) {
        state.userInvalidCredentials = value;
    }
};

const actions = {
    /**
     * Load the data in local storage from the previous session.
     */
    loadLocalUserData: ({ commit }) => {
        commit(types.LOAD_LOCAL_USER_DATA);
    },
    /**
     * Get the details of the currently connected user.
     */
    loadUserDetails: ({ commit }) => {
        return api.getUserDetails()
        .then((resp) => {
            commit(types.SET_USER_DATA, resp.data.user);
        });
    },
    /**
     * Ask a new authentication token using the refresh token.
     */
    refreshUserToken: ({ commit, dispatch, getters }) => {
        return api.refreshToken(getters.refreshToken)
        .then((resp) => {
            let tokenType = resp.data.token_type;
            let accessToken = resp.data.access_token;
            commit(types.SET_JWT_TOKEN, tokenType + ' ' + accessToken);
        })
        .catch((error) => {
            dispatch('displayNotification', { content: 'Error refreshing the token. Please log in.' });
            commit(types.SET_JWT_TOKEN, null);
            throw error;
        });
    },
    /**
     * Logs the user in using basic credentials.
     * If the logging is successfull, the returns JWT token is saved for the subsequent authentications.
     * @param {Object} payload - the user payload
     * @param {string} payload.username - the username
     * @param {string} payload.password - the password
     * @returns {Promise} Resolves if the logging was successefull, and rejects otherwise
     */
    loginUserBasic: ({ commit, dispatch }, payload) => {
        commit(types.SET_INVALID_CREDENTIALS, false);
        return api.loginBasic(payload.username, payload.password)
        .then((resp) => {
            let token_type = resp.data.token_type;
            let access_token = resp.data.access_token;
            let refreshToken = resp.data.refresh_token;
            commit(types.SET_JWT_TOKEN, token_type + ' ' + access_token);
            commit(types.SET_REFRESH_TOKEN, token_type + ' ' + refreshToken);
            return dispatch('loadUserDetails');
        })
        .catch((error) => {
            commit(types.SET_INVALID_CREDENTIALS, true);
            throw error;
        });
    },
    /**
     * Logs the user out.
     * Deletes the loaded user data and the saved JWT token.
     */
    logoutUser: ({ commit }) => {
        commit(types.SET_USER_DATA, {});
        commit(types.SET_JWT_TOKEN, null);
        commit(types.SET_REFRESH_TOKEN, null);
    }
};

const user = {
    state,
    getters,
    mutations,
    actions
};

export default user;