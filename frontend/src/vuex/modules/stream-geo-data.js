import * as types from '../types';
import api from '../../api';


const state = {
    stream: {},
    streamPermissions: undefined,
    streamVersions: {}
}


const getters = {
    streamDetails: state => state.stream,
    streamPermissions: state => state.streamPermissions,
}


const mutations = {
    [types.SET_STREAM] (state, stream) {
        state.stream = stream;
    },
    [types.SET_STREAM_PERMISSIONS] (state, permissions) {
        state.streamPermissions = permissions;
    },
}


const actions = {
    loadStream: ({ commit, dispatch }, id) => {
        return new Promise((resolve, reject) => {
            api.getStreamDetails(id)
            .then((resp) => {
                let data = resp.data.stream;
                commit(types.SET_STREAM, data);
                resolve();
            })
            .catch((resp) => {
                dispatch('displayNotification', { content: resp.data && resp.message || 'Error loading the stream' });
                reject();
            });
        });
    },
    deleteStream: ({ commit, dispatch, getters }, id) => {
        return api.deleteStream(id)
        .then(() => {
            if (getters.streamDetails && getters.streamDetails.id === id) {
                commit(types.SET_STREAM, {});
            }
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error deleting stream'})
            throw resp;
        });
    },
    setStreamDetails: ({ commit }, data) => {
        commit(types.SET_STREAM, data);
    },
    updateStream: ({ commit, dispatch }, params) => {
        if ('id' in params && 'data' in params) {
            return new Promise((resolve, reject) => {
                api.updateStream(params.id, params.data).then(() => {
                    resolve();
                }, (resp) => {
                    dispatch('displayNotification', { content: resp.data.message || 'Error updating the Stream' });
                    reject(resp.data);
                });
            });
        }
    },
    loadStreamPermissions: ({ commit, dispatch }, streamId) => {
        return api.getStreamPermissions(streamId)
        .then((resp) => {
            commit(types.SET_STREAM_PERMISSIONS, resp.data.access);
        }, (resp) => {
            dispatch('displayNotification', { content: resp.data.message || 'Error loading the stream permissions' });
            throw resp;
        });
    },
    updateStreamPermissions: ({ dispatch }, params) => {
        return api.updateStreamPermissions(params.id, params.data)
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error updating the stream permissions' });
            throw error;
        });
    },
    addStreamVersion: ({ dispatch }, params) => {
        return api.addStreamVersion(params.id, params.data)
        .catch((error) => {
            dispatch('displayNotification', { content: error.message || 'Error creating the stream version' });
            throw error;
        });
    }
}


const streamGeoData = {
    state,
    getters,
    mutations,
    actions
}

export default streamGeoData;