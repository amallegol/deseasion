import * as types from '../types';


const state = {
    lastUIUpdate: null,
    mapVisible: false,  // show or hide the map panel
    mapSize: [600, 600],  // size in pixels
    panelVisible: false,
    panelComponents: {
        dataExplorer: true,
        stats: true,
        layers: true
    },
    backgroundVisible: true,
    projectExtentVisible: false
};

const getters = {
    lastUIUpdate: state => state.lastUIUpdate,
    mapVisible: state => state.mapVisible,
    mapSize: state => state.mapSize,
    mapWidth: state => state.mapSize[0],
    mapWidthPx: state => state.mapVisible ? state.mapSize[0] + 'px' : '0px',
    mapHeight: state => state.mapSize[1],
    mapHeightPx: state => state.mapVisible ? state.mapSize[1] + 'px' : '0px',
    panelVisible: state => state.panelVisible,
    panelComponents: state => state.panelComponents,
    backgroundVisible: state => state.backgroundVisible,
    projectExtentVisible: state => state.projectExtentVisible
};

const mutations = {
    [types.UI_SET_MAP_VISIBILITY] (state, visibility) {
        state.mapVisible = visibility;
        state.lastUIUpdate = Date.now();
    },
    [types.UI_SET_MAP_SIZE] (state, size) {
        state.mapSize = size;
        state.lastUIUpdate = Date.now();
    },
    [types.UI_SET_PANEL_VISIBILITY] (state, visibility) {
        state.panelVisible = visibility;
        state.lastUIUpdate = Date.now();
    },
    [types.UI_SET_PANEL_COMPONENTS] (state, components) {
        state.panelComponents = components;
    },
    [types.UI_SET_BACKGROUND_VISIBILITY] (state, visibility) {
        state.backgroundVisible = visibility;
    },
    [types.UI_SET_PROJECT_EXTENT_VISIBILITY] (state, visibility) {
        state.projectExtentVisible = visibility;
    }
};

const actions = {
    setMapVisibility: ({ commit }, visibility) => {
        commit(types.UI_SET_MAP_VISIBILITY, visibility);
    },
    setMapSize: ({ commit }, size) => {
        commit(types.UI_SET_MAP_SIZE, size);
    },
    setMapWidth: ({ commit, state }, width) => {
        commit(types.UI_SET_MAP_SIZE, [width, state.mapSize[1]]);
    },
    setMapHeight: ({ commit, state }, height) => {
        commit(types.UI_SET_MAP_SIZE, [state.mapSize[0], height]);
    },
    showDataOnMap: ({ dispatch }, payload) => {
        payload = Object.assign({
            geoDataId: null,
            overwrite: true
        }, payload);

        let firstStep = () => payload.overwrite ? dispatch('clearGeoData') : Promise.resolve();

        firstStep()
        .then(() => dispatch('loadGeoData', payload.geoDataId))
        .then(() => dispatch('setMapVisibility', true));
    },
    setPanelVisibility: ({ commit, dispatch, state }, visibility) => {
        commit(types.UI_SET_PANEL_VISIBILITY, visibility);
    },
    setPanelStats: ({ dispatch }, visibility) => {
        dispatch('setPanelComponents', { stats: visibility });
    },
    setPanelComponents: ({ commit, state }, components) => {
        let comps = Object.assign({}, state.panelComponents, components);
        commit(types.UI_SET_PANEL_COMPONENTS, comps);
    },
    setBackgroundVisibility: ({ commit }, visibility) => {
        commit(types.UI_SET_BACKGROUND_VISIBILITY, visibility);
    },
    setProjectExtentVisibility: ({ commit }, visibility) => {
        commit(types.UI_SET_PROJECT_EXTENT_VISIBILITY, visibility);
    }
};

const ui = {
    state,
    getters,
    mutations,
    actions
};

export default ui;