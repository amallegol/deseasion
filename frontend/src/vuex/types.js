export const UI_SET_MAP_VISIBILITY = 'UI_SET_MAP_VISIBILITY';
export const UI_SET_MAP_SIZE = 'UI_SET_MAP_SIZE';
export const UI_SET_SIDEBAR_VISIBILITY = 'UI_SET_SIDEBAR_VISIBILITY';
export const UI_SET_SIDEBAR_WIDTH = 'UI_SET_SIDEBAR_WIDTH';
export const UI_SET_PANEL_VISIBILITY = 'UI_SET_PANEL_VISIBILITY';
export const UI_SET_PANEL_COMPONENTS = 'UI_SET_PANEL_COMPONENTS';
export const UI_SET_BACKGROUND_VISIBILITY = 'UI_SET_BACKGROUND_VISIBILITY';
export const UI_SET_PROJECT_EXTENT_VISIBILITY = 'UI_SET_PROJECT_EXTENT_VISIBILITY';
export const UI_SET_SMALL_SCREEN = 'UI_SET_SMALL_SCREEN';

export const SET_USER_DATA = 'SET_USER_DATA';
export const SET_JWT_TOKEN = 'SET_JWT_TOKEN';
export const SET_REFRESH_TOKEN = 'SET_REFRESH_TOKEN';
export const LOAD_LOCAL_USER_DATA = 'LOAD_LOCAL_USER_DATA';
export const SET_INVALID_CREDENTIALS = 'SET_INVALID_CREDENTIALS';

export const ADD_NOTIFICATION = 'ADD_NOTIFICATION';
export const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION';

export const MODAL_SHOW = 'MODAL_SHOW';
export const MODAL_SET_DATA = 'MODAL_SET_DATA';
export const MODAL_SET_TEMPLATE = 'MODAL_SET_TEMPLATE';
export const MODAL_SET_RESOLVE_CB = 'MODAL_SET_RESOLVE_CB';
export const MODAL_SET_REJECT_CB = 'MODAL_SET_REJECT_CB';

export const SET_PROJECT= 'SET_PROJECT';
export const SET_PROJECT_HIERARCHY = 'SET_PROJECT_HIERARCHY';
export const SET_PROJECT_SHARED_DATA = 'SET_PROJECT_SHARED_DATA';
export const SET_PROJECT_PERMISSIONS = 'SET_PROJECT_PERMISSIONS';
export const SET_PROJECT_TEMPLATES = 'SET_PROJECT_TEMPLATES';

export const SET_PROJECT_DATA = 'SET_PROJECT_DATA';
export const SET_PROJECT_DATA_INPUT = 'SET_PROJECT_DATA_INPUT';

export const ADD_GLOBAL_DATA = 'ADD_GLOBAL_DATA';
export const REMOVE_GLOBAL_DATA = 'REMOVE_GLOBAL_DATA';
export const SET_GLOBAL_DATA_ORDER = 'SET_GLOBAL_DATA_ORDER';
export const CLEAR_GLOBAL_DATA = 'CLEAR_GLOBAL_DATA';
export const SET_GLOBAL_DATA_PERMISSIONS = 'SET_GLOBAL_DATA_PERMISSIONS';

export const ADD_GEO_DATA = 'ADD_GEO_DATA';
export const REMOVE_GEO_DATA = 'REMOVE_GEO_DATA';
export const SET_GEO_DATA_ORDER = 'SET_GEO_DATA_ORDER';
export const SET_GEO_DATA_PARAMETERS = 'SET_GEO_DATA_PARAMETERS';
export const CLEAR_GEO_DATA = 'CLEAR_GEO_DATA';
export const SET_GEO_DATA_FEATURES = 'SET_GEO_DATA_FEATURES';
export const SET_GEO_DATA_PERMISSIONS = 'SET_GEO_DATA_PERMISSIONS';
export const SET_FEATURE = 'SET_FEATURE';

export const SET_USER_TASK = 'SET_USER_TASK';
export const REMOVE_USER_TASK = 'REMOVE_USER_TASK';
export const SET_TASK_TIMEOUT_ID = 'SET_TASK_TIMEOUT_ID';
export const SET_TASK_TIMEOUT_DATE = 'SET_TASK_TIMEOUT_DATE';

export const SET_STREAM = 'SET_STREAM';
export const SET_STREAM_PERMISSIONS = 'SET_STREAM_PERMISSIONS';