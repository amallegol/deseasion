import { createStore } from 'vuex';

import ui from './modules/ui';
import user from './modules/user';
import notifier from './modules/notifier';
import modal from './modules/modal';
import project from './modules/project';
import projectData from './modules/project-data.js';
import globalData from './modules/global-data';
import geoData from './modules/geo-data';
import streamGeoData from './modules/stream-geo-data';
import tasks from './modules/tasks.js';

const store = createStore({
    modules: {
        ui,
        user,
        notifier,
        modal,
        project,
        projectData,
        globalData,
        geoData,
        streamGeoData,
        tasks
    },
    //TODO: Set strict mode only in development build: https://vuex.vuejs.org/guide/strict.html#development-vs-production
    strict: true
});

export default store;