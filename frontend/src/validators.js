/**
 * Verify that the value is a valid python identifier, and that it is not in the reserved names
 */
export const reservedWords = [
    'and', 'del', 'from', 'not', 'while', 'as', 'elif', 'global', 'or', 'with',
    'assert', 'else', 'if', 'pass', 'yield', 'break', 'except', 'import',
    'print', 'class', 'exec', 'in', 'raise', 'continue', 'finally', 'is',
    'return', 'def', 'for', 'lambda', 'try', 'True', 'False', 'None'
];

export function isValidName(value) {
    let pattern = /^[a-zA-Z][0-9a-zA-Z_]*$/;
    return (pattern.test(value) && reservedWords.indexOf(value) === -1);
}
