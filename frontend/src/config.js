/**
 * Load the application configuration
 * The config module is loaded if it exists and override the default configuration options
 */
let configDefault = {
    APPLICATION_TITLE: 'DESEASION',
    API_ROOT: '/api',
    BASIC_AUTHENTICATION_HEADER: 'Authorization',
    JWT_AUTHENTICATION_HEADER: 'Authorization'
};
let configUser;
var app_config;
await fetch('@/../instance/config.json')
    .then((response) => response.json())
    .then((json) => configUser = json )
    .catch((e) => configUser = {})
    .finally(() => app_config = Object.assign(configDefault, configUser))

// Modify the title of the webpage
document.title = app_config.APPLICATION_TITLE;

export default app_config;
