import {createRouter, createWebHistory} from 'vue-router';

import store from './vuex/store.js';

import Home from './components/Home.vue';
import Login from './components/Login.vue';
import Logout from './components/Logout.vue';
import MainApp from './components/MainApp.vue';
import ProjectManager from './components/Project/ProjectManager.vue';
import Project from './components/Project/index.vue';
import ProjectDataManager from './components/ProjectDataManager/index.vue';
import ProjectApp from './components/ProjectApp.vue';
import GlobalDataManager from './components/GlobalData/GlobalDataManager.vue';
import GeoDataManager from './components/GeoData/GeoDataManager.vue';
import StreamManager from './components/Stream/StreamManager.vue';
import GeoDataViewer from './components/GeoDataViewer.vue';
import ProjectData from './components/ProjectData/index.vue';
import ProjectTaskManager from './components/ProjectTaskManager.vue';
import ProjectRecommendations from './components/ProjectRecommendations/index.vue';
import ShareManager from './components/ProjectDataShare/manager.vue';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', name: 'home', component: Home },
        { path: '/login', name: 'login', component: Login },
        { path: '/logout', name: 'logout', component: Logout },
        { path: '/', component: MainApp, children: [
            { path: '/geo-data/view/:id', name: 'geodataView', component: GeoDataViewer },
            { path: '/geo-data-manager', name: 'geodataManager', component: GeoDataManager },
            { path: '/stream-manager', name: 'streamManager', component: StreamManager },
            { path: '/global-data-manager', name: 'globaldataManager', component: GlobalDataManager },
            { path: '/project-manager', name: 'projectsManager', component: ProjectManager },
            { path: '/project/:projectId', component: ProjectApp, children: [
                { path: 'details', name: 'projectDetails', component: Project },
                { path: 'data', name: 'projectData', component: ProjectDataManager },
                { path: 'project-data/:dataId', name: 'projectDataDetails', component: ProjectData },
                { path: 'task-manager', name: 'projectTaskManager', component: ProjectTaskManager },
                { path: 'objective', name: 'projectRecommendations', component: ProjectRecommendations },
                { path: 'share', name: 'dataShares', component: ShareManager },
                { path: '/', redirect: to => {
                    return { name: 'projectDetails', query: { q: to.params.projectId } }
                }}
            ]},
            { path: '/', redirect: { name: 'home' }}
        ]}
    ],
    linkActiveClass: 'active'
});

/**
 * Update the UI on the routes transitions.
 * Can be viewed as a FSM for the UI, with the states being the different routes
 */
function updateUIState(to, from) {
    if (to.name === 'projectDataDetails') {
        store.dispatch('setMapVisibility', true);
        store.dispatch('setPanelVisibility', true);
        store.dispatch('setPanelComponents', { dataExplorer: true, stats: true, explainability: true });
    } else if (to.name === 'geodataManager') {
        store.dispatch('setMapVisibility', false);
        store.dispatch('setPanelVisibility', true);
        store.dispatch('setPanelComponents', { dataExplorer: false, stats: true, explainability: false });
    } else if (to.name === 'projectData') {
        store.dispatch('setMapVisibility', false);
        store.dispatch('setPanelVisibility', true);
        store.dispatch('setPanelComponents', { dataExplorer: false, stats: true, explainability: true });
    } else if (to.name === 'projectRecommendations') {
        store.dispatch('setMapVisibility', true);
        store.dispatch('setPanelVisibility', false);
    } else {
        store.dispatch('setMapVisibility', false);
        store.dispatch('setPanelVisibility', false);
    }
}

router.beforeEach((to, from, next) => {
    if (to.name !== null) {
        // if the route exists, update the UI state and continue
        updateUIState(to, from);
        next();
    } else {
        // if the route doesn't exist, stay on the previous route
        next(false);
    }
});

export default router
