import 'ol/ol.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';
import '@fortawesome/fontawesome-free/css/solid.css';
import '@fortawesome/fontawesome-free/css/fontawesome.css';

import './css/app.css';
import router from './routes.js';
import { createApp } from 'vue'
import store from './vuex/store.js';
import App from './App.vue';
import setAxios from './api/utils';

setAxios()
const app = createApp(App)
app.use(router)
app.use(store)
app.mount('#app')
