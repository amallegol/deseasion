import { get as getProj } from 'ol/proj';
import chroma from 'chroma-js';
import axios from 'axios'


/**
 * Return the color for the data.
 *
 * @param {Object} params
 * @param {} value
 */
export function getColor(params, value) {
    let color = '#e5f5f9';
    if (params.color.type === 'color') {
        color = params.color.base;
    } else if (params.color.type === 'scale') {
        let c = chroma(params.color.base);
        let values = [c.brighten(2), c.darken(2)];
        let scale = chroma.scale(values);
        if (params.color.domain) {
            scale = scale.domain(params.color.domain);
        }
        color = scale(value);
    } else if (params.color.type === 'map') {
        let map = new Map(params.color.values);
        color = map.get(value);
    }
    return chroma(color).alpha(params.opacity).css();
}


export function tileLoadFunction(tile, url) {
    const loader = (extent, resolution, projection) => {
        let format = tile.getFormat();
        axios.get(url, { responseType: 'blob' })
        .then((resp) => {
            let reader = new FileReader();
            reader.addEventListener('loadend', () => {
                let data = reader.result;
                tile.setProjection(format.readProjection(data));
                let features = format.readFeatures(data, {
                    featureProjection: getProj('EPSG:3857')
                });
                tile.setFeatures(features);
                tile.setExtent(format.getLastExtent());
            });
            reader.readAsArrayBuffer(resp.data);
        });
    };
    tile.setLoader(loader);
}


export function getMapExtent(geoDataExtents, projectExtent) {
    let extent = [Infinity, Infinity, -Infinity, -Infinity];
    for (let dExtent of geoDataExtents) {
        if (dExtent[0] < extent[0]) { extent[0] = dExtent[0]; }
        if (dExtent[1] < extent[1]) { extent[1] = dExtent[1]; }
        if (dExtent[2] > extent[2]) { extent[2] = dExtent[2]; }
        if (dExtent[3] > extent[3]) { extent[3] = dExtent[3]; }
    }
    if (projectExtent) {
        if (projectExtent[0] > extent[0]) { extent[0] = projectExtent[0]; }
        if (projectExtent[1] > extent[1]) { extent[1] = projectExtent[1]; }
        if (projectExtent[2] < extent[2]) { extent[2] = projectExtent[2]; }
        if (projectExtent[3] < extent[3]) { extent[3] = projectExtent[3]; }
    }
    return extent;
}
