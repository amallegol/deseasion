export function useFormatDataType(dataType) {
    if (dataType === 'geo_data') {
        return 'GeoData';
    } else if (dataType === 'generator') {
        return 'Generator';
    } else if (dataType === 'global_data') {
        return 'GlobalData'
    } else {
        return 'Unknown';
    }
}

export function useFormatPrefType(prefType) {
    if (prefType === 'categories_rule') {
        return 'Categories';
    } else if (prefType === 'continuous_rule') {
        return 'Rule';
    } else if (prefType === 'geo_buffer') {
        return 'Buffer';
    } else if (prefType === 'mrsort') {
        return 'MR-Sort';
    } else if (prefType === 'weighted_sum') {
        return 'Weighted Sum';
    } else {
        return 'Unknown';
    }
}
