========
REST API
========

Reference
=========

.. qrefflask:: api:create_app('config.py')


Documentation
=============

.. autoflask:: api:create_app('config.py')
    :undoc-static:

OpenAPI Specification
=====================

.. literalinclude:: spec.yml
   :language: yaml
