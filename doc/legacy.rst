Legacy documentation
====================

This part of the documentation contains legacy doc for older versions of the application.
It is kept here before being updated and integrated in the regular doc.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   legacy/*