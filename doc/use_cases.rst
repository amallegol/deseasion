Modèles UML
===========

Modèle de classes
-----------------

.. todo:: update

.. image:: images/build/uml.png


Modèle de base de données
-------------------------

.. todo:: update

.. image:: images/build/database.png


Use cases
=========

.. todo:: update

.. image:: images/build/use_cases.png


``UcLogIn``
-----------

Contexte de déclenchement:

    Use-case déclenché quand l'utilisateur veut se connecter dans l'application.

Rôles:

    Aucun

Résumé:

    Use-case déclenché quand l'utilisateur se connecte à l'application. L'utilisateur rentre son nom d'utilisateur et son mot de passe, et l'application l'authentifie et lui renvoie un token lui permettant d'être reconnu par l'API.

Pré-conditions:

    L'utilisateur n'est pas connecté.

Description:

    Une API Rest doit être stateless, toutes les informations d'authentification doivent donc être transmises à chaque requête à l'API.

    Lorsque l'utilisateur se connecte, le serveur d'application doit lui fournir un token d'authentification, qui sera transmis à chaque nouvelle requête, permettant de vérifier les droits de l'utilisateur.

    Ce token, défini selon le standard JWT, contient les informations de l'utilisateur et une date d'expiration, et est signé avant d'être transmis à l'utilisateur.

    Le token ne peut pas être révoqué et ne doit donc pas avoir une date d'expiration trop lointaine.

Post-condition:

    L'utilisateur est connecté si l'authentification s'est bien déroulée.

Scénario nominal:

    1. L'utilisateur renseigne son nom d'utilisateur et son mot de passe dans les champs prévus
    2. L'application valide les informations, et renvoie un token d'authentification à l'utilisateur, pour l'authentifier lors des requêtes suivantes
    3. L'utilisateur est redirigé sur la page d'accueil, et l'interface affiche de nouveaux menus accessibles uniquement pour les utilisateurs connectés

Exceptions:

    2a. Les identifiants sont incorrects

        1. L'utilisateur renseigne son nom d'utilisateur et son mot de passe dans les champs prévus
        #. Les informations ne sont pas validées par le serveur et l'utilisateur reçoit un message d'erreur
        #. Un message s'affiche, signalant que les identifiants sont incorrects




``UcCreateNewProject``
----------------------

Contexte de déclenchement:

    L'utilisateur veut créer un nouveau projet.

Rôles:

    Utilisateur

Résumé:

    L'utilisateur crée un nouveau projet

Pré-conditions:

    L'utilisateur est connecté.

Description:

    Chaque projet a un utilisateur associé, qui est gestionnaire du projet. Ce gestionnaire est l'utilisateur ayant créé le projet.

    Le nom du projet et sa description sont définis lors de la création du projet.

Post-condition:

    Un nouveau projet est créé.

Scénario nominal:

    1. L'utilisateur accède à la page de création d'un nouveau projet
    2. L'utilisateur renseigne le nom et la description du projet à créer
    3. L'utilisateur valide la création du projet
    4. Le système crée un nouveau projet lié à l'utilisateur, et portant le nom et la description spécifiés
    5. L'utilisateur est redirigé vers la page de gestion des projets

Exceptions:

    3a. L'utilisateur annule la création du projet

        1. L'utilisateur est redirigé vers la page de gestion des projets




``UcUploadShapefiles``
----------------------

Contexte de déclenchement:

    L'utilisateur veut charger des fichiers shapefile dans l'application.

Rôles:

    Utilisateur

Résumé:

    L'utilisateur téléverse des fichiers shapefile sur l'application.

Pré-condition:

    L'utilisateur est connecté.

Description:

    L'application permet de charger des données géographiques au format shapefile.

    Le format shapefile nécessite au moins 3 fichiers différents pour représenter une donnée, avec les extensions ``.shp`` (fichier principal), ``.shx`` (fichier d'index) et ``.dbf`` (fichier d'attributs). Des fichiers supplémentaires peuvent être également fournis, par exemple un fichier ``.prj`` décrivant le système de projection au format WKT.

    Chaque utilisateur peut téléverser des données shapefile sans restriction. Les données téléversées seront visibles par tous les autres utilisateur, et pourront être utilisées dans d'autres parties de l'application.

Post-condition:

    Une nouvelle donnée géographique est disponible dans l'application.

Scénario nominal

    1. L'utilisateur accède à la page de téléversement des fichiers
    2. L'utilisateur sélectionne les fichiers de son shapefile sur son disque
    3. L'utilisateur valide l'envoie des fichiers
    4. L'application lit la liste des fichiers et les charge
    5. L'application reprojette les données en WGS84 
    6. L'application enregistre les données en base de données
    7. L'utilisateur est redirigée vers la page listant les données géographiques

Scénarios alternatifs:

    5a. La projection n'est pas connue (pas de fichier de projection fourni)

        1. Le chargement continue en considérant que les données sont projetées en WGS84

Exceptions:

    4a. Il n'y a pas de fichier avec l'extension .shp dans la liste

        1. Le chargement est arrêté
        #. Un message d'erreur est affiché à l'utilisateur pour l'avertir qu'il y a des fichiers manquants

    4b. Plusieurs fichiers avec l'extension .shp sont reconnus

        1. Le chargement est arrêté
        #. Un message est affiché à l'utilisateur, pour lui dire de charger les fichiers un par un

    6a. Les données ne sont pas valides (ex: données 3D)

        1. Le chargement est arrêté
        #. Un message d'erreur est affiché à l'utilisateur lui disant que les données ne peuvent pas être chargées




``UcDeleteExistingGeoData``
---------------------------

Contexte de déclenchement:

    L'utilisateur veut supprimer une donnée géographique de l'application.

Rôles:

    Utilisateur

Résumé:

    L'utilisateur supprime une donnée géographique de l'application.

Pré-condition:

    L'utilisateur est connecté

Description:

    N'importe quel utilisateur peut supprimer une donnée géographique (``GeoData``) enregistrée dans le système.

    L'application doit valider la suppression avant d'effectivement supprimer une donnée.

    Cette validation échoue si les données sont déjà utilisées dans d'autres parties de l'application (par ex. si un autre utlisateur a ajouté cette donnée dans un modèle, voir ``UcAddModelGeoData``). Cette donnée là ne pourra donc pas être supprimée.

Scénarion nominal:

    1. L'utilisateur choisi la donnée à supprimer
    2. L'utilisateur supprime la donnée
    3. L'application autorise la suppression de la donnée
    4. La donnée est supprimée

Exceptions:

    3a. L'application n'autorise pas la suppression de la donnée (la donnée géographique est référencée par d'autres objets de l'application, par ex. dans un modèle)

        1. Un message d'erreur est affiché à l'utilisateur, lui disant que la donnée est déjà utilisée et ne peut pas être supprimée



``UcViewGeodataDetailsMap``
---------------------------

Contexte de déclenchement:

    L'utilisateur veut visualiser une donnée géographique de l'application.

Rôles:

    Aucun

Résumé:

    L'utilisateur visualise une donnée géographique de l'application sur un fond de carte.

Pré-condition:

    L'utilisateur est connecté

Description:

    L'utilisateur peut visualiser les données géographiques chargées dans l'application sur un fond de carte.

Scénario nominal:

    1. L'utilisateur accède à la page de visualisation d'une donnée géographique
    2. L'application lui renvoie les géométries des données et les affiche sur un fond de carte OSM



``UcAddModelGeoData``
---------------------

Contexte de déclenchement:

    L'utilisateur veut ajouter une donnée géospatiale dans un modèle

Rôles:

    Utilisateur

Résumé:

    L'utilisateur ajoute un attribut d'une donnée géospatiale déjà chargée dans l'application dans un modèle.

Pré-condition:

    L'utilisateur est connecté

    L'utilisateur est sur la page d'un modèle existant

Description:

    L'utilisateur peut ajouter des données en entrée d'un modèle.

    Ces données proviennent des attributs d'une donnée géospatiale.

    Les données ne sont pas référencées directement mais au travers d'un objet intermédiaire (``ModelGeoData``), qui permet d'ajouter des méta-données supplémentaires.

Scénario nominal:

    1. L'utilisateur est sur la page des détails d'un modèle
    2. L'utilisateur sélectionne une donnée géospatiale dans l'application
    3. L'utilisateur valide son choix
    4. L'utilisateur sélectionne l'attribut de la donnée géospatiale
    5. L'utilisateur valide son choix
    6. L'application associe l'attribut de la donnée géospatiale au modèle avec des meta-données
    7. L'utilisateur est de retour sur la page des détails du modèle



``UcAddCategoryToModel``
------------------------

Contexte de déclenchement:

    L'utilisateur veut ajouter une catégorie dans un modèle

Rôles:

    Utilisateur

Résumé:

    L'utilisateur ajoute une nouvelle catégorie dans un modèle

Pré-condition:

    L'utilisateur est connecté

    L'utilisateur est sur la page d'un modèle existant

Description:

    L'utilisateur peut ajouter des catégories, qui correspondent aux différentes valeurs possible sur la carte de décision d'un modèle.

    Une catégorie est une chaîne de caractères, et l'ordre des catégories dans un modèle représente la direction de préférence des catégories (la première est moins bonne que la dernière).

Scénario nominal:

    1. L'utilisateur est sur la page des détails d'un modèle
    2. L'utilisateur ajoute une nouvelle catégorie en précisant son nom
    3. L'application ajoute cette catégorie à la fin de la liste des catégories
    4. L'utilisateur est de retour sur la page de détail du modèle, avec la liste des catégories à jour

Exceptions:

    2a. Le nom est déjà utilisé pour une catégorie dans ce modèle

        1. L'application n'ajoute pas de catégorie
        2. Un message d'erreur est affiché pour indiquer que le nom est déjà utilisé



``UcEditCriterionRule``
-----------------------

Contexte de déclenchement:

    L'utilisateur veut modifier la règle d'un critère

Rôles:

    Utilisateur

Résumé :

    L'utilisateur édite la règle d'un critère, écrite en Python.

Pré-conditions :

    L'utilisateur est connecté

    L'utilisateur est sur la page d'un modèle existant

    Un critère a déjà été créé dans le modèle

Description :

    L'utilisateur peut modifier la règle d'un critère pour choisir quelles valeurs aura la donnée générée par ce critère.

    La règle est décrite en écrivant du code en Python. Les données d'entrées sont des objets Python correspondant aux ``ModelGeoData`` et aux ``IntermediateData`` du modèle, dont les attributs sont accessibles à travers les propriétés des objets (ex : si une donnée "Bathymetrie" est enregistrée dans le modèle, l'utilisateur peut écrire ``Bathymetrie.Origine`` dans la règle du critère pour accéder à l'attribut "Origine" de la donnée "Bathymetrie").

Scénario nominal :

    1. L'utilisateur est sur la page des détails d'un modèle
    2. L'utilisateur voit la liste des critères de ce modèle
    3. L'utilisateur choisi d'éditer la règle d'un critère
    4. Une fenêtre s'ouvre, avec un champ texte permettant de modifier la règle existante
    5. L'utilisateur écrit du code Python pour éditer la règle
    6. L'utlisateur enregistre les modifications


``UcProcessCriterion``
----------------------

Contexte de déclenchement:

    L'utilisateur veut exécuter les règles d'un critère.

Rôles:

    Utilisateur

Résumé:

    L'utilisateur exécute les règles d'un critère.

Pré-conditions:

    Un critère avec une règle définie existe dans un modèle

Description:

    L'utilisateur peut lancer l'exécution des règles d'un critère pour construire la carte de ce critère.

    Ce processus peut avoir une durée relativement longue (plusieurs secondes), un message s'affiche à la fin de l'exécution indiquant qu'elle s'est bien déroulée.

    Si une erreur survient, un message d'erreur détaillé est affiché à l'utilisateur.

Scénario nominal:

    1. L'utilisateur est sur la page des détails d'un modèle
    2. L'utilisateur voit la liste des critères dans ce modèle
    3. L'utilisateur choisi de lancer l'exécution des règles d'un critère
    4. L'application crée une nouvelle donnée géographique à partir des règles d'un modèle
    5. Un message de succès s'affiche à la fin de l'exécution

Exceptions:

    4a. Une erreur survient lors de l'éxecution des règles (ex: erreur dans la définition des règles en python)

        1. Un message d'erreur détaillé est affiché à l'utilisateur
