===========
Development
===========

For development, we have configured development containers as
docker compose services. You can launch them in the background, then attach to them
to open files, run tests or else from your IDE.

.. warning::
    VS Code devcontainer.json configurations are not working though.
    Development containers need to be launched manually for now.

.. todo:: Reorganize development doc to have devcontainer and global setup sections?

We have added a pre-commit hook to make sure following tests pas before validating a commit:

- backend linters: `make backend-lint`
- doc build: `make doc`

Also the Gitlab CI/CD pipeline publish the following:

- documentation

You can activate it by copying it in the `.git/hooks/` directory:

.. code:: bash

    cp pre-commit ./git/hooks/.

.. note::
    Even if it is bad practice, you can bypass the pre-commit hook with the following
    commit command: `git commit --no-verify`

Whole stack
===========

.. todo:: Make web container a dev one as well

A docker compose configuration is available at `.devcontainer`.
For now, it can only be launched by going to that directory and running docker compose:

.. code:: bash

    docker compose up -d

It will run the whole stack in development mode, binding the code to the development containers.
For the moment, only the `backend` and `worker` containers are run this way.

The `backend` container runs `uwsgi` with automatic reload on source code changes.
So does the `worker` container with the `celery` worker (using `watchdog``).

.. note::
    The `uwsgi` application in the `backend` run "as is", you will need to supply it with a working
    `instance/config.py`. The repository supplies one as an example in `instance-example/config.py` that is working with the dev containers.
    You will also need to create the PostgreSQL tables and build the sandbox docker image
    (if you need to test it) yourself. See `backend-entrypoint.sh` for a complete
    list of installation commands run by the production backend on startup. You can use the command
    :code:`docker compose exec backend /bin/bash` to open a terminal on the development `backend`.

It uses a different docker image than the production one, and uses a non-root user called "non-root"
to be mapped with the local user when mounting workspace in the container (with UID 1000).

If your local user is not the UID 1000, you will need to customize the `docker-compose.yml` file.
To that end simply modify:

- the USER_UID build argument in the devcontainer docker compose file (in the 3 containers that have it)

Backend
=======

A docker compose configuration is available at `backend/.devcontainer`.
For now, it can only be launched by going to that directory and running docker compose:

.. code:: bash

    docker compose up -d

It will run only the backend software in its development state, binding the api code
with the code present in your local workspace.
The `backend` container is the main one and simply launches the backend with flask and serves it
on port 5000.

.. note::
    The flask application in the `backend` run "as is", you will need to supply it with a working
    `instance/config.py`. The repository supplies one as an example in `instance-example/config.py` that is working with the dev containers.
    You will also need to create the PostgreSQL tables and
    build the sandbox docker image (if you need to test it) yourself. See `backend-entrypoint.sh` for a complete
    list of installation commands run by the production backend on startup. You can use the command
    :code:`docker compose exec backend /bin/bash` to open a terminal on the development `backend`.

.. warning::
    The flask application serves the backend directly on / (no /api prefix), and on port 5000.

.. warning::
    The `flask` command running on the `backend` container will relaod when detecting changes to
    the source code. The celery `worker` will reload too (thanks to the `watchdog` wrapping it).

It uses a different docker image than the production one, and uses a non-root user called "non-root"
to be mapped with the local user when mounting workspace in the container (with UID 1000).

If your local user is not the UID 1000, you will need to customize the `docker-compose.yml` file.
To that end simply modify:

- the USER_UID build argument in the devcontainer docker compose file (in the 3 containers that have it)

Frontend
========

.. todo:: Create and document
