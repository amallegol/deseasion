================
Proposed Changes
================

This page contains all change proposals for the DESEASION web application.
If some changes can be patches, others are breaking and won't be deployed before
a future release.

Database Changes
================

DataAttributeOrdinal: change `order` field from JSON to ARRAY (since it is used as a list)

API Changes
===========

Pagination
----------

One API endpoints use pagination: /project/{project_id}/tasks
This should be a general practice throughout the package on all GET endpoints
returning a list of resources.

Idea: Add two query parameters offset and limit, and specify default values.

End-points
----------

Idea: Get as Restful as possible

- Rename GET /user -> /users/me
- Add GET /user/{id} (get one user)
- Merge /project into /projects
- Rename /template into /templates
- Rename /task into /tasks
- Rename /zone-proposition into /zone-propositions

Issues:

- POST /project-data/{data_id}/input-data ask for `data_type` in request body, even though it only accepts data generator

Responses
---------

Most responses are single item dictionary containing then the resource.
This forces the definition of another schema nesting the resource, which is
redundant.
We could remove that useless nesting, and maybe generalize a type field saying
what type of data is returned.

Schemas
-------

Multiple schemas per resource
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are too many schemas/variants per endpoint of each resource type.
We could simplify by generalizing only 4 schemas (at most) per resource type:
- the complete data (e.g. for GET methods)
- creation schema (e.g. for POST/PUT methods)
- an update schema (e.g. for PATCH methods)
- a nestable schema with less fields (and no nested subfields)

Convert relationships
~~~~~~~~~~~~~~~~~~~~~

For the moment we have a lot of ways to convert relationships:

- nested object with all fields
- nested object with partial fields
- plucked field with only id
- id field created as a plucked field (`{relationship_field}_id`)
- no field at all

It would be a good idea to be more coherent about this.
Also to make the API more traversable, all references should be put in the schemas
(at least the first order ones).

Ideal cases
~~~~~~~~~~~

* One-To-One (association): A --- B

Both schemas should contain a '_id'/pluck field for the other (at least for write)
Both schemas could contain a read-only nested field for the other

Or both schemas contain only one pluck field for the other

* One-To-One (composition): A --> B

A schema should contain a nested field for B with every fields but 'id'
B schema should not contain A

* One-To-Many (association): A --* B

Same case as One-To-One association (but with `many=True` for the field in A)

* One-To-Many (composition): A -->* B

Same case as One-To-One composition (but with `many=True` for the field in A)

Technological stack replacement
-------------------------------

To ease the development, maintenance, documentation of the API, we could swap
the flask/marshmallow/marshmallow_sqlalchemy/sqlalchemy to a simpler
fastapi/sqlmodel(pydantic, sqlalchemy) stack.