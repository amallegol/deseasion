============
Installation
============

.. role:: bash(code)
    :language: bash

This web application is packaged using docker compose.
All the following instruction consider you have a working docker/docker compose
installation on your system.
If you need any help setting up docker, please visit the
`docker doc <https://docs.docker.com/engine/install/>`_.

Software Architecture
=====================

As previously mentioned, this web application is packaged using docker compose.
Each component is split in its own docker container:

* :ref:`web <installation:web>`: contains the reverse proxy nginx and the frontend code
* :ref:`backend <installation:backend>`: contains a flask REST API backend served through nginx from web container
* :ref:`database <installation:database>`: contains the postgresql database with postgis extension (for geometry)
* :ref:`redis <installation:redis>`: contains the redis task queue message broker (used by backend to delegate long tasks)
* :ref:`worker <installation:worker>`: contains one celery worker processing tasks (some directly, some through rootless docker-in-docker)
* :ref:`docker <installation:docker>`: contains the rootless docker-in-docker daemon used to run unsafe code of backend-delegated long tasks

This application also needs one transient container, which is only spawned when
starting the complete app and stops after running once. This container (named
`change-vol-ownership`` in the example `docker-compose.yml` file) is necessary to make docker
certificates available to both worker and docker containers.

Those containers should be in the same network so they can communicate.

web
---

It can be configured by modifying the `nginx.conf` file before building the application.
Or by modifying its final location in the container `/etc/nginx/conf.d/default.conf`.

Otherwise, this container only need to expose the 80 port, and be on the same network than the
:ref:`backend <installation:backend>` container.

backend
-------

It can be configured in two ways.
Either you provide it with an `instance` folder with well-formatted `config.py` and `config.json`
files, or you provide some environment variables to the container so it can build
the configuration files for you:

* `DB_URI`: URI for the postgresql database
* `CELERY_BROKER_URL`: celery broker endpoint
* `API_TOKEN`: token used to salt the JWT tokens (generated if not set)
* `PROFILE_DIR`: profiler directory (set as `./profiler` if not set)
* `CELERY_RESULT_BACKEND`: celery result backend endpoint (equal to `CELERY_BROKER_URL` if not set)

If you want, you can configure the backend locally by running the script `configure-backend.sh`
with the environment variables set. You then need to mount this volume in the container.
Be careful to set variables for the config intended instance deployment (docker or local).

Other environment variables are necessary for the workings of this container:

* `DOCKER_HOST`: URL for the rootless docker-in-docker daemon
* `DOCKER_CERT_PATH`: client certificates directory necessary for communication with the docker-in-docker daemon
* `DOCKER_TLS_VERIFY`: whether or not to check for TLS communication with docker-in-docker daemon (should be 1)

It needs a read access to the :ref:`docker <installation:docker>` certificates
directory (the one set in `DOCKER_CERT_PATH`). This can be accomplished by a volume
mounted as read-only.
If you want to persist the backend instance configuration, you should mount the container `/app/instance`
directory as a volume.

When starting this container, it will perform the following tasks before launching the backend:

* backend configuration using the environment variables (if `instance/config.py` is absent)
* database tables creation (if the database has no alembic version)
* database tables upgrade (if the database is not up-to-date)
* extract openAPI specification from API code (performed at each start)
* pull sandbox docker container image (if it is absent from local registry)

It exposes a uwsgi endpoint on port 3031 (should be routed by the nginx reverse-proxy
from :ref:`web <installation:web>` container in most cases).

database
--------

This container runs a postgresql database with PostGIS extension.
It initially contains only generic default data and will create an empty database
using the following credentials (given as environment variables):

* `POSTGRES_USER`: database user for the application data
* `POSTGRES_PASSWORD`: database password for the application data
* `POSTGRES_DB`: database name for the application data

In the current state of the software, the application data tables are created by
the :ref:`backend <installation:backend>` container upon initialization.

The example `docker-compose.yml` file set up a volume for the database `/var/lib/postgresql/data/` folder
so its container can be destroyed without affecting the data.

redis
-----

This container runs redis as a message broker between the :ref:`backend <installation:backend>`
and the :ref:`worker <installation:worker>` containers.
It is used to delegate long and unsafe tasks to the worker (which may use rootless docker-in-docker
to safely run those) and getting their results.

worker
------

This container runs a celery worker to handle long and/or unsafe tasks sent by the :ref:`backend <installation:backend>`.

Some environment variables are necessary for the workings of this container:

* `DOCKER_HOST`: URL for the rootless docker-in-docker daemon
* `DOCKER_CERT_PATH`: client certificates directory necessary for communication with the docker-in-docker daemon
* `DOCKER_TLS_VERIFY`: whether or not to check for TLS communication with docker-in-docker daemon (should be 1)

It needs a read access to the :ref:`docker <installation:docker>` certificates
directory (the one set in `DOCKER_CERT_PATH`). This can be accomplished by a volume
mounted as read-only.
It also need acess to the :ref:`backend <installation:backend>` configuration folder
(`instance/`). This can be accomplished with a volume binding those directories between the containers.

docker
------

This contains a rootless docker-in-docker daemon waiting for jobs sent by the :ref:`worker <installation:worker>`.
It runs unsafe tasks safely and other long tasks as well in temporary containers
(destroyed after completion).

It needs the following environment variable:

* `DOCKER_TLS_CERTDIR`: certificates directory (should be the parent folder than the client certificates directory shared with
  :ref:`backend <installation:backend>` and :ref:`worker <installation:worker>`).

It exposes its API to port 2376, which should be used by :ref:`worker <installation:worker>` container.

Usage
=====

Install required docker images
------------------------------

You can either build the required docker images from source, or you can simply
use the remote built images. In the former case, you need to clone the repository
and build all docker images within it:

.. code:: bash

   git clone https://gitlab.com/amallegol/deseasion.git
   cd deseasion
   make docker

In the latter case, the images will be pulled from the repository automatically.
Though you can pull them manually if you desire:

.. code:: bash

   docker pull registry.gitlab.com/decide.imt-atlantique/deseasion/frontend:latest
   docker pull registry.gitlab.com/decide.imt-atlantique/deseasion/database:latest
   docker pull registry.gitlab.com/decide.imt-atlantique/deseasion/backend:latest
   docker pull registry.gitlab.com/decide.imt-atlantique/deseasion/worker:latest

.. note::
    You can also directly manually pull all required docker images using docker compose
    (once you set up the `docker-compose.yml` file) using the following: :code:bash:`docker compose pull`

Set up application
------------------

You need to setup your `docker-compose.yml` file.
Here is an example for such file:

.. literalinclude:: docker-compose.yml
   :language: yaml


Start
-----

Then, once you set up your `docker-compose.yml` file, you can start the complete application:

* To start in daemon mode (better for production):

.. code:: bash

    docker compose up -d

* To start in the terminal (will be shut down once the terminal is stopped):

.. code:: bash

    docker compose up

Stop
----

You can stop the application in two way (if launched in daemon-mode).

* To stop the application but keeping the data and configuration:

.. code:: bash

    docker compose down

* To stop the application and removing all data/configuration:

.. code:: bash

    docker compose down --volumes

Monitoring
----------

You can use any of the docker compose commands to help you monitor your application state:

* See running containers of the application and their state:

.. code:: bash

    docker compose ps

* See logs

.. code:: bash

    docker compose logs [SERVICE]

Add user
--------

You can add a user interactively by connecting to the :ref:`backend <installation:backend>` and using the command: :bash:`flask user create`.
You can do it in two commands:

.. code:: bash

    docker compose exec backend /bin/bash
    flask user create

Or in one single command:

.. code:: bash

    docker compose exec -it backend flask user create

Services
========

This software contains multiple services which are exposed through a nginx reverse proxy:

* `/`: web application frontend
* `/api`: backend (REST API)
* `/api/apidocs`: Swagger-UI documentation of the backend
