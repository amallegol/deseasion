Tests
=====


Configuration
-------------

The environment must be configured
for the tests to run properly.

A configuration file must exist in ``instance/test.py``.
The variable ``TESTING`` must be set.

.. code:: python

    DEBUG = True
    TESTING = True

The test database must exist.
It is configured the same way as the application database.

Run the tests using the makefile:

.. code:: bash

    make test
