Development
===========


Upgrade to a new version
------------------------

Execute the following commands to migrate the application to a newer version.

  * Upgrade the Python libraries::

      pip install -r requirements/requirements.txt

  * Upgrade the JavaScript libraries::

      npm install

  * Migrate the database to the most recent version::

      python manage.py db upgrade head

  * Build the new version::

      make

It will also be necessary to restart uWSGI and Celery.
