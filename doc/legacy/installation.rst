Installation Guide
==================


General settings
----------------

.. code:: bash

    # project's user
    AILERON_USER=aileron

    # PostgreSQL aileron DB settings
    PGSQL_USER="aileron"
    PGSQL_PASS="pass"
    PGSQL_DBNAME="aileron"



System requirements
-------------------

This installation process has been test on Debian 10.

Minimal packages
~~~~~~~~~~~~~~~~

.. code:: bash

    # minimum comfort
    apt-get install -y \
      binutils cron logrotate procps bash bash-completion grep findutils sed \
      tmux git emacs-nox vim htop wget less tar gzip bzip2 lbzip2 xz-utils \
      dialog locate
    updatedb



User ``aileron``
~~~~~~~~~~~~~~~~

Create the user ``aileron`` and its home directory:

.. code:: bash

    # create the user
    addgroup ${AILERON_USER}
    useradd --base-dir /home/ --home-dir /home/${AILERON_USER}    \
            --shell /bin/bash --gid ${AILERON_USER} --create-home \
            --no-user-group ${AILERON_USER}

.. note::

    You also need to make the home directory of this new user traversable.
    So that uwsgi, nginx and other services can access the project files.


Clone the git repository
~~~~~~~~~~~~~~~~~~~~~~~~

The project is hosted on GitLab.

.. code:: bash

    su -l ${AILERON_USER}
    git clone -b devel https://gitlab.com/amallegol/deseasion $HOME/deseasion && exit


Application Configuration
^^^^^^^^^^^^^^^^^^^^^^^^^

A configuration file ``instance/config.py`` must exist in the root folder of the application, where an example file is already present.

This configuration file must contain at least the database connection information, as presented in the example file.

.. code:: bash

    # config.py
    pushd /home/${AILERON_USER}/deseasion/instance
    cp config.py-example config.py
    DB_URI="postgresql://${PGSQL_USER}:${PGSQL_PASS}@localhost/${PGSQL_DBNAME}"
    DB_KEY="SQLALCHEMY_DATABASE_URI"
    sed -i -e "s#${DB_KEY} *= *'[^']*'#${DB_KEY} = '${DB_URI}'#" config.py
    SECRET=$(head -c 1024 /dev/urandom | tr -dc '[:alnum:]' | head -c 32)
    TOKEN="API_TOKEN_SECRET_KEY"
    sed -i -e "s/${TOKEN} *= *'[^']*'/${TOKEN} = '${SECRET}'/" config.py
    SANDBOX="/home/${AILERON_USER}/deseasion/sandbox"
    SANDBOX_KEY="SANDBOX_ROOT"
    sed -i -e "s#${SANDBOX_KEY} *= *'[^']*'#${SANDBOX_KEY} = '${SANDBOX}'#" config.py
    unset DB_KEY DB_URI TOKEN SECRET SANDBOX SANDBOX_KEY
    popd


The client application also tries to load a configuration file ``instance/config.json``.
Building the application will display a warning if this file doesn't exist.

.. code:: bash

    pushd /home/${AILERON_USER}/deseasion/instance
    cp config.json-example config.json
    popd



Python
~~~~~~

Install Python
^^^^^^^^^^^^^^

Install Python3, pip and virtualenvwrapper:

.. code:: bash

    # Python 3
    apt-get install -y python3 python3-virtualenv python3-pip virtualenvwrapper


Configure the Python virtual environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create a virtual environment to install the libraries:

.. code:: bash

    su -l ${AILERON_USER} -c "mkdir /home/${AILERON_USER}/.virtualenvs"
    USER_PRF="/home/${AILERON_USER}/.profile"
    echo 'export WORKON_HOME=$HOME/.virtualenvs' >> ${USER_PRF}
    echo 'source /usr/share/virtualenvwrapper/virtualenvwrapper.sh' >> ${USER_PRF}
    su -l ${AILERON_USER} -c 'mkvirtualenv --python=/usr/bin/python3 aileron'
    unset USER_PRF


Install the libraries
^^^^^^^^^^^^^^^^^^^^^

Install the necessary dependencies:

.. code:: bash

    # required for Fiona
    apt-get install -y libgdal-dev
    # required for geopandas
    apt-get install -y libfreetype6-dev
    # redis message broker for celery
    apt-get install -y redis-server


Install the libraries in the virtual environment.
Numpy is installed first because other libraries are dependant on it:

.. code:: bash

    su -l ${AILERON_USER} \
       -c "workon aileron; cd /home/${AILERON_USER}/deseasion ;\
           pip install --upgrade pip &&
           pip install \$(grep numpy requirements/requirements.txt) &&
           pip install -r requirements/requirements.txt"


GDAL binaries can be installed to allow to reproject the geographical data faster:

.. code:: bash

    apt-get install -y gdal-bin
    OGR_KEY="OGR2OGR_BIN"
    OGR_PATH=$(which ogr2ogr)
    sed -i -e "s#^.*${OGR_KEY} *= *'[^']*'#${OGR_KEY} = '${OGR_PATH}'#" /home/${AILERON_USER}/deseasion/instance/config.py
    unset OGR_KEY OGR_PATH


Installing the Python sandbox
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The user rules in Python are run in a PyPy sandbox.

The sandbox must be built from source as explained at https://www.pypy.org/download_advanced.html#building-from-source.

Install Mercurial and Python pycparser:

.. code:: bash

  apt-get install -y mercurial python-pycparser

Get the source code and build it:

.. code:: bash

  pushd /home/${AILERON_USER}
  hg clone https://foss.heptapod.net/pypy/pypy
  pushd pypy/pypy/goal
  # comment warning for sandbox
  sed -i '/assert 0, ("--sandbox/,/")/s:^:#:' targetpypystandalone.py
  python ../../rpython/bin/rpython -O2 --sandbox targetpypystandalone.py --withmod-struct
  mv pypy-c pypy-sandbox
  popd
  popd

Update the configuration file:

.. code:: bash

  pushd /home/${AILERON_USER}/deseasion/instance
  PYPY="/home/${AILERON_USER}/pypy"
  PYPY_KEY="PYPY_PATH"
  sed -i -e "s#${SANDBOX_KEY} *= *'[^']*'#${SANDBOX_KEY} = '${SANDBOX}'#" config.py
  unset PYPY_KEY PYPY
  popd

The option :code:`--withmod-struct` must be used to build the sandbox with the `struct` library.



PostgreSQL database and PostGIS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Installing PostgreSQL
^^^^^^^^^^^^^^^^^^^^^

Install PostgreSQL and PostGIS.
Make sure your locales are correctly configured, PostgreSQL may not start otherwise.

.. code:: bash

    # install PostgreSQL
    apt-get install -y postgresql

Installing PostGIS
^^^^^^^^^^^^^^^^^^

The version of PostGIS must be 2.4 or higher.
Make sure your package manager installs the correct version
(this is not the case for Debian stretch):

.. code:: bash

    # the candidate version should be 2.4 or hidher
    apt-cache policy postgis

Install PostGIS if the version in the package manager is correct:

.. code:: bash

    apt-get install -y postgis

Otherwise follow the instructions at https://postgis.net/install/.

Configuration
^^^^^^^^^^^^^

Configure PostgreSQL and PostGIS:

.. code:: bash

    su -l postgres -c \
      "psql -c \"CREATE USER ${PGSQL_USER} WITH PASSWORD '${PGSQL_PASS}'\""
    su -l postgres -c \
      "psql -c \"CREATE DATABASE ${PGSQL_DBNAME} WITH OWNER ${PGSQL_USER}\""
    # superuser needed to create the following extension
    su -l postgres -c \
      "psql -d \"${PGSQL_DBNAME}\" -c \"CREATE EXTENSION postgis\""

It might be necessary to install the protobuf-c library, protoc-c compiler and pkg-config to install the necessary features in PostGIS.

The installation instructions are in the repositories at https://github.com/protocolbuffers/protobuf and https://github.com/protobuf-c/protobuf-c.

Configure the database:

.. code:: bash

    su -l ${AILERON_USER} -c "echo \"localhost::${PGSQL_DBNAME}:${PGSQL_USER}:${PGSQL_PASS}\" > /home/${AILERON_USER}/.pgpass"
    chmod 0600 /home/${AILERON_USER}/.pgpass

Create the tables and populate the database:

.. code:: bash

    su -l ${AILERON_USER} \
       -c "cd /home/${AILERON_USER}/deseasion && workon aileron && \
           python manage.py app create_db && \
           python manage.py db stamp head"

Create the database functions and triggers:

.. code:: bash

    su -l postgres -c \
      "psql -d \"${PGSQL_DBNAME}\" -f /home/${AILERON_USER}/deseasion/sql/tile_bbox.sql"
    su -l postgres -c \
      "psql -d \"${PGSQL_DBNAME}\" -f /home/${AILERON_USER}/deseasion/sql/trigger_feature_make_valid.sql"
    su -l postgres -c \
      "psql -d \"${PGSQL_DBNAME}\" -f /home/${AILERON_USER}/deseasion/sql/dissolve_adjacent.sql"

.. note:: If you see permission denied on your attempts, do the following:

.. code:: bash

    cp -R /home/${AILERON_USER}/deseasion/sql /tmp/
    sudo su -l postgres -c \
      "psql -d \"${PGSQL_DBNAME}\" -f /tmp/sql/tile_bbox.sql"
    sudo su -l postgres -c \
      "psql -d \"${PGSQL_DBNAME}\" -f /tmp/sql/trigger_feature_make_valid.sql"
    sudo su -l postgres -c \
      "psql -d \"${PGSQL_DBNAME}\" -f /tmp/sql/dissolve_adjacent.sql"


JavaScript dependencies: Node.js & npm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Install the nodejs and npm package if they are available for your system:

.. code:: bash

    # Node.js & npm
    apt-get install -y nodejs npm
    # Legacy symlink for node
    apt-get install -y nodejs-legacy

Otherwise follow the instructions at https://nodejs.org/en/download/.

Upgrade node.js to the version 6.0 or later.

.. code:: bash

    # Upgrade npm to the lastest version
    npm install -g npm@latest
    # Update node.js
    npm install -g n
    n stable

Install the javascript packages and build the project:

.. code:: bash

    su -l ${AILERON_USER} -c "cd /home/${AILERON_USER}/deseasion && npm install --dev"
    su -l ${AILERON_USER} -c "cd /home/${AILERON_USER}/deseasion && make"


Web server
~~~~~~~~~~

nginx
^^^^^

Configure the nginx web server:

.. code:: bash

    apt-get install -y nginx
    NGINX_CONF="/etc/nginx/sites-available/default"
    cp ${NGINX_CONF} ${NGINX_CONF}.debian.ori

    cat > ${NGINX_CONF} <<EOF
    server {
        listen 80 default_server;
        listen [::]:80 default_server;
        server_name  _;

        location / {
            # path of the static files
            root   /home/${AILERON_USER}/deseasion/web/app;
            index  index.html index.htm;
        }
        location /api {
            # path of the uWSGI socket
            uwsgi_pass unix:/run/uwsgi/deseasion.sock;
            uwsgi_read_timeout 300s;  # prevent timeout on large file uploads
            include uwsgi_params;
            client_max_body_size 100m;  # max size for file uploads
        }
        location /api/shares {
            # Allow cross-origin request for shared data
            uwsgi_pass unix:/tmp/uwsgi.sock;
            uwsgi_read_timeout 300s;
            include uwsgi_params;
            client_max_body_size 800m;  # max size for the file uploads
            if (\$request_method = 'GET') {
                add_header 'Access-Control-Allow-Origin' '*';
            }
        }
    }
    EOF

    service nginx restart
    unset NGINX_CONF

uWSGI
^^^^^

Install the latest uWSGI version using pip, and create the configuration file.

.. code:: bash

    pip3 install uwsgi
    mkdir -p /etc/uwsgi

    cat <<EOF > /etc/uwsgi/deseasion_config.ini
    [uwsgi]
    ; socket file which will be used to communicate with nginx
    socket=/run/uwsgi/deseasion.sock
    vacuum=true

    ; uncomment to active the stats server
    ; must be a socket address or a port number
    ; use ``uwsgitop`` or ``uwsgi --connect-and-read``
    ;stats=127.0.0.1:1717
    ; activates the stats over http
    ;stats-http=true

    ; path of the application
    chdir=/home/${AILERON_USER}/deseasion

    ; allow to specify the mount point
    manage-script-name=true

    ; the entry point on the server (eg. http://localhost:8080/api)
    mount=/api=app

    ; specify the python virtualenv directory
    virtualenv=/home/${AILERON_USER}/.virtualenvs/deseasion

    ; autoreload the application on modification, refresh period in seconds
    ; use only in development
    ;python-autoreload=1

    ; allow the user to use the standard input
    ; useful for debugging
    ;honour-stdin=1

    ; buffer the post requests
    post-buffering=1

    ; user and group to start as
    ; must allow nginx to access the socket file
    uid=www-data
    gid=www-data

    ; spawn multiple processes
    processes=4
    ;threads=2

    die-on-term=true

    ; start the celery daemon
    ; see http://uwsgi-docs.readthedocs.io/en/latest/AttachingDaemons.html
    master = true
    socket = :3031
    smart-attach-daemon = /tmp/celery.pid /home/${AILERON_USER}/.virtualenvs/deseasion/bin/celery -A app.celery worker --pidfile=/tmp/celery.pid

    ; disable file wrapper, causing problems with io.BytesIO objects
    ; see https://github.com/unbit/uwsgi/issues/1126
    ; uncomment for the newer uwsgi versions
    wsgi-disable-file-wrapper=true
    EOF

The uWSGI server can be started as a service.

The following configuration file will start the uWSGI server as a systemd service, run by the user www-data.

.. code:: bash

    cat <<EOF > /etc/systemd/system/uwsgi.service
    [Unit]
    Description=uWSGUI application
    After=syslog.target

    [Service]
    ExecStart=/usr/local/bin/uwsgi --ini /etc/uwsgi/deseasion_config.ini
    Restart=always
    KillSignal=SIGTERM
    Type=notify
    StandardError=syslog
    NotifyAccess=all
    RuntimeDirectory=uwsgi
    User=www-data
    Group=www-data

    [Install]
    WantedBy=multi-user.target
    EOF

    # Start the service
    systemctl start uwsgi.service


Start the application
~~~~~~~~~~~~~~~~~~~~~

Build the application
^^^^^^^^^^^^^^^^^^^^^

Build the JavaScript application:

.. code:: bash

    su -l ${AILERON_USER} \
        -c "workon aileron; cd /home/${AILERON_USER}/deseasion; make"
    systemctl restart nginx.service


Create users
^^^^^^^^^^^^

The following command will create a new user.
An email, a username, and a password for the new user will be asked.

.. code:: bash

    su -l ${AILERON_USER}
    cd $HOME/deseasion && workon aileron && python manage.py app create_user && exit
