Application configuration
=========================

Backend
-------

The backend part of the application can be configured in ``instance/config.py``.

Frontend
--------

The frontend part of the application can be configured in ``instance/config.json``.
