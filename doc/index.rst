.. deSEAsion documentation master file, created by
   sphinx-quickstart on Wed Jun 20 10:01:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=======================
deSEAsion documentation
=======================

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :hidden:

   installation
   api
   use_cases
   development
   python_api
   legacy
   proposed_changes


.. warning::
    This version of deSEAsion si based on the old obsolete repository.
    Please change to the new uptodate and maintained repository: https://gitlab.com/decide.imt-atlantique/deseasion
    You can directly check out its documentation `here <https://deseasion-2227c9.gitlab.io/>`_.

Getting started
===============

Installation
------------

.. role:: bash(code)
    :language: bash

This web application is packaged using docker compose.
All the following instruction consider you have a working docker/docker compose
installation on your system.
If you need any help setting up docker, please visit the
`docker doc <https://docs.docker.com/engine/install/>`_.

That's it! This repository contains an example `docker-compose.yml` file which you can modify
according to your need:

.. literalinclude:: docker-compose.yml
   :language: yaml

Any information regarding the application configuration can be found in
:doc:`installation section <installation>`.

Starting the application
------------------------

To start in daemon mode (better for production):

.. code:: bash

    docker compose up -d

To start in the terminal (will be shut down once the terminal is stopped):

.. code:: bash

    docker compose up

That's it! You only need to wait around a minute for the whole application to start.

Using the application
---------------------

Add users
~~~~~~~~~

.. code:: bash

    docker compose exec -it backend flask user create

Using the application
~~~~~~~~~~~~~~~~~~~~~

Go to your browser and get the `application page <http://localhost>`_.

Check the backend API
~~~~~~~~~~~~~~~~~~~~~

Go to your browser and get the `Swagger-UI <http://localhost/api/apidocs>`_.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
